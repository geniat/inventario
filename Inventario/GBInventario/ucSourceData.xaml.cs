﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GBInventario.Controls;

namespace GBInventario
{
    /// <summary>
    /// Lógica de interacción para ucSourceData.xaml
    /// </summary>
    public partial class ucSourceData : UserControl
    {
        //cDatabaseGenia geniaDataBase = new cDatabaseGenia();
        cCompactDatabase localDataBase = new cCompactDatabase();
        cTools tools = new cTools();
        MainWindow parentWindow;

        public ucSourceData()
        {
            InitializeComponent();
        }

        private void btnGeniaSource_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnLocalSource_Click(object sender, RoutedEventArgs e)
        {

        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //parentWindow = Window.GetWindow(this) as MainWindow;
            ////intenta conexión a base de datos genia
            //if(geniaDataBase.isConnect())
            //{
            //    tools.setVariable("sourceData", "genia");
            //    tbState.Text = Properties.Resources.srce_data_db_genia_conected;
            //    tbState.Foreground = Brushes.DarkGreen;
            //    //btnGeniaSource.Background = Brushes.LightGreen;
            //    //btnLocalSource.Background = Brushes.Transparent;
            //    parentWindow.TimedDialog.ShowHandlerDialog(Properties.Resources.srce_data_db_genia_conected, 2000);
                
            //}
            //else
            //{
            //    tools.setVariable("sourceData", "local");
            //    localDataBase.InitializeDatabase(Properties.Settings.Default.path_genia);
            //    tbState.Text = Properties.Resources.srce_data_db_disconnected;
            //    tbState.Foreground = Brushes.Red;
            //    //btnGeniaSource.Background = Brushes.Transparent;
            //    //btnLocalSource.Background = Brushes.LightGreen;
            //    parentWindow.TimedDialog.ShowHandlerDialog(Properties.Resources.srce_data_db_local_conected, 2000);
                
            //}
        }
    }
}
