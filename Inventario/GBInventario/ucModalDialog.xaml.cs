﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;

namespace GBInventario
{
    /// <summary>
    /// Lógica de interacción para ucModalDialog.xaml
    /// </summary>
    public partial class ucModalDialog : UserControl
    {
        //private System.Timers.Timer timer = new System.Timers.Timer();
        public ucModalDialog()
        {
            InitializeComponent();
            Visibility = Visibility.Hidden;
            //timer.Interval = 500;
            //timer.Elapsed += timer_Elapsed;
        }

        void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            // HACK: Simulate "DoEvents"
            this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background, new ThreadStart(delegate { }));
        }
        private bool _hideRequest = false;
        private bool _result = false;
        private UIElement _parent;

        public void SetParent(UIElement parent)
        {
            _parent = parent;
        }

        #region Message

        public string Message
        {
            get { return (string)GetValue(MessageProperty); }
            set { SetValue(MessageProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Message.
        // This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MessageProperty = DependencyProperty.Register("Message", typeof(string), typeof(ucModalDialog), new UIPropertyMetadata(string.Empty));

        #endregion

        public bool ShowHandlerDialog(string message)
        {
            Message = message;
            
            Visibility = Visibility.Visible;

            MessageTextBlock.Text = Message;

            OkButton.Visibility = System.Windows.Visibility.Hidden;

            //_parent.IsEnabled = false;

            _hideRequest = false;
            /*//while (!_hideRequest)
            //{
                // HACK: Stop the thread if the application is about to close
                if (this.Dispatcher.HasShutdownStarted || this.Dispatcher.HasShutdownFinished)
                {
                    //break;
                }
                
                // HACK: Simulate "DoEvents"
                this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background, new ThreadStart(delegate { }));
                Thread.Sleep(20);
            //}*/
            //timer.Enabled = true;
            //timer.Start();

            return _result;
        }

        public void HideHandlerDialog()
        {
            //timer.Enabled = false;
            //timer.Stop();
            _hideRequest = true;
            Visibility = Visibility.Hidden;
            _parent.IsEnabled = true;
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            _result = true;
            HideHandlerDialog();
            
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            _result = false;
            HideHandlerDialog();
        }

        void previewPlayer_MediaEnded(object sender, RoutedEventArgs e)
        {
        //    imgLoadingImage.Position = TimeSpan.Zero;
        //    imgLoadingImage.Play();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            //timer.Enabled = false;
            //timer.Stop();
        }
    }
}
