﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tools;

namespace GBInventario.Controls
{
    public class cTools
    {
        Variables variable = new Variables();
        FormatDate formatDate = new FormatDate(Properties.Settings.Default.ConfigFechaHora, Properties.Settings.Default.ConfigFechaHoraFiles);
        public void setVariable(string key, object value)
        {
            variable.SetValue(key, value);
        }
        public string getVariable(string key)
        {
            //if (string.IsNullOrEmpty(variable.GetValue<string>(key)))
            //    return null;
            //else
                return variable.GetValue<string>(key);
        }
        public string FormatoFileFecha(DateTime fecha)
        {
            return formatDate.FormatoFileFecha(fecha);
        }
        public DateTime GetfechaFile(string fecha)
        {
            return formatDate.getFechaFileName(fecha);
        }
    }
}
