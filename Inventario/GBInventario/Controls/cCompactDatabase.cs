﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data;
using System.Data.SqlServerCe;
using GBInventario.Models;

namespace GBInventario.Controls
{
    public class cCompactDatabase
    {
        public static string connectionString;

        public bool InitializeDatabase(string path)
        {
            bool resp = false;
            connectionString ="";
            try
            {
                string dbPath = String.Format("{0}CompactGenia_1.0.sdf", path);
                connectionString = String.Format("DataSource=\"{0}\";Max Database Size=3000;", dbPath);
                if (File.Exists(dbPath))
                    return true;

                if (CreateDatabase(path, connectionString))
                {
                    SqlCeConnection conn = new SqlCeConnection(connectionString);
                    conn.Open();

                    CreateTables(conn);
                    resp = true;
                }
            }
            catch { }
            return resp;
        }
        private bool CreateDatabase(string path, string connectionString)
        {
            bool resp = false;
            try
            {
                SqlCeEngine en = new SqlCeEngine(connectionString);
                en.CreateDatabase();
                en.Dispose();
                resp = true;
            }
            catch
            {
            }
            return resp;
        }
        private void CreateTables(SqlCeConnection conn)
        {
            using (SqlCeCommand comm = new SqlCeCommand())
            {
                comm.Connection = conn;
                comm.CommandType = System.Data.CommandType.Text;

                comm.CommandText = "CREATE TABLE existencias (idExist [int] IDENTITY(1,1) PRIMARY KEY, "
                    + "barcode NVARCHAR(100), "
                    + "epc NVARCHAR(100), "
                    + "tag_id NVARCHAR(100), "
                    + "nombre NVARCHAR(500), "
                    + "descripcion NVARCHAR(500), "
                    + "ubicacion NVARCHAR(200), "
                    + "usuario NVARCHAR(100), "
                    + "observacion NVARCHAR(500), "
                    + "fecha [datetime], "
                    + "clasificacion NVARCHAR(200), "
                    + "categoria NVARCHAR(500), "
                    + "volumen [int], "
                    + "ejemplar [int], "
                    + "tomo [int], "
                    + "prestado [bit])";
                comm.ExecuteNonQuery();
                //SELECT idInv, CONCAT (nombre , ', ' , fecha_ini) AS nombre, estado, fecha_ini,tipo, fecha_fin, biblioteca FROM inventario ORDER BY fecha_ini DESC; "
                //CREATE TABLE myTable (myBit bit, myStr NVARCHAR(100))
                comm.CommandText = "CREATE TABLE inventario (idInv [int] IDENTITY(1,1) PRIMARY KEY, "
                    + "nombre NVARCHAR(200), "
                    + "fecha_ini [datetime], "
                    + "estado [int], "
                    + "fecha_fin [datetime], "
                    + "biblioteca [int], "
                    + "tipo [int], "
                    + "fecha_existencias [datetime])";
                comm.ExecuteNonQuery();

                comm.CommandText = "CREATE TABLE inventario_en_curso (idInvCurso [int] IDENTITY(1,1) PRIMARY KEY, "
                    + "acq NVARCHAR(100) NOT NULL, "
                    + "estado [int], "
                    + "id_inventario [int], "
                    + "observacion NVARCHAR(200), "
                    + "fecha [datetime])";
                comm.ExecuteNonQuery();
            }
        }
        public string insertInventario(string nombre, int biblioteca)
        {
            string resp = "";

            SqlCeConnection conn = new SqlCeConnection(connectionString);
            try
            {
                string query = "INSERT INTO inventario (nombre, fecha_ini, estado, biblioteca, tipo, fecha_existencias) VALUES (@nombre, GETDATE(), 0, @biblioteca ,0, NULL);";
                if (conn.State == System.Data.ConnectionState.Closed)
                {
                    conn.Open();
                }
                SqlCeCommand commSqlCe = new SqlCeCommand(query, conn);



                SqlCeParameter param = new SqlCeParameter("@nombre", SqlDbType.NVarChar);
                param.Value = nombre;
                commSqlCe.Parameters.Add(param);
                param = new SqlCeParameter("@biblioteca", SqlDbType.Int);
                param.Value = biblioteca;
                commSqlCe.Parameters.Add(param);

                resp = commSqlCe.ExecuteNonQuery().ToString();
                int numReg = 0;
                if (!int.TryParse(resp, out numReg))
                    return resp;
                if (numReg < 1)
                    return resp;
                //verifica ultimo id
                commSqlCe = new SqlCeCommand("SELECT @@IDENTITY;", conn);
                resp = commSqlCe.ExecuteScalar().ToString();

                

            }
            catch (Exception sqle)
            {
                conn.Close();
                //MessageBox.Show(sqle.Message);
                resp = sqle.Message;
            }
            conn.Close();

            return resp;
        }
        public bool insertExistencias(string file, out string message)
        {
            message = "";
            bool resp = false;
            //SqlCeConnection conn = new SqlCeConnection(connectionString);
            //try
            //{
            //    if (conn.State == System.Data.ConnectionState.Closed)
            //    {
            //        conn.Open();
            //    }
            //    SqlCeCommand commSqlCe = new SqlCeCommand(query, conn);
            //    string select = query.Trim().Substring(0, 6);
            //    switch (select)
            //    {
            //        case "SELECT":
            //            resp = commSqlCe.ExecuteScalar().ToString();
            //            break;
            //        case "UPDATE":
            //            resp = commSqlCe.ExecuteNonQuery().ToString();
            //            break;
            //        case "DELETE":
            //            resp = commSqlCe.ExecuteNonQuery().ToString();
            //            break;
            //        case "INSERT":
            //            resp = commSqlCe.ExecuteNonQuery().ToString();
            //            break;
            //        case "TRUNCA":
            //            resp = commSqlCe.ExecuteNonQuery().ToString();
            //            break;
            //        case "ALTER ":
            //            resp = commSqlCe.ExecuteNonQuery().ToString();
            //            break;
            //    }
            //}
            //catch (Exception sqle)
            //{
            //    conn.Close();
            //    //MessageBox.Show(sqle.Message);
            //    resp = sqle.Message;
            //}
            //conn.Close();
            return resp;
        }
        public string exeQuery(string query)
        {
            //ConnStringPostgresql = ConfigurationSettings.AppSettings["postgresSqlConectionString"];
            string resp = "";

            SqlCeConnection conn = new SqlCeConnection(connectionString);
            try
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                {
                    conn.Open();
                }
                SqlCeCommand commSqlCe = new SqlCeCommand(query, conn);
                string select = query.Trim().Substring(0, 6);
                switch (select)
                {
                    case "SELECT":
                        resp = commSqlCe.ExecuteScalar().ToString();
                        break;
                    case "UPDATE":
                        resp = commSqlCe.ExecuteNonQuery().ToString();
                        break;
                    case "DELETE":
                        resp = commSqlCe.ExecuteNonQuery().ToString();
                        break;
                    case "INSERT":
                        resp = commSqlCe.ExecuteNonQuery().ToString();
                        break;
                    case "TRUNCA":
                        resp = commSqlCe.ExecuteNonQuery().ToString();
                        break;
                    case "ALTER ":
                        resp = commSqlCe.ExecuteNonQuery().ToString();
                        break;
                }
            }
            catch (Exception sqle)
            {
                conn.Close();
                //MessageBox.Show(sqle.Message);
                resp = sqle.Message;
            }
            conn.Close();

            return resp;
        }
        public object exeQuery(string query, string tipoDato)
        {
            object resp = new object();
            DataTable dt = new System.Data.DataTable();
            DataSet ds = new System.Data.DataSet();
            SqlCeConnection conn = new SqlCeConnection(connectionString);
            SqlCeDataAdapter da = new SqlCeDataAdapter(query, conn);
            try
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                {
                    conn.Open();
                }
                switch (tipoDato)
                {
                    case "DataTable":
                        da.Fill(dt);
                        resp = new DataTable();
                        resp = dt;
                        break;
                    case "DataSet":
                        da.Fill(ds);
                        resp = new DataSet();
                        resp = ds;
                        break;
                    case "string":
                        da.Fill(dt);
                        if (dt.Rows.Count > 0)
                            resp = dt.Rows[0][0].ToString();
                        break;

                }
                if (conn.State == System.Data.ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            catch (Exception sqle)
            {
                conn.Close();
                resp = sqle.Message;
            }
            return resp;
        }
        public int? insertExistencias(DataTable items, Dictionary<string, int> columns, out string fecha, out string mensaje)
        {
            fecha = "";
            int? resp = null;
            mensaje = "";
            if (!cleanExistencias().HasValue)
                return resp;
            string input = "";
            bool firstRow = true;
            //List<string> columns = new List<string>();
            StringBuilder sbQuery = new StringBuilder();
            //object[] arrayCol;
            //StringBuilder sbVal = new StringBuilder();
            foreach (DataRow row in items.Rows)
            {

                if (firstRow && Properties.Settings.Default.db_colexs_head)
                {
                    sbQuery.Append("INSERT INTO ejemplares.existencias(");
                    //sbVal.Append(") VALUES (");
                    foreach (var item in columns)
                    {
                        //row[arrCol[0]]
                        sbQuery.Append(item.Key + ", ");
                    }
                    input = sbQuery.ToString(0, sbQuery.Length - 2);
                    sbQuery = new StringBuilder(input);
                    sbQuery.Append(") VALUES ");
                    firstRow = false;

                    continue;
                }
                sbQuery.Append("(");
                //id, barcode, tag_id, nombre, descripcion, ubicacion, clasificacion, categoria, volumen, ejemplar, tomo, prestado
                foreach (var item in columns)
                {
                    if (item.Value == Properties.Settings.Default.db_colexs_prestado)
                        if (item.Key == Properties.Settings.Default.db_colexs_prestado_true)
                            sbQuery.Append("'True',");
                        else
                            sbQuery.Append("'False',");
                    else
                    {
                        string aux = row[item.Value - 1].ToString().Trim();
                        //if (aux.Contains("'"))
                        //    aux = "";
                        if (string.IsNullOrEmpty(aux))
                            sbQuery.Append("NULL,");
                        else
                            sbQuery.Append("'" + aux.Replace("'", "\"") + "',");
                    }
                }
                //eliminar "," y agregar "),"
                input = sbQuery.ToString(0, sbQuery.Length - 1) + "),";
                sbQuery = new StringBuilder(input);
                //string aux = row[item.Value].ToString(); 
                //sbQuery.Append("('" + string.Join("','", aux) + "'), ");


                //INSERT INTO ejemplares.existencias(barcode, nombre, fecha) VALUES ('1', 'uno', NOW()),('2', 'dos', NOW()),('3', 'tres', NOW()),('4', 'cuatro', NOW()),('5', 'cinco', NOW());
                //fecha = fechaTools.FormatoInsertFecha(DateTime.Now);
                //sbQuery.Append("('" + string.Join("','", "") + "', '" + fecha + "'), ");


            }
            input = sbQuery.ToString(0, sbQuery.Length - 1) + ";";
            sbQuery = new StringBuilder(input);
            int respAux = 0;
            string respQuery = exeQuery(sbQuery.ToString());

            if (int.TryParse(respQuery, out respAux))
                resp = respAux;
            else
                mensaje = respQuery;
            return resp;
        }
        private int? cleanExistencias()
        {
            int? resp = null;
            try
            {
                string sQuery = "DELETE FROM ejemplares.existencias; ALTER SEQUENCE ejemplares.autonum_existencias RESTART WITH 0;";
                string sNumReg = exeQuery(sQuery);
                int respAux = 0;
                if (int.TryParse(sNumReg, out respAux))
                    resp = respAux;
            }
            catch { }
            return resp;
        }
        //public List<Inventario> getInventories()
        //{
        //    string query = "SELECT idInv, (nombre + CAST(fecha_ini AS nvarchar)) AS nombre, estado, fecha_ini,tipo, fecha_fin, biblioteca FROM inventario ORDER BY fecha_ini DESC; ";
        //    List<Inventario> inventarios = new List<Inventario>();
        //    try
        //    {
        //        DataTable inventariosTabla = (DataTable)exeQuery(query, "DataTable");
        //        foreach (DataRow row in inventariosTabla.Rows)
        //        {
        //            Inventario inv = new Inventario();
        //            inv.id = Convert.ToString(row["id"]);
        //            inv.nombre = Convert.ToString(row["nombre"]);
        //            inv.fecha_ini = Convert.ToDateTime(row["fecha_ini"]);
        //            inv.estado = Convert.ToInt16(row["estado"]);
        //            //inv.biblioteca = Convert.ToInt16(row["biblioteca"]);
        //            // inv.tipo = Convert.ToInt16(row["tipo"]);

        //            inventarios.Add(inv);
        //        }

        //        return inventarios;

        //    }
        //    catch //(Exception ex)
        //    {
        //        return null;
        //        //throw;
        //    }
        //}
    }
}
