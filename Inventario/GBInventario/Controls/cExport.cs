﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace GBInventario.Controls
{
    class cExport
    {
        //cDatabaseGenia geniaDataBase = new cDatabaseGenia();
        cFiles archivos = new cFiles();
        GeniaWebServices.Service1 geniaWebService = new GeniaWebServices.Service1();
        /// <summary>
        /// Exportar inventario a un archivo csv
        /// </summary>
        /// <param name="report">datos a exportar</param>
        /// <returns>Nombre del archivo creado</returns>
        public bool ExportInventoryCsv(string pathFile, DataTable report)
        {
            bool resp = false;

            StringBuilder sb = new StringBuilder();
            //nombre, fecha_ini, estado, fecha_fin, biblioteca, tipo, fecha_existencias
            DataRow row = report.Rows[0];
            //sb.Append( string.Format(Properties.Resources.export_header,row[0].ToString(), row[1].ToString(), row[6].ToString()));
            //sb.Append(Environment.NewLine);
            //sb.Append(Environment.NewLine);
            foreach (DataColumn col in report.Columns)
            {
                sb.Append(col.ColumnName + ';');
            }

            sb.Remove(sb.Length - 1, 1);
            sb.Append(Environment.NewLine);

            foreach (DataRow rowInv in report.Rows)
            {
                for (int i = 0; i < report.Columns.Count; i++)
                {
                    sb.Append(rowInv[i].ToString().Trim() + ";");
                }

                sb.Append(Environment.NewLine);
            }

            if (archivos.crearFileExportInventario(pathFile, sb.ToString()))
                resp = true;

            return resp;
        }
        /// <summary>
        /// Exportar inventario a un archivo csv
        /// </summary>
        /// <param name="report">datos a exportar</param>
        /// <returns>Nombre del archivo creado</returns>
        public bool ExportInventoryTxt(string pathFile, string content)
        {
            bool resp = false;

            if (archivos.crearFileExportInventario(pathFile, content))
                resp = true;

            return resp;
        }
        //public Dictionary<string, string> getCategories(string idInventory)
        //{
        //    geniaWebService.inventoryc
        //    return geniaDataBase.inventoryCategories(idInventory);
        //}
        //public DataTable getDescInventory(string idInventory)
        //{
            
        //    return geniaDataBase.getDescriptionInventory(idInventory);
        //}
        //public DataTable getInventory(string idInventory)
        //{
        //    return geniaDataBase.getInventoryFullExistencias(idInventory);
        //}

    }
}
