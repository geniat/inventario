﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using GBInventario.Models;
using Tools;

namespace GBInventario.Controls
{
    public class cFiles
    {
        FormatDate herram = new FormatDate(Properties.Settings.Default.ConfigFechaHora, Properties.Settings.Default.ConfigFechaHoraFiles);
        //cDatabaseGenia database = new cDatabaseGenia();
        cTools tools = new cTools();
        GeniaWebServices.Service1 geniaWebService = new GeniaWebServices.Service1();
        mConfiguration conf;

        public cFiles()
        {
            string message = "";
            conf = new mConfiguration(out message);
            setLenguage();
        }
        private void setLenguage()
        {
            #region inicializa idioma
            var culture = System.Globalization.CultureInfo.CurrentCulture;//.GetCultureInfo("en-EU");//.CurrentCulture;
            System.Threading.Thread.CurrentThread.CurrentCulture = culture;
            //Dispatcher.Thread.CurrentUICulture = culture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = culture;
            #endregion
        }
        public List<string> getAllFiles(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            List<string> respAux = Directory.GetFiles(path, "*.ig", SearchOption.TopDirectoryOnly).ToList();
            List<string> respSync = Directory.GetFiles(path, "*.igs", SearchOption.TopDirectoryOnly).ToList();
            List<string> respMiss = Directory.GetFiles(path, "*.if", SearchOption.TopDirectoryOnly).ToList();
            List<string> respMissSync = Directory.GetFiles(path, "*.ifs", SearchOption.TopDirectoryOnly).ToList();
            respAux.AddRange(respSync);
            respAux.AddRange(respMiss);
            respAux.AddRange(respMissSync);
            return respAux;
        }
        public List<string> getFiles(string path, string typeFile)
        {
            //string message = "";
            List<string> resp = new List<string>();
            //DateTime maxDateTime = new DateTime();
            List<string> respAux = new List<string>();
            if(!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            resp = Directory.GetFiles(path, "*." + typeFile).ToList();
            //int countReg = 0;
            //foreach (string filel in respAux)
            //{
            //    if (fileIsClose(filel,out countReg, out message))
            //    {
            //        resp.Add(filel);
            //        continue;
            //    }
            //    DateTime dateTimefile = herram.getFechaFileName(filel);
            //    //para que hago esto????????????
            //    if (dateTimefile >= maxDateTime)
            //    {
            //        maxDateTime = dateTimefile;
            //        resp.Insert(0, filel);
            //    }
            //}
            return resp;
        }
        /// <summary>
        /// Crear archivo de carga de ítems detectados detectados
        /// </summary>
        /// <param name="directorio">carpeta donde se creará el archivo</param>
        /// <param name="fileType">if:inventario faltantes; ig: inventario general</param>
        /// <param name="inventarioSel">(opcional)Inventario asociado</param>
        /// <param name="usuario">(opcional) usuario</param>
        /// <returns></returns>
        public string crearArchivoInventario(string directorio, string fileType, string inventarioSel, string usuario, string infoFileName)
        {
            string idCliente = System.Configuration.ConfigurationManager.AppSettings["idCliente"];//Properties.Settings.Default.idCliente;
            string archivoFin = "";
            //bool validaTipoFile = false;
            
            bool saved = false;
            try
            {
                //crea directorio si no existe
                if (!System.IO.Directory.Exists(directorio))
                {
                    System.IO.Directory.CreateDirectory(directorio);
                }
                //set directory
                //if (System.IO.Directory.Exists(directorio))
                //{
                    string columnas = "";

                    if (fileType == Properties.Settings.Default.file_type_general)
                        columnas = Properties.Settings.Default.file_encabezado_general;
                    else if (fileType == Properties.Settings.Default.file_type_missing)
                        columnas = Properties.Settings.Default.file_encabezado_faltantes;
                    if (!string.IsNullOrEmpty(infoFileName))
                        infoFileName = infoFileName + "_";
                    archivoFin = Path.Combine(directorio, infoFileName + herram.FormatoFileFecha(DateTime.Now) + "." + fileType);
                    File.Create(archivoFin).Close();

                    using (System.IO.StreamWriter sw = new System.IO.StreamWriter(archivoFin))
                    {
                        //tipoArchivo;fecha sincronización (opcional);inventario seleccionado (opcional);identificador de biblioteca;usuario crea
                        //sw.WriteLine(fileType + ";;" + inventarioSel + ";" + idCliente + ";" + usuario);
                        sw.WriteLine(columnas);
                    }
                    if (System.IO.File.Exists(archivoFin))
                        saved = true;
                    else
                        saved = false;
                //}
            }
            catch
            {
                saved = false;
            }
            if (saved)
                return archivoFin;
            else
                return "";
        }
        /// <summary>
        /// Obtiene la lista de tags leídos en el archivo
        /// </summary>
        /// <param name="path">Nombre completo del archivo</param>
        /// <param name="idInventario">Identificador del inventario que se está procesando</param>
        /// <param name="message">Se guardan mensajes del sistema</param>
        /// <returns>Lista de tags contenidos en el archivo</returns>
        public List<string> loadFromFile(string path,string idInventario, out string message)
        {
            List<string> resp = new List<string>();
            bool validaIdInv = false;
            bool validaTipoFile = false;
            string idInv = idInventario.Split('-')[0].Trim();
            message = "";

            if(!File.Exists(path))
            {
                message = Properties.Resources.file_not_exists;
                return resp;
            }
            using (StreamReader stream = new StreamReader(path))
            {
                string line = "";
                if ((line = stream.ReadLine()) != null)
                {
                    string[] array = line.Split(';');

                    if (string.IsNullOrEmpty(idInventario))
                        message = Properties.Resources.file_inventory_no_sel;
                    else
                        validaIdInv = true;
                    string ext = path.Substring(path.Length - 3, 3);
                    if (ext == "." + Properties.Settings.Default.file_type_general || ext == "." + Properties.Settings.Default.file_type_missing)
                        validaTipoFile = true;

                }
                if (!validaTipoFile)
                {
                    message += Properties.Resources.file_wrong_type;
                    stream.Close();
                    return resp;
                }
                if (!validaIdInv)
                {
                    message += Properties.Resources.file_no_inventory_sel;
                }

                if (line.Trim() != Properties.Settings.Default.file_encabezado_general)
                {
                    message += Properties.Resources.file_wrong_header;
                }

                //mTag tagAux;
                while ((line = stream.ReadLine()) != null)
                {
                    if (line.Length < 26)
                        continue;
                    if (line.Substring(0, 2) == "//")
                        continue;
                    string[] array = line.Split(';');
                    resp.Add(array[0]);
                }
            }
            return resp;
        }

        public bool EscribirLineaFaltantes(string nombreArchivo, string acq, string fecha, string observacion)
        {
            bool resp = false;
            try
            {
                if (File.Exists(nombreArchivo))
                {
                    File.AppendAllText(nombreArchivo, acq + ";" + fecha + ";" + observacion + Environment.NewLine);
                    resp = true;
                }
            }
            catch
            {
                resp = false;
            }
            return resp;
        }
        public bool EscribirLineaGeneral(string nombreArchivo, List<string> id_tags)
        {
            bool resp = false;
            try
            {
                if (File.Exists(nombreArchivo))
                {
                    File.AppendAllLines(nombreArchivo, id_tags);//AppendAllText(nombreArchivo, id_tag + ";" + fecha + ";" + observacion + Environment.NewLine);
                }
            }
            catch
            {
                resp = false;
            }
            return resp;
        }
        public bool LoadListToInventory(string idInventory, string pathFile , List<string> tagList, string user, string library, out string mensaje)
        {
            int? actualizados = 0;
            mensaje = "";
            bool resp = false;
            try
            {
                string message = "";
                //string path = pathFile.Replace("\\", "/");
                int sizeList = Properties.Settings.Default.size_list_tags_update;
                //cantidad de sublistas
                var chunkCount = tagList.Count() / sizeList;
                //si la división no es exacta se debe agregar una lista mas
                if (tagList.Count % sizeList > 0)
                    chunkCount++;
                string inventory_id = idInventory.Split('-')[0].Trim();
                //obtiene las sublistas
                string response = "";
                for (var i = 0; i < chunkCount; i++)
                {
                    List<string> Auxlist = tagList.Skip(i * sizeList).Take(sizeList).ToList();
                    response = geniaWebService.UpdateFileToInventarioEnCurso(Auxlist.ToArray(), inventory_id, out message);
                    //actualizados = database.UpdateFileToInventarioEnCursoExistencias(Auxlist, pathFile, idInventory.Split('-')[0].Trim(), user, library, tagList.Count.ToString(), out message) + actualizados;
                    //actualizados = database.UploadFileGeniaDb(Auxlist, pathFile, user, library, tagList.Count.ToString(), out message);
                    int responseParse = 0;
                    if (int.TryParse(response, out responseParse))
                        actualizados += responseParse;
                    bool first = false;
                    int tags_count = 0;
                    if (i == 0)
                    {
                        tags_count = tagList.Count;
                        first = true;
                    }
                    else
                        tags_count = actualizados.Value;
                    response = geniaWebService.UploadFileGeniaDb(first, true, Auxlist.ToArray(), inventory_id, pathFile, user, library, tags_count.ToString(), out message);
                    if(string.IsNullOrEmpty(response))
                    {
                        mensaje = message;
                        return false;
                    }
                }
                
                mensaje = actualizados.Value.ToString();
                resp = true;
            }
            catch (Exception ex){
                mensaje = ex.Message;
                resp = false;
            }
            return resp;
        }
        public int countRegFiles(string path, out string message)
        {
            message = "";
            int resp = 0;
            if(!File.Exists(path))
            {
                message = "No existe archivo " + path;
                return 0;
            }
            using (StreamReader stream = new StreamReader(path))
            {
                string line = "";
                while ((line = stream.ReadLine()) != null)
                {
                    if (line.StartsWith("tag_id"))
                        continue;
                    string[] array = line.Split(';');
                    if (array[0].Length == 24)
                        resp++;

                }
                
            }
            return resp;
        }
        //public bool fileIsClose(string path,out int countReg, out string message)
        //{
        //    message = "";
        //    countReg = 0;
        //    bool resp = false;

        //    using(StreamReader stream = new StreamReader(path))
        //    {
        //        string line = "";
        //        if ((line = stream.ReadLine()) != null)
        //        {
        //            string[] array = line.Split(';');
        //            if (array.Length < 6)
        //            {
        //                stream.Close();
        //                return false;
        //            }

        //            if (!int.TryParse(array[5], out countReg))
        //            {
        //                stream.Close();
        //                return false;
        //            }  
        //            resp = true;
        //        }
        //    }
        //    return resp;
        //}
        /// <summary>
        /// Syncronizar archivos
        /// </summary>
        /// <param name="path">Archivo a sincronizar</param>
        /// <param name="invSel">Inventario donde se va a cargar el archivo</param>
        /// <param name="fileClose">Cerrar archivo actual</param>
        /// <param name="numTags">Si fileClose es verdadero, enviar el número de tags</param>
        /// <param name="message">Mensajes devueltos por el proceso</param>
        public bool syncFile(string path, string invSel, bool fileClose, out string message)
        {
            message = "";
            //int countReg = 0;
            setLenguage();
            try
            {

                if (!File.Exists(path))
                {
                    message = Properties.Resources.file_not_exists;
                    return false;
                }
                if (isFileSync(path, invSel.Split('-')[0].Trim()))
                {
                    //if (WpfMessageBox.Show("", "Archivo previamente sincronizado, desea continuar sincronizando de todos modos ?", System.Windows.MessageBoxButton.YesNo) == System.Windows.MessageBoxResult.No)
                    message = Properties.Resources.file_prev_sync;
                    return false;
                }
                string user = "";
                string library = conf.idCliente;
                List<string> tagsInFile = loadFromFile(path, invSel, out message);
                if (tagsInFile.Count <= 0)
                {
                    File.Delete(path);
                    message = Properties.Resources.head_file_reg_num + ": " + tagsInFile.Count;
                    return false;
                }

                if (LoadListToInventory(invSel,path, tagsInFile,user, library, out message))
                {
                    //guarda copia y elimina file sincronizado
                    string pathFileSync = path + "s";
                    File.Copy(path, pathFileSync, true);
                    if (File.Exists(pathFileSync))
                    {
                        File.Delete(path);
                    }
                }
                //message = "Sync " + message + " Reg.";
                return true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }
        }
        private bool isFileSync(string path, string inventory_id)
        {
            bool resp = false;
            string message = "";
            //return database.isSyncFile(path.Replace('\\', '/'), out message);
            string response = geniaWebService.isSyncFile(path.Replace('\\', '/'),inventory_id, out message);
            if (bool.TryParse(response, out resp))
                return resp;
            else
                return false;
            //System.Data.DataTable table = database.getSyncFile(out message);
            //foreach (System.Data.DataRow row in table.Rows)
            //{
            //    if (row[1].ToString().Replace('\\', '/') == path.Replace('\\','/') && row[5].ToString() == countReg)
            //        return true;
            //}
            //return false;
        }
        public int? getSynchronized(string Inv)
        {
            int resp = 0;
            //string sourceData = tools.getVariable("sourceData");
            //if (string.IsNullOrEmpty(sourceData))
            //{
            //    return null;
            //}
            //if (sourceData != "genia")
            //{
            //    return null;
            //}
            //Dictionary<string, int> inventoryState = database.inventorySummary(Inv.Split('-')[0].Trim());
            //resp = inventoryState["1"];
            return resp;



            //System.Data.DataTable inventoryState = database.inventorySummary(Inv.Split('-')[0].Trim());
            //foreach(System.Data.DataRow row in inventoryState.Rows)
            //{
            //    switch(row[0].ToString())
            //    {
            //        case "1":
            //            int.TryParse(row[1].ToString(), out resp);
            //            return resp;
            //            //break;
            //    }
            //}
            //return null;
        }
        //public bool closeFile(string path, out string message)
        //{
        //    message = "";
        //    string numTags = "0";

        //    //int intNumTags = 0;

        //    //if (!int.TryParse(numTags, out intNumTags))
        //    //    return false;

        //    bool resp = false;
        //    string[] Completfile = File.ReadAllLines(path);
        //    if (Completfile.Length < 2)
        //    {
        //        File.Delete(path);
        //        return false;
        //    }
        //    numTags = (Completfile.Length - 2).ToString();
        //    string firstLine = Completfile[0];
        //    string[] arrayFirstLine = firstLine.Split(';');
        //    if (arrayFirstLine.Length < 6)
        //        Completfile[0] = firstLine + ";" + numTags;
        //    else
        //    {
        //        arrayFirstLine[5] = numTags;
        //        Completfile[0] = "";
        //        foreach (string data in arrayFirstLine)
        //            Completfile[0] += data + ";";
        //        Completfile[0] = Completfile[0].Substring(0, Completfile[0].Length - 1);
        //    }
        //    File.WriteAllLines(path, Completfile);
            
        //    return resp;
        //}
        public bool crearFileExportInventario(string nameFile, string contentFile)
        {
            bool resp = false;
            try
            {
                File.WriteAllText(nameFile, contentFile);
                if (File.Exists(nameFile))
                    resp = true;
            }
            catch { }
            return resp;
        }
    }

}
