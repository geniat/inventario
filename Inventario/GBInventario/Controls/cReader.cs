﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GBInventario.Models;
using System.Threading;

namespace GBInventario.Controls
{
    public class cReader
    {
        #region variables reader
        public List<string> tags { get; set; }
        public string readerFirmware;
        public bool isConnect = false;
        ControlTSL.TslControl TSL;
        IP30.ControlIp30 ip30;
        string readerType;
        //mTag tag;
        #endregion
        
        #region variables timer
        TimerHelper _timerHelper;
        #endregion

        public cReader()
        {
            #region inicializa reader
            tags = new List<string>();
            #endregion

            #region inicializa timer
            _timerHelper = new TimerHelper();
            _timerHelper.TimerEvent += (timer, state) => Timer_Elapsed();
            #endregion
        }
        #region métodos reader
        public bool connect(string readerName, string portName, out string message)
        {
            message = "";
            int proc = 0;
            int.TryParse(Properties.Settings.Default.id_proceso_rfid, out proc);
            
            isConnect = false;
            message = "";

            if (readerName.ToUpper().Contains("US-1153") || readerName.ToUpper().Contains("US-1128"))
            {
                TSL = new ControlTSL.TslControl(proc);
                readerType = "tsl";
            }
            if (readerName.ToUpper().Contains("IP30"))
            {
                ip30 = new IP30.ControlIp30();
                ip30.attribIp30 = Properties.Settings.Default.AttribIp30;
                ip30.comBaudRate = Properties.Settings.Default.com_ip30_BaudRate;
                readerType = "ip30";
            }
            switch (readerType)
            {
                case "tsl":
                    isConnect = TSL.Connect(portName, out message);
                    if (isConnect)
                    {
                        TSL.habilitado = true;
                        StartTimer();
                    }
                    else
                        TSL.Disconnect();
                    break;
                case "ip30":
                    int idProc =0;
                    int.TryParse(Properties.Settings.Default.id_proceso_rfid, out idProc);
                    isConnect = ip30.Connect(portName,idProc);//TSL.Connect(portName, out message);
                    if (isConnect)
                    {
                        //ip30.conectado = true;
                        //TSL.habilitado = true;
                        Properties.Settings.Default.com_ip30_Firmware = ip30.firmwareIP30;
                        Properties.Settings.Default.Save();
                        StartTimer();
                    }
                    else
                        ip30.Disconnect();
                        //TSL.Disconnect();
                    break;

                default:
                    message = Properties.Resources.rfid_no_support;
                    break;
            }
            
            return isConnect;
        }
        public void disconnect(string readerName)
        {
            switch (readerType)
            {
                case "tsl":
                    TSL.Disconnect();
                    StopTimer();
                    break;
                case "ip30":
                    if (ip30 != null)
                        ip30.Disconnect();
                    StopTimer();
                    break;
            }
        }
        #endregion

        #region métodos timer
        void StartTimer()
        {
            
            _timerHelper.Start(TimeSpan.FromSeconds(1));
            //System.Threading.Thread.Sleep(TimeSpan.FromSeconds(12));
            
        }
        void StopTimer()
        {
            _timerHelper.Stop(new TimeSpan(0, 0, 1));
        }
        void Timer_Elapsed()
        {
            
            List<string> tagsAux = new List<string>();
            switch(readerType)
            {
                case "tsl":
                    tagsAux = TSL.tagsLeidos;
                    if (tagsAux.Count > 0)
                    {
                        TSL.tagsLeidos = new List<string>();
                        foreach (string tagAux in tagsAux) 
                        {
                            //tag = new mTag(tagAux, null);
                            tags.Add(tagAux);
                        }
                        
                    }
                    break;
                case "ip30":
                    isConnect = ip30.isConnect;
                    tagsAux = ip30.tagsLeidos;
                    if (tagsAux == null)
                        break;
                    if (tagsAux.Count > 0)
                    {
                        ip30.tagsLeidos = new List<string>();
                        foreach (string tagAux in tagsAux)
                        {
                            tags.Add(tagAux);
                        }

                    }
                    break;
            }
        }
        #endregion
    }
}
