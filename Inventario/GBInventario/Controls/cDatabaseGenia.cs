﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GBInventario.Models;
using System.Data;
using BaseDatos;
using System.Configuration;
using System.Globalization;

namespace GBInventario.Controls
{
    
    class cDatabaseGenia
    {
        clsBaseDatos dataBase;
        cTools tools;
        cCompactDatabase localDBase;
        Tools.FormatDate fechaTools;
        

        string sourceData;

        public cDatabaseGenia()
        {
            dataBase = new clsBaseDatos(Properties.Settings.Default.ConnStringPostgresGenia);
            tools = new cTools();
            localDBase = new cCompactDatabase();
            fechaTools = new Tools.FormatDate(Properties.Settings.Default.ConfigFechaHora, Properties.Settings.Default.ConfigFechaHoraFiles);
            fechaTools.configFechaHora = Properties.Settings.Default.ConfigFechaHora;
            fechaTools.ConfigFechaHoraFiles = Properties.Settings.Default.ConfigFechaHoraFiles;
            //sourceData = tools.getVariable("sourceData");
        }
        private void startLenguage()
        {
            var culture = System.Globalization.CultureInfo.CurrentCulture;//.GetCultureInfo("en-EU");//.CurrentCulture;
            System.Threading.Thread.CurrentThread.CurrentCulture = culture;
            //Dispatcher.Thread.CurrentUICulture = culture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = culture;
        }
        public bool isConnect()
        {
            string query = "SELECT 1;";
            string respDb = dataBase.exeQuery(query);

            if (string.IsNullOrEmpty(respDb))
                return false;
            if (respDb != "1")
                return false;
            return true;
        }
        public bool isSyncFile(string name, out string message)
        {
            message = "";
            try
            {
                string query = "SELECT id FROM ejemplares.file WHERE name = '" + name + "';";
                var resp = (DataTable)dataBase.exeQuery(query, "DataTable");
                if (resp.GetType() != typeof(DataTable))
                {
                    message = "Error: " + resp.ToString();
                    return false;
                }
                if (resp.Rows.Count == 0)
                    return false;
                else
                    return true;
            }
            catch (Exception ex)
            {
                message = ex.Message + ", StackTrace: " + ex.StackTrace;
            }
            return false;
        }
        public DataTable getSyncFile(out string message)
        {
            message = "";
            try
            {
                string query = "SELECT MAX(id), name, MAX(time_stamp), MAX(biblioteca), MAX(usuario), MAX(num_reg), SUM(efectives)  FROM ejemplares.file GROUP BY name ORDER BY MAX(time_stamp) DESC LIMIT 100;";
                var resp = (DataTable)dataBase.exeQuery(query, "DataTable");
                if (resp.GetType() != typeof(DataTable))
                    return new DataTable();
                return (DataTable)resp;
            }
            catch(Exception ex)
            {
                message = ex.Message + ", StackTrace: " + ex.StackTrace;
            }
            return null;
        }
        public List<Inventario> getInventories()
        {
            string query = "";
            List<Inventario> inventarios = new List<Inventario>();
            DataTable inventariosTabla = new DataTable();
            sourceData = tools.getVariable("sourceData");
           
            try
            {
                switch (sourceData)
                {
                    case "genia":
                        query = "SELECT id, nombre || ', ' || fecha_ini AS nombre, estado, fecha_ini,tipo, fecha_fin, biblioteca FROM ejemplares.inventario ORDER BY fecha_ini DESC; ";
                        inventariosTabla = (DataTable)dataBase.exeQuery(query, "DataTable");
                        break;
                    case "local":
                        query = "SELECT idInv AS id, (nombre + CAST(fecha_ini AS nvarchar)) AS nombre, estado, fecha_ini,tipo, fecha_fin, biblioteca FROM inventario ORDER BY fecha_ini DESC; ";
                        inventariosTabla = (DataTable)localDBase.exeQuery(query, "DataTable");
                        break;
                }
                
                foreach (DataRow row in inventariosTabla.Rows)
                {
                    Inventario inv = new Inventario();
                    inv.id = Convert.ToString(row["id"]);
                    inv.nombre = Convert.ToString(row["nombre"]);
                    inv.fecha_ini = Convert.ToDateTime(row["fecha_ini"]);
                    inv.estado = Convert.ToInt16(row["estado"]);
                    //inv.biblioteca = Convert.ToInt16(row["biblioteca"]);
                    // inv.tipo = Convert.ToInt16(row["tipo"]);

                    inventarios.Add(inv);
                }

                return inventarios;

            }
            catch //(Exception ex)
            {
                return null;
                //throw;
            }
        }
        /// <summary>
        /// Actualiza el estado de un ejemplar
        /// </summary>
        /// <param name="acq">Barcode del ejemplar</param>
        /// <param name="idInventario">Identificador del inventario en curso</param>
        /// <param name="estado">Nuevo estado del ejemplar</param>
        /// <param name="message">Mensaje que devuelve el proceso</param>
        public void UpdateFileToInventarioEnCurso(string acq, string idInventario, string estado, out string message)
        {
            message = "";
            string queryUpdate = "UPDATE ejemplares.inventario_en_curso SET estado =" + estado + " WHERE acq ='" + acq + "' AND id_inventario = '" + idInventario + "' ";
            try
            {
                dataBase.exeQuery(queryUpdate);
            }
            catch (Exception e)
            {
                message = e.Message;
            }
        }
        /// <summary>
        /// Busca el barcode asociado y actualiza el estado de una lista de tags
        /// </summary>
        /// <param name="tags">Lista de tags</param>
        /// <param name="idInventario">Identifidcador del inventario en curso</param>
        /// <param name="message">Mensaje que devuelve el proceso</param>
        /// <returns>El número de registros actualizados</returns>
        public int UpdateFileToInventarioEnCurso(List<string> tags, string idInventario, out string message)
        {
            message = "";
            int resp = 0;
            string queryUpdate = "";
            clsBaseDatos dataBase = new clsBaseDatos(Properties.Settings.Default.ConnStringPostgresGenia);

            if (tags.Count > 0)
            {
                queryUpdate = "UPDATE ejemplares.inventario_en_curso SET estado = 1 WHERE ";

                foreach (string tag in tags)
                {
                    string query = "SELECT acq FROM ejemplares.ejemplares WHERE id_tag = '" + tag + "'";
                    try
                    {
                        var acq = dataBase.exeQuery(query, "string");
                        if (acq.GetType() != typeof(string))
                            continue;
                        if (!String.IsNullOrEmpty(acq.ToString()))
                        {
                            queryUpdate += " acq ='" + acq + "' OR ";
                        }
                    }
                    catch
                    {
                    }
                }
                queryUpdate = queryUpdate.Substring(0, queryUpdate.Length - 3);
                queryUpdate += " AND id_inventario = '" + idInventario + "';";

                try
                {
                    int.TryParse(dataBase.exeQuery(queryUpdate), out resp);
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    resp = 0;
                }

            }
            return resp;
        }
        public int? UploadFileGeniaDb(List<string> tags, string fileName, string user, string library, string countTags, out string message)
        {

            int? resp = null;
            StringBuilder query = new StringBuilder();
            int intOut = 0;
            message = "";
            //insert file
            query.Append( "INSERT INTO ejemplares.file(name, time_stamp, biblioteca, usuario, num_reg) VALUES ('" + fileName + "', NOW(), '" + library + "', '" + user + "', " + countTags + ") RETURNING id;");
            string idFile = (string)dataBase.exeQuery(query.ToString(),"string");
            if (string.IsNullOrEmpty(idFile))
                return null;
            
            if (int.TryParse(idFile, out intOut))
                resp = intOut;
            else
                message += idFile;
            if (resp <0)
                return null;
            //insert tags
            query = new StringBuilder();
            query.Append("INSERT INTO ejemplares.inventory_files(id_tag, time_stamp, id_file) VALUES ");
            //"('', '', ),('', '', );";
            foreach(string tag in tags)
            {
                query.Append("('" + tag + "', NOW(), " + idFile + "),");
            }
            query.Remove(query.Length - 1, 1);
            query.Append(";");

            string insertResp = dataBase.exeQuery(query.ToString());
            if (string.IsNullOrEmpty(insertResp))
                return null;
            intOut = 0;
            if (int.TryParse(insertResp, out intOut))
                resp = intOut;
            else
                message += insertResp;
            return resp;
        }
        /// <summary>
        /// Busca el estado a inventariado de la lista enviada</summary>
        /// <param name="tags">Lista de tags</param>
        /// <param name="idInventario">Identifidcador del inventario actual</param>
        /// <param name="message">Mensaje que devuelve el proceso</param>
        /// <returns>El número de registros actualizados</returns>
        public int? UpdateFileToInventarioEnCursoExistencias(List<string> tags, string fileName, string idInventario, string user, string library, string countTags, out string message)
        {
            message = "";
            int intOut = 0;
            int? resp = null;
            StringBuilder queryUpdate = new StringBuilder();
            clsBaseDatos dataBase = new clsBaseDatos(Properties.Settings.Default.ConnStringPostgresGenia);

            if (tags.Count > 0)
            {
                try
                {
                    sourceData = tools.getVariable("sourceData");
                    //insert file
                    queryUpdate.Append("INSERT INTO ejemplares.file(name, time_stamp, biblioteca, usuario, num_reg) VALUES ('" + fileName.Replace("\\", "/") + "', NOW(), '" + library + "', '" + user + "', " + countTags + ") RETURNING id;");
                    string idFile = (string)dataBase.exeQuery(queryUpdate.ToString(), "string");
                    if (string.IsNullOrEmpty(idFile))
                        return null;

                    if (int.TryParse(idFile, out intOut))
                        resp = intOut;
                    else
                    { 
                        message += idFile;
                        //return null;
                    }
                    if (resp < 0)
                        return null;
                    resp = 0;
                    string startQuery = "";
                    int respAux = 0;

                    queryUpdate = new StringBuilder();
                    
                    //queryUpdate.Append("UPDATE ejemplares.inventario_en_curso SET estado = 1 ");
                    queryUpdate.Append("WHERE id_inventario = " + idInventario + " AND acq IN (SELECT acq FROM ejemplares.ejemplares WHERE id_tag IN (");

                    foreach (string tag in tags)
                    {
                        queryUpdate.Append("'" + tag + "',");
                    }
                    queryUpdate.Remove(queryUpdate.Length - 1, 1);
                    queryUpdate.Append("));");
                    switch (sourceData)
                    {
                        case "genia":
                            startQuery = "UPDATE ejemplares.inventario_en_curso SET estado = 1 ";
                            if (int.TryParse(dataBase.exeQuery(startQuery + queryUpdate.ToString()), out respAux))
                                resp = respAux;
                            break;
                        case "local":
                            //startQuery = "UPDATE inventario_en_curso SET estado = 1 ";
                            //if (int.TryParse(localDBase.exeQuery(startQuery + queryUpdate.ToString(0, queryUpdate.Length - 1) + ");"), out respAux))
                            //    resp = respAux;
                            break;
                    }
                    if(resp>0)
                    {
                        queryUpdate = new StringBuilder();
                        queryUpdate.Append("UPDATE ejemplares.file SET efectives=" + resp.ToString() + " WHERE id = " + idFile + ";");
                        if (!int.TryParse(dataBase.exeQuery(queryUpdate.ToString()), out respAux))
                        {
                            message += ", efectives register: " + respAux.ToString();
                        }
                    }
                    
                }
                catch (Exception ex)
                {
                    message = ex.Message + ", Stacke Trace: "+ ex.StackTrace;
                    resp = 0;
                }

            }
            return resp;
        }
        public int? insertExistencias(DataTable items, Dictionary<string, int> columns, out string fecha, out string mensaje)
        {
            sourceData = tools.getVariable("sourceData");
            fecha = "";
            int? resp = null;
            mensaje = "";
            if (!cleanExistencias().HasValue)
                return resp;
            string input = "";
            bool firstRow = true;
            //List<string> columns = new List<string>();
            StringBuilder sbQuery = new StringBuilder();
            //object[] arrayCol;
            //StringBuilder sbVal = new StringBuilder();
            string respQuery = "";
            int respAux = 0;

            switch (sourceData)
            {
                case "local":

                    //
                    string queryIni = "INSERT INTO existencias( ";
                    
                    resp = 0;
                    try 
                    { 
                    foreach (DataRow row in items.Rows)
                    {
                        sbQuery = new StringBuilder();
                        if (firstRow && Properties.Settings.Default.db_colexs_head)
                        {
                            foreach (var item in columns)
                            {
                                //row[arrCol[0]]
                                queryIni += item.Key + ", ";
                            }
                            queryIni = queryIni.Substring(0, queryIni.Length - 2);
                            queryIni += ") VALUES ";

                            firstRow = false;
                            continue;
                        }
                        sbQuery.Append(queryIni + "(");
                        //id, barcode, tag_id, nombre, descripcion, ubicacion, clasificacion, categoria, volumen, ejemplar, tomo, prestado
                        foreach (var item in columns)
                        {
                            if (item.Value == Properties.Settings.Default.db_colexs_prestado)
                                if (item.Key == Properties.Settings.Default.db_colexs_prestado_true)
                                    sbQuery.Append("'True',");
                                else
                                    sbQuery.Append("'False',");
                            else
                            {
                                string aux = row[item.Value - 1].ToString().Trim();
                                //if (aux.Contains("'"))
                                //    aux = "";
                                if (aux.Length > 100)
                                    aux = aux.Substring(0, 100);
                                if (string.IsNullOrEmpty(aux))
                                    sbQuery.Append("NULL,");
                                else
                                    sbQuery.Append("'" + aux.Replace("'", "\"") + "',");
                            }
                            
                            
                        }
                        sbQuery.Remove(sbQuery.Length - 1, 1);
                        sbQuery.Append(");");
                        string respQueryAux =localDBase.exeQuery(sbQuery.ToString());
                        int respInt = 0;
                        if(string.IsNullOrEmpty(respQueryAux))
                        {
                            mensaje += " row:" + resp;
                            resp = null;
                            break;
                        }
                        if(!int.TryParse(respQueryAux, out respInt))
                        {
                            mensaje += respQueryAux + " row:" + resp;
                            resp = null;
                            break;
                        }
                        if(respInt != 1)
                        {
                            mensaje += "Error, row:" + resp;
                            resp = null;
                        }

                        resp++;

                    }
                    //
                    }
                    catch(Exception ex)
                    {
                        mensaje += ", " + ex.Message;
                    }
                    break;
                case "genia":
                    try
                    {
                        foreach (DataRow row in items.Rows)
                        {

                            if (firstRow && Properties.Settings.Default.db_colexs_head)
                            {

                                //sbVal.Append(") VALUES (");
                                foreach (var item in columns)
                                {
                                    //row[arrCol[0]]
                                    sbQuery.Append(item.Key + ", ");
                                }
                                input = sbQuery.ToString(0, sbQuery.Length - 2);
                                sbQuery = new StringBuilder(input);
                                sbQuery.Append(") VALUES ");
                                firstRow = false;

                                continue;
                            }
                            sbQuery.Append("(");
                            //id, barcode, tag_id, nombre, descripcion, ubicacion, clasificacion, categoria, volumen, ejemplar, tomo, prestado
                            foreach (var item in columns)
                            {
                                if (item.Value == Properties.Settings.Default.db_colexs_prestado)
                                    if (item.Key == Properties.Settings.Default.db_colexs_prestado_true)
                                        sbQuery.Append("'True',");
                                    else
                                        sbQuery.Append("'False',");
                                else
                                {
                                    string aux = row[item.Value - 1].ToString().Trim();
                                    //if (aux.Contains("'"))
                                    //    aux = "";

                                    if (string.IsNullOrEmpty(aux))
                                        sbQuery.Append("NULL,");
                                    else
                                        sbQuery.Append("'" + aux.Replace("'", "\"") + "',");
                                }
                            }
                            //eliminar "," y agregar "),"
                            input = sbQuery.ToString(0, sbQuery.Length - 1) + "),";
                            sbQuery = new StringBuilder(input);
                            //string aux = row[item.Value].ToString(); 
                            //sbQuery.Append("('" + string.Join("','", aux) + "'), ");


                            //INSERT INTO ejemplares.existencias(barcode, nombre, fecha) VALUES ('1', 'uno', NOW()),('2', 'dos', NOW()),('3', 'tres', NOW()),('4', 'cuatro', NOW()),('5', 'cinco', NOW());
                            //fecha = fechaTools.FormatoInsertFecha(DateTime.Now);
                            //sbQuery.Append("('" + string.Join("','", "") + "', '" + fecha + "'), ");


                        }
                        input = sbQuery.ToString(0, sbQuery.Length - 1) + ";";
                        sbQuery = new StringBuilder(input);

                        respQuery = dataBase.exeQuery("INSERT INTO ejemplares.existencias( " + sbQuery.ToString());
                        if (int.TryParse(respQuery, out respAux))
                            resp = respAux;
                        else
                            mensaje = respQuery;
                    }
                    catch (Exception ex)
                    {
                        mensaje += ", " + ex.Message;
                    }
                    break;
            }
            

            return resp;
        }
        private int? cleanExistencias()
        {
            int? resp = null;
            sourceData = tools.getVariable("sourceData");
            string sQuery = "";
            string sNumReg = "";

            try
            {
                switch (sourceData)
                {
                    case "genia":
                        sQuery = "DELETE FROM ejemplares.existencias; ALTER SEQUENCE ejemplares.autonum_existencias RESTART WITH 0;";
                        sNumReg = dataBase.exeQuery(sQuery);
                        break;
                    case "local":
                        sQuery = "DELETE FROM existencias; ";
                        sNumReg = localDBase.exeQuery(sQuery);
                        sQuery = "ALTER TABLE existencias ALTER COLUMN [idExist] IDENTITY (1,1);";
                        sNumReg = localDBase.exeQuery(sQuery);
                        break;
                }
                int respAux = 0;
                if (int.TryParse(sNumReg, out respAux))
                    resp = respAux;
            }
            catch{}
            return resp;
        }
        /// <summary>
        /// Crea un nuevo inventario
        /// </summary>
        /// <param name="nombre">Nombre del inventario</param>
        /// <param name="fechaExistencias">Si es tipo 2, es la fecha de carga de existencias</param>
        /// <param name="tipo">1: base de datos; 2:carga archivo con existencias</param>
        /// <param name="message">mensaje que devuelve el proceso</param>
        /// <returns>Identificador del inventario creado</returns>
        public string CreateInventory(string nombre, string fechaExistencias, int biblioteca, int tipo, out string message)
        {
            message = "";
            string resp = "";
            int idresult = 0;
            //string respAux = "";
            sourceData = tools.getVariable("sourceData");
            try
            {
                if (string.IsNullOrEmpty(fechaExistencias))
                    fechaExistencias = "NULL";
                else
                    fechaExistencias = "'" + fechaExistencias + "'";
                switch (sourceData)
                {
                    case "genia":
                        string[] queries = new string[3];
                        queries[0] = "INSERT INTO ejemplares.inventario (nombre, fecha_ini, estado, biblioteca, tipo, fecha_existencias) VALUES ('" + nombre + "', NOW(), 0, 1 ,"+tipo+", " + fechaExistencias + ") RETURNING id;";
                        queries[1] = "SELECT to_char(MAX(hora_file), 'yyyy-mm-dd HH24:MI:') FROM ejemplares.ejemplares;";
                        queries[2] = "INSERT INTO ejemplares.inventario_en_curso (acq, estado, id_inventario) SELECT acq, 0, {0} FROM ejemplares.ejemplares WHERE hora_file >= '{1}00' ORDER BY clasificacion;";
                        string[] respAux = dataBase.transactionExeQuery(queries, 2);

                        int numReg = 0;
                        if (int.TryParse(respAux[0], out numReg))
                            resp = respAux[0];
                        else
                            message = respAux[0];
                        break;
                    case "local":
                        string respString = localDBase.insertInventario(nombre, biblioteca);

                        //query = "INSERT INTO inventario (nombre, fecha_ini, estado, biblioteca, tipo, fecha_existencias) VALUES ('{0}', GETDATE(), 0, 1 ,{1}, {2});";
                        //string respString = localDBase.exeQuery(String.Format(query, nombre, tipo, fechaExistencias));

                        if (!int.TryParse(respString, out idresult))
                        {
                            message = respString;
                            return null;
                            //break;
                        }
                        //resp = idresult;
                        break;
                }
                
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return resp;
        }
        public int? InsertInventarioCurso_existencias(string idInventario, out string message)
        {
            int? resp = null;
            message = "";
            string query = "";
            string respAux = "";
            sourceData = tools.getVariable("sourceData");
            try
            {
                switch (sourceData)
                {
                    case "genia":
                       string fecUpdate = dataBase.exeQuery("SELECT to_char(MAX(hora_file), 'yyyy-mm-dd HH24:MI:SS') FROM ejemplares.ejemplares;");
                       //query = "INSERT INTO ejemplares.inventario_en_curso (acq, estado, id_inventario) SELECT epc,0,{0} FROM ejemplares.existencias WHERE epc IS NOT NULL ORDER BY id;";
                       query = "INSERT INTO ejemplares.inventario_en_curso (acq, estado, id_inventario) SELECT acq, 0, {0} FROM ejemplares.ejemplares WHERE hora_file >= '" + fecUpdate.Substring(0,17) + "00' ORDER BY clasificacion;";
                        respAux = dataBase.exeQuery(String.Format(query, idInventario));
                        break;
                    case "local":
                        query = "INSERT INTO inventario_en_curso (acq, estado, id_inventario) SELECT epc,0,{0} FROM existencias WHERE epc IS NOT NULL ORDER BY idExist;";
                        respAux = localDBase.exeQuery(String.Format(query, idInventario));
                        break;
                }
                
                int numReg = 0;
                if (int.TryParse(respAux, out numReg))
                    resp = numReg;
                else
                    message = respAux;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return resp;
        }
        public Dictionary<string, int> inventorySummary(string idInventario)
        {
            Dictionary<string, int> dictionaryResult = new Dictionary<string, int>();
            sourceData = tools.getVariable("sourceData");
            string resp = "";
            switch (sourceData)
            {
                case "genia":
                    int total = 0;
                    int inventoried = 0;
                    int loaned = 0;
                    int noAsociated = 0;

                    resp = dataBase.exeQuery("SELECT COUNT(*) FROM ejemplares.inventario_en_curso WHERE id_inventario = " + idInventario + ";");
                    if (!int.TryParse(resp, out total))
                        return null;
                    resp = dataBase.exeQuery("SELECT COUNT(*) FROM ejemplares.inventario_en_curso ic LEFT JOIN ejemplares.ejemplares e ON e.acq = ic.acq WHERE id_inventario = " + idInventario + " AND e.id_tag is null;");
                    if (!int.TryParse(resp, out noAsociated))
                        return null;
                    resp = dataBase.exeQuery("SELECT COUNT(*) FROM ejemplares.inventario_en_curso ic LEFT JOIN ejemplares.ejemplares e ON e.acq = ic.acq WHERE id_inventario = " + idInventario + " AND ic.estado = 1;");
                    if (!int.TryParse(resp, out inventoried))
                        return null;
                    resp = dataBase.exeQuery("SELECT COUNT(*) FROM ejemplares.inventario_en_curso ic LEFT JOIN ejemplares.ejemplares e ON e.acq = ic.acq WHERE id_inventario = " + idInventario + " AND ic.estado = 0 AND e.estado_activador = 15; ");
                    if (!int.TryParse(resp, out loaned))
                        return null;
                    if (noAsociated + inventoried + loaned > total)
                        return null;
                    if (inventoried > total)
                        return null;
                    dictionaryResult.Add("0", total - inventoried - noAsociated - loaned);
                    dictionaryResult.Add("1", inventoried);
                    dictionaryResult.Add("2", loaned);
                    dictionaryResult.Add("3", noAsociated);
                    break;
                case "local":
                    resp = localDBase.exeQuery("SELECT DISTINCT(estado), count(estado) FROM inventario_en_curso WHERE id_inventario= " + idInventario + " GROUP BY estado;");
                    break;
            }

            return dictionaryResult;
        }
        
        public DataTable copiesSummary(string idInventario)
        {
            sourceData = tools.getVariable("sourceData");
            object resp = new object();
            switch (sourceData)
            {
                case "genia":
                    resp = dataBase.exeQuery("SELECT e.ubicacion, COUNT(*) FROM ejemplares.inventario_en_curso ic LEFT JOIN ejemplares.ejemplares e ON e.acq = ic.acq WHERE id_inventario = " + idInventario + " GROUP BY e.ubicacion; ", "DataTable");
                    //resp = dataBase.exeQuery("SELECT DISTINCT(estado), count(estado) FROM ejemplares.inventario_en_curso WHERE id_inventario= " + idInventario + " GROUP BY estado ORDER BY estado;", "DataTable");
                    break;
                case "local":
                    resp = localDBase.exeQuery("SELECT DISTINCT(estado), count(estado) FROM inventario_en_curso WHERE id_inventario= " + idInventario + " GROUP BY estado;", "DataTable");
                    break;
            }

            if (resp.GetType() == typeof(DataTable))
                return (DataTable)resp;
            else
                return null;
        }
        public DataTable MissingcopiesSummary(string idInventario)
        {
            sourceData = tools.getVariable("sourceData");
            object resp = new object();
            switch (sourceData)
            {
                case "genia":
                    resp = dataBase.exeQuery("SELECT e.ubicacion, COUNT(*) FROM ejemplares.inventario_en_curso ic LEFT JOIN ejemplares.ejemplares e ON e.acq = ic.acq WHERE id_inventario = " + idInventario + " AND ic.estado = 0 AND e.id_tag IS NOT NULL GROUP BY e.ubicacion; ", "DataTable");
                    //resp = dataBase.exeQuery("SELECT DISTINCT(estado), count(estado) FROM ejemplares.inventario_en_curso WHERE id_inventario= " + idInventario + " GROUP BY estado ORDER BY estado;", "DataTable");
                    break;
                case "local":
                    resp = localDBase.exeQuery("SELECT DISTINCT(estado), count(estado) FROM inventario_en_curso WHERE id_inventario= " + idInventario + " GROUP BY estado;", "DataTable");
                    break;
            }

            if (resp.GetType() == typeof(DataTable))
                return (DataTable)resp;
            else
                return null;
        }
        public Dictionary<string,string> inventoryCategories(string idInventario)
        {
            object catTotal = dataBase.exeQuery("SELECT DISTINCT(ex.ubicacion), count(ex.ubicacion) FROM ejemplares.inventario_en_curso ic "
                + "LEFT JOIN ejemplares.existencias ex ON ex.epc = ic.acq "
                + "WHERE id_inventario= " + idInventario + " GROUP BY ex.ubicacion;","DataTable");
            if (catTotal.GetType() != typeof(DataTable))
                return null;

            object catInv = dataBase.exeQuery("SELECT DISTINCT(ex.ubicacion), count(ex.ubicacion) FROM ejemplares.inventario_en_curso ic "
                + "LEFT JOIN ejemplares.existencias ex ON ex.epc = ic.acq "
                + "WHERE id_inventario= " + idInventario + " AND ic.estado = 1 GROUP BY ex.ubicacion;", "DataTable");
            if (catInv.GetType() != typeof(DataTable))
                return null;

            DataTable catTotaldT = (DataTable)catTotal;
            DataTable catInvdT = (DataTable)catInv;

            Dictionary<string,string> categoryData = new Dictionary<string,string>();
            
            foreach(DataRow rowTot in catTotaldT.Rows)
            {
                categoryData.Add(rowTot[0].ToString(), rowTot[1].ToString());
            }
            foreach (DataRow rowInv in catInvdT.Rows)
            {
                string datoActual = categoryData[rowInv[0].ToString()];
                categoryData[rowInv[0].ToString()] = datoActual + ", " + rowInv[1].ToString();
            }
            return categoryData;
            //if (resp.GetType() == typeof(DataTable))
            //    return (DataTable)resp;
            //else
            //    return null;
        }
        public DataTable getInventoryFullExistencias(string idInventario)
        {
            startLenguage();
            object resp = dataBase.exeQuery("SELECT e.id, barcode, e.epc, e.nombre, e.descripcion, e.ubicacion, e.usuario, e.observacion, "
                + "CASE WHEN ic.estado=1 THEN '" + Properties.Resources.dhbrd_inventoried + "' WHEN ic.estado=0 THEN '" + Properties.Resources.dhbrd_non_inventoried + "' ELSE 'other' END, ic.fecha "
                + "FROM ejemplares.inventario_en_curso ic "
                + "LEFT JOIN ejemplares.existencias e ON e.epc = ic.acq "
                + "WHERE id_inventario= " + idInventario + " ORDER BY e.id;", "DataTable");

            if (resp.GetType() == typeof(DataTable))
                return (DataTable)resp;
            else
                return null;
        }
        public DataTable getDescriptionInventory(string idInventory)
        {
            object resp = dataBase.exeQuery("SELECT nombre, fecha_ini, estado, fecha_fin, biblioteca, tipo, fecha_existencias FROM ejemplares.inventario WHERE id = " + idInventory + ";", "DataTable");
            if (resp.GetType() == typeof(DataTable))
                return (DataTable)resp;
            else
                return null;
        }
        public DataTable getInventoryList()
        {
            object resp = dataBase.exeQuery("SELECT i.id, i.nombre, fecha_ini, estado, fecha_fin, b.nombre FROM ejemplares.inventario i LEFT JOIN ejemplares.biblioteca b ON biblioteca = b.id ORDER BY fecha_ini DESC;", "DataTable");
            if (resp.GetType() == typeof(DataTable))
                return (DataTable)resp;
            else
                return null;
        }
        public DataTable getInventoryReport(string idInventario)
        {
            string query = "SELECT e.acq AS BARCODE, e.id_tag AS EPC, i.id AS ID_INVENTARIO, CASE WHEN e.id_tag IS NULL THEN 'NO ASOCIADO' ELSE CASE WHEN i.estado=0 THEN 'NO INVENTARIADO' WHEN i.estado=1 THEN 'INVENTARIADO'  WHEN i.estado=2 THEN 'PRESTADO'  ELSE 'NA'  END END AS ESTADO, "
                + "e.clasificacion AS CLASIFICACION, split_part(e.ubicacion,'-',2) AS COLECCION, split_part(e.ubicacion,'-',1) AS BIBLIOTECA, e.titulo AS TITULO, e.autor AS AUTOR "
                + "FROM ejemplares.ejemplares e "
                + "LEFT JOIN ejemplares.inventario_en_curso i ON i.acq = e.acq AND i.id_inventario = " + idInventario + " "
                + "ORDER BY e.clasificacion;";

            object resp = dataBase.exeQuery(query, "DataTable");
            if (resp.GetType() == typeof(DataTable))
                return (DataTable)resp;
            else
                return null;
        }
        public DataTable getMissingReport(string idInventario)
        {
            string query = "SELECT e.acq AS BARCODE, e.id_tag AS EPC, i.id AS ID_INVENTARIO, CASE WHEN e.id_tag IS NULL THEN 'NO ASOCIADO' ELSE CASE WHEN i.estado=0 THEN 'NO INVENTARIADO' WHEN i.estado=1 THEN 'INVENTARIADO'  WHEN i.estado=2 THEN 'PRESTADO'  ELSE 'NA'  END END AS ESTADO, "
                + "e.clasificacion AS CLASIFICACION, split_part(e.ubicacion,'-',2) AS COLECCION, split_part(e.ubicacion,'-',1) AS BIBLIOTECA, e.titulo AS TITULO, e.autor AS AUTOR "
                + "FROM ejemplares.ejemplares e "
                + "LEFT JOIN ejemplares.inventario_en_curso i ON i.acq = e.acq AND i.id_inventario = " + idInventario + " "
                + "WHERE i.estado = 0 AND e.id_tag IS NOT NULL "
                + "ORDER BY e.clasificacion;";

            object resp = dataBase.exeQuery(query, "DataTable");
            if (resp.GetType() == typeof(DataTable))
                return (DataTable)resp;
            else
                return null;
        }
        public StringBuilder getInventoryStringBuilder(string idInventario)
        {
            string query = "SELECT acq FROM ejemplares.inventario_en_curso WHERE estado = 1 AND id_inventario = " + idInventario + ";";

            object resp = dataBase.exeQuery(query, "DataTable");

            if (resp.GetType() != typeof(DataTable))
                return null;
            DataTable reporte = (DataTable)resp;
            StringBuilder respString = new StringBuilder();
            foreach(DataRow row in reporte.Rows)
            {
                respString.Append(row[0].ToString());
                respString.Append(Environment.NewLine);
            }
            if (respString.Length > 0)
                respString.Remove(respString.Length - 1, 1);
            return respString;
        }

        public bool DeleteInventory(string idInventory, out string message)
        {
            message = "";
            string resp = "";
            int respInt = 0;
            try
            {
                string queryUpdate = "DELETE FROM ejemplares.inventario_en_curso WHERE id_inventario = " + idInventory + ";";
                resp = dataBase.exeQuery(queryUpdate);
                if(!int.TryParse(resp,out respInt))
                {
                    message = "Error DELETE FROM inventario_en_curso: " + resp;
                    return false;
                }
                queryUpdate = "DELETE FROM ejemplares.inventario WHERE id = " + idInventory + ";";
                resp = dataBase.exeQuery(queryUpdate);
                if (!int.TryParse(resp, out respInt))
                {
                    message = "Error DELETE FROM inventario: " + resp;
                    return false;
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            return true;
        }
        public bool DeleteFileRegisters(out string message)
        {
            message = "";
            string resp = "";
            int respInt = 0;
            try
            {
                string queryUpdate = "DELETE FROM ejemplares.inventory_files;";
                resp = dataBase.exeQuery(queryUpdate);
                if (!int.TryParse(resp, out respInt))
                {
                    message = "Error DELETE FROM inventory_files: " + resp;
                    return false;
                }
                queryUpdate = "DELETE FROM ejemplares.file;";
                resp = dataBase.exeQuery(queryUpdate);
                if (!int.TryParse(resp, out respInt))
                {
                    message = "Error DELETE FROM file: " + resp;
                    return false;
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            return true;
        }
    }
}
