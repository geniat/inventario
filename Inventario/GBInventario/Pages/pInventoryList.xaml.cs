﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using GBInventario.Controls;
using System.Xml;

namespace GBInventario.Pages
{
    /// <summary>
    /// Lógica de interacción para pInventoryList.xaml
    /// </summary>
    public partial class pInventoryList : Page
    {
        //cDatabaseGenia geniaDataBase = new cDatabaseGenia();
        GeniaWebServices.Service1 geniaWebService = new GeniaWebServices.Service1();
        cTools tools = new cTools();

        public pInventoryList()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            InventoriesLoad();
        }
        private void InventoriesLoad()
        {
            try
            {
                //DataTable inventories = geniaDataBase.getInventoryList();
                string response = geniaWebService.getInventories();
                if (string.IsNullOrEmpty(response))
                {
                    return;
                }
                if (response == "<DocumentElement />")
                {
                    return;
                }
                XmlDocument xm = new XmlDocument();
                xm.LoadXml(response);
                XmlNode root = xm.SelectNodes("DocumentElement").Item(0);
                XmlNodeList inventories = root.SelectNodes("inventories");

                foreach (XmlNode row in inventories)
                {
                    Button btnAux = new Button();
                    btnAux.Click += btnAux_Click;
                    btnAux.Tag = row["id"].InnerText + " - " + row["nombre"].InnerText;
                    btnAux.ToolTip = Properties.Resources.inv_select;
                    string state = "";
                    if (string.IsNullOrEmpty(row["tipo"].InnerText))
                        state = Properties.Resources.inv_state_open + ", " + row["estado"].InnerText;
                    else
                        state = Properties.Resources.inv_state_close;

                    btnAux.Content = row["nombre"].InnerText + " " + "(" + state + ")";
                    btnAux.Style = (Style)Application.Current.Resources["LinkButton"];

                    spInventoryList.Children.Add(btnAux);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message + "\n\n" + ex.StackTrace);
            }
        }

        void btnAux_Click(object sender, RoutedEventArgs e)
        {
            
            Button btnAux = (Button)sender;

            string invSel = btnAux.Tag.ToString();

            Properties.Settings.Default.last_id_inventory = invSel;
            Properties.Settings.Default.Save();
            tools.setVariable("invSel", invSel);
            this.NavigationService.Navigate(new Uri("/Pages/pDashboard.xaml", UriKind.Relative));
        }
    }
}
