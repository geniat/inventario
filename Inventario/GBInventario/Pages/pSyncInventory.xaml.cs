﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GBInventario.Controls;
using System.Xml;
using System.ComponentModel;

namespace GBInventario.Pages
{
    /// <summary>
    /// Lógica de interacción para pSyncInventory.xaml
    /// </summary>
    public partial class pSyncInventory : Page
    {
        #region variables inventario
        List<string> detectedTags = new List<string>();
        List<string> newInventoried = new List<string>();
        List<string> detectedBarcodes = new List<string>();
        List<string> updatedBarcodes = new List<string>();
        int noresponsecount = 0;
        #endregion

        #region variables timer
        //TimerHelper _timerHelper;
        System.Timers.Timer timer;
        #endregion

        #region clases internas
        cTools tools = new cTools();

        Controls.cFiles archivos = new cFiles();
        //Controls.cDatabaseGenia dbGenia = new Controls.cDatabaseGenia();
        GeniaWebServices.Service1 geniaWebService = new GeniaWebServices.Service1();
        Controls.cFiles files = new Controls.cFiles();
        MainWindow parentWindow;
        #endregion

        string invSel = "";
        string id_inventario = "";
        public List<string> missingList;
        bool stateStartButton = false;
        Brush inventoriedColor = Brushes.LightGreen;
        Brush nonInventroriedColor = Brushes.IndianRed;
        Brush pending = Brushes.Yellow;
        SolidColorBrush nonProgramed = (SolidColorBrush)(new BrushConverter().ConvertFrom("#c9ba1a"));
        string barcodeStart = "";
        string barcodeEnd = "";

        public pSyncInventory()
        {

            InitializeComponent();

            #region inicializa idioma
            var culture = System.Globalization.CultureInfo.CurrentCulture;//.GetCultureInfo("en-EU");//.CurrentCulture;
            Dispatcher.Thread.CurrentCulture = culture;
            Dispatcher.Thread.CurrentUICulture = culture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = culture;
            #endregion

            #region inicializa timer
            //_timerHelper = new TimerHelper();
            //_timerHelper.TimerEvent += (timer, state) => Timer_Elapsed();
            //StartTimer();
            timer = new System.Timers.Timer();
            timer.Interval = 1000;
            timer.Elapsed += timer_Elapsed;

            #endregion
        }
        void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {

            #region inicializa idioma
            var culture = System.Globalization.CultureInfo.CurrentCulture;//.GetCultureInfo("en-EU");//.CurrentCulture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = culture;
            #endregion
            //if (uc_sync.isSyncActive)
            //    return;

            timer.Enabled = false;
            timer.Stop();
            //if (!uc_sync.existOpenFile)
            //{
            //    string message = string.Format(Properties.Resources.main_message_create_file, Properties.Resources.menu_files_sync);
            //    tbMessage.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Send, new UpdateTextHandler(update_message), message);
            //}
            loadInfoRfid();
            timer.Enabled = true;
            timer.Start();
        }
        public void startTimer()
        {
            timer.Enabled = true;
            timer.Start();
        }
        public void stopTimer()
        {
            timer.Enabled = false;
            timer.Stop();
        }
        #region métodos timer
        //void StartTimer()
        //{

        //    _timerHelper.Start(TimeSpan.FromSeconds(2));
        //    //System.Threading.Thread.Sleep(TimeSpan.FromSeconds(12));

        //}
        //void StopTimer()
        //{
        //    _timerHelper.Stop(new TimeSpan(0, 0, 1));
        //}
        //void Timer_Elapsed()
        //{
        //    loadInfoRfid();

        //}
        #endregion

        #region métodos inventario
        private void loadInfoRfid()
        {
            try
            {
                copiesViewer.Dispatcher.Invoke(new Action(delegate
                {
                    List<string> tags = new List<string>();
                    tags.AddRange(userControlRfid.reader.tags);
                    if (tags.Count > 0)
                    {
                        userControlRfid.reader.tags.Clear();
                        foreach (string tag in tags)
                        {
                            if (detectedTags.Contains(tag))
                                continue;
                            if (detectedBarcodes.Contains(tag))
                                continue;
                            object aux = copiesViewer.FindName("N_" + tag);
                            if (aux == null)
                                continue;
                            if (aux.GetType() != typeof(Button))
                                continue;
                            Button copy = (Button)aux;
                            copy.Background = pending;
                            detectedBarcodes.Add(copy.Tag.ToString());
                        }
                        
                       
                    }
                    else
                    {
                        noresponsecount++;
                        if (detectedBarcodes.Count > 0 && updatedBarcodes.Count == 0)
                        {
                            noresponsecount=0;
                            updatedBarcodes.AddRange(detectedBarcodes);
                            detectedBarcodes.Clear();
                            geniaWebService.UpdateFileDynamicallyAsync(updatedBarcodes.ToArray(), id_inventario);
                        }
                        if (newInventoried.Count > 0)
                        {
                            noresponsecount=0;
                            foreach (string copy in newInventoried)
                            {
                                if (!copy.StartsWith("1|"))
                                    continue;
                                string tag = copy.Split('|')[1];
                                //updatedBarcodes.Add("");
                                object aux = copiesViewer.FindName("N_" + tag);
                                if (aux == null)
                                    continue;
                                if (aux.GetType() != typeof(Button))
                                    continue;
                                Button copyButton = (Button)aux;

                                copyButton.Background = inventoriedColor;
                            }
                            newInventoried.Clear();
                            updatedBarcodes.Clear();
                        }
                        if (noresponsecount > 50)
                        {
                            noresponsecount = 0;
                            //clear lists
                            if (updatedBarcodes.Count>0)
                            geniaWebService.UpdateFileDynamicallyAsync(updatedBarcodes.ToArray(), id_inventario);
                            else
                            {
                                updatedBarcodes.Clear();
                            }
                        }
                    }
                }));
                loadView();
                string DeviceState = userControlRfid.state;
                if (string.IsNullOrEmpty(DeviceState))
                {
                    tbMessage.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Send, new UpdateTextHandler(update_message), Properties.Resources.main_message_attemp_conn);
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        delegate void UpdateTextHandler(string updatedText);
        private void loadView()
        {
            tblTagsDet.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Send, new UpdateTextHandler(update_textBox), detectedTags.Count.ToString());
        }
        void update_message(string updatedText)
        {
            tbMessage.Text = updatedText;
        }
        void update_textBox(string updatedText)
        {
            tblTagsDet.Text = updatedText;
        }

        delegate void UpdateTextFileNameHandler(string update_textBoxFileName);
        #endregion

        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {
            unload();
        }
        public void unload()
        {
            userControlRfid.unload();
            //StopTimer();
            timer.Enabled = false;
            timer.Stop();
            timer.Dispose();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                parentWindow = Window.GetWindow(this) as MainWindow;
                string mensajeIdInv = "";
                invSel = findInventoryBdd(out mensajeIdInv);
                id_inventario = invSel.Split('-')[0].Trim();
                geniaWebService.UpdateFileDynamicallyCompleted += GeniaWebService_UpdateFileDynamicallyCompleted;
                loadView();
            }
            catch (Exception ex)
            {
                WpfMessageBox.Show(ex.Message);
            }
        }

        private void GeniaWebService_UpdateFileDynamicallyCompleted(object sender, GeniaWebServices.UpdateFileDynamicallyCompletedEventArgs e)
        {
            string result = e.Result;
            if (string.IsNullOrEmpty(result))
                return;
            if (result == "<DocumentElement />")
                return;
            XmlDocument xm = new XmlDocument();
            xm.LoadXml(result);
            XmlNode root = xm.SelectNodes("DocumentElement").Item(0);

            XmlNodeList copies = root.SelectNodes("copies");
            foreach(XmlNode copy in copies)
            {
                newInventoried.Add(copy["estado"].InnerText + "|" + copy["id_tag"].InnerText);
                if (copy["estado"].InnerText == "1")
                    detectedTags.Add(copy["id_tag"].InnerText);
            }
        }

        public static object ExitFrame(object f)
        {
            ((System.Windows.Threading.DispatcherFrame)f).Continue = false;

            return null;
        }
        private string findInventoryBdd(out string message)
        {
            message = "";
            string response = "";
            XmlDocument xm = new XmlDocument();
            XmlNode root;
            try
            {
                invSel = Properties.Settings.Default.last_id_inventory;
                if (invSel == "No es posible recuperar información de inventarios")
                    invSel = "";
                if (string.IsNullOrEmpty(invSel))
                {
                    response = geniaWebService.getInventories();

                    if (string.IsNullOrEmpty(response))
                        return null;
                    if (response == "<DocumentElement />")
                        return null;

                    xm = new XmlDocument();
                    xm.LoadXml(response);
                    root = xm.SelectNodes("DocumentElement").Item(0);

                    XmlNodeList inventories = root.SelectNodes("inventories");

                    if (inventories.Count <= 0)
                    {
                        WpfMessageBox.Show(Properties.Resources.database_no_inventory);
                        return null;
                    }
                    invSel = inventories[0]["id"].InnerText + " - " + inventories[0]["nombre"].InnerText;
                    Properties.Settings.Default.last_id_inventory = invSel;
                    Properties.Settings.Default.Save();
                    tools.setVariable("invSel", invSel);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message + "\n\nStackTrace: " + ex.StackTrace;
            }
            return invSel;
        }
        /// <summary>
        /// obsoleto JUCO
        /// </summary>
        /// <param name="message"></param>
        /// <param name="idInventario"></param>
        /// <returns></returns>
        private string setFileInvGen(out string message, string idInventario)
        {
            string FileInvGen = Properties.Settings.Default.last_file_missing;
            //string FileInvGenSettings = Properties.Settings.Default.last_file_gen;
            //bool FileSettingsExists = false;
            message = "";

            try
            {
                List<string> Listfiles = files.getFiles(Properties.Settings.Default.path_genia, Properties.Settings.Default.file_type_general);

                if (Listfiles.Count > 0)
                {
                    //no estoy seguro que este sea el mejor mecanismo para escoger el 
                    var prim = Listfiles.First();
                    FileInvGen = prim.ToString();
                    //tools.setVariable("FileInvGen", FileInvGen);
                    Properties.Settings.Default.last_file_missing = FileInvGen;
                    Properties.Settings.Default.Save();
                    return FileInvGen;
                }
                //FileInvGen = files.crearArchivoInventario(Properties.Settings.Default.path_genia, Properties.Settings.Default.file_type_general, idInventario, "");
                //tools.setVariable("FileInvGen", FileInvGen);
                //Properties.Settings.Default.last_file_gen = FileInvGen;
                //Properties.Settings.Default.Save();

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return FileInvGen;
        }

        private void tbFileName_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnStartStop_Click(object sender, RoutedEventArgs e)
        {
            if (stateStartButton)
            {
                startCopy.IsEnabled = true;
                endCopy.IsEnabled = true;
                stateStartButton = false;
                //sincronizar
                btnStartStop.Content = FindResource("Play");
                timer.Enabled = false;
                timer.Stop();
            }
            else
            {
                stateStartButton = true;
                string startValid = ""; //"" if valid
                btnStartStop.Content = FindResource("Stop");
                System.ComponentModel.BackgroundWorker bw = new System.ComponentModel.BackgroundWorker();
                parentWindow.ModalDialog.ShowHandlerDialog("Cargando Segmento...");
                copiesViewer.Children.Clear();
                detectedTags.Clear();
                barcodeStart = startCopy.Text.Trim();
                if (endCopy.Text.Trim() == "")
                    endCopy.Text = startCopy.Text;
                barcodeEnd = endCopy.Text.Trim();
                startCopy.IsEnabled = false;
                endCopy.IsEnabled = false;
                //bw.DoWork += new DoWorkEventHandler(Background_Method);
                bw.DoWork += delegate //(s, args) =>
                {
                    if (barcodeStart != "")
                    {
                        try
                        { 
                            string response = geniaWebService.getRangeInventory(barcodeStart, barcodeEnd, id_inventario);

                            if (!string.IsNullOrEmpty(response))
                            {

                                if (response != "<DocumentElement />")
                                {
                                    XmlDocument xm = new XmlDocument();
                                    xm.LoadXml(response);
                                    XmlNode root = xm.SelectNodes("DocumentElement").Item(0);

                                    XmlNodeList copies = root.SelectNodes("copies");
                                    this.Dispatcher.BeginInvoke(new Action(delegate {
                                        RotateTransform myRotateTransform = new RotateTransform();
                                        myRotateTransform.Angle = -90;
                                        TransformGroup myTransformGroup = new TransformGroup();
                                        myTransformGroup.Children.Add(myRotateTransform);

                                        int buttonWidth = 4;
                                        int buttonHeight = 4;
                                        int buttonFontSize = 22;
                                        int range = 65;
                                        int newNumber = copies.Count / range;
                                        switch (newNumber)
                                        {
                                            case 0:
                                                buttonWidth = 220;//
                                                buttonHeight = 40;
                                                buttonFontSize = 22;
                                                break;
                                            case 1:
                                                buttonWidth = 158;//
                                                buttonHeight = 29;
                                                buttonFontSize = 14;
                                                break;
                                            case 2:
                                                buttonWidth = 158;//
                                                buttonHeight = 20;
                                                buttonFontSize = 14;
                                                break;
                                            case 3:
                                                buttonWidth = 158;//
                                                buttonHeight = 14;
                                                buttonFontSize = 11;
                                                break;
                                            case 4:
                                                buttonWidth = 118;//
                                                buttonHeight = 16;
                                                buttonFontSize = 11;
                                                break;
                                            case 5:
                                                buttonWidth = 118;
                                                buttonHeight = 13;
                                                buttonFontSize = 9;
                                                break;
                                            case 6:
                                                buttonWidth = 118;
                                                buttonHeight = 11;
                                                buttonFontSize = 8;
                                                break;
                                            case 7:
                                                buttonWidth = 95;
                                                buttonHeight = 12;
                                                buttonFontSize = 8;
                                                break;
                                            case 8:
                                                buttonWidth = 95;
                                                buttonHeight = 11;
                                                buttonFontSize = 8;
                                                break;
                                            case 9:
                                                buttonWidth = 95;
                                                buttonHeight = 10;
                                                buttonFontSize = 8;
                                                break;
                                            case 10:
                                                buttonWidth = 95;
                                                buttonHeight = 9;
                                                buttonFontSize = 7;
                                                break;
                                            case 11:
                                                buttonWidth = 79;
                                                buttonHeight = 10;
                                                buttonFontSize = 7;
                                                break;
                                            case 12:
                                                buttonWidth = 68;
                                                buttonHeight = 10;
                                                buttonFontSize = 7;
                                                break;
                                            case 13:
                                                buttonWidth = 65;
                                                buttonHeight = 10;
                                                buttonFontSize = 7;
                                                break;
                                            case 14:
                                                buttonWidth = 58;
                                                buttonHeight = 10;
                                                buttonFontSize = 7;
                                                break;
                                            case 15:
                                                buttonWidth = 56;
                                                buttonHeight = 10;
                                                buttonFontSize = 7;
                                                break;
                                            case 16:
                                                buttonWidth = 52;
                                                buttonHeight = 10;
                                                buttonFontSize = 7;
                                                break;
                                            default:
                                                buttonFontSize = 0;
                                                if (copies.Count < 10000)
                                                {
                                                    //const float a1 = 220.23f;
                                                    //const float b1 = 0.0072f;
                                                    buttonWidth = 50 - ((copies.Count / 100));
                                                    if (buttonWidth < 5)
                                                        buttonWidth = 5;
                                                    //const float a2 = 40.038f;
                                                    //const float b2 = 0.0012f;
                                                    buttonHeight = 10 - ((copies.Count / 400));
                                                    if (buttonHeight < 4)
                                                        buttonHeight = 4;
                                                }
                                                break;
                                        }
                                        foreach (XmlNode copy in copies)
                                        {
                                            Button aux = new Button();
                                            aux.BorderBrush = Brushes.Transparent;
                                            aux.BorderThickness = new Thickness(0);

                                            string clasif = copy["clasificacion"].InnerText;
                                            if (buttonFontSize > 6)
                                            {
                                                aux.Content = string.IsNullOrEmpty(clasif) ? "Código: " + copy["acq"].InnerText : clasif;
                                                aux.FontSize = buttonFontSize;
                                            }
                                            string state = copy["estado"].InnerText;
                                            if (state == "1")
                                                detectedTags.Add(copy["id_tag"].InnerText);
                                            if (copy["id_tag"] == null)
                                            {
                                                aux.Name = "N_" + copy["acq"].InnerText;
                                                aux.Tag = copy["acq"].InnerText;
                                                aux.ToolTip = "No Asociado\n" + copy["clasificacion"].InnerText + "\n" + copy["substring"].InnerText + "\n" + copy["acq"].InnerText;
                                                aux.Background = nonProgramed;
                                                if (copiesViewer.FindName("N_" + copy["acq"].InnerText) != null)
                                                    UnregisterName("N_" + copy["acq"].InnerText);
                                                RegisterName("N_" + copy["acq"].InnerText, aux);
                                            }
                                            else
                                            {
                                                aux.Name = "N_" + copy["id_tag"].InnerText;
                                                aux.Tag = copy["acq"].InnerText;
                                                aux.Background = state == "1" ? inventoriedColor : nonInventroriedColor;
                                                aux.ToolTip = copy["clasificacion"].InnerText + "\n" + copy["substring"].InnerText + "\n" + copy["acq"].InnerText;
                                                if (copiesViewer.FindName("N_" + copy["id_tag"].InnerText) != null)
                                                    UnregisterName("N_" + copy["id_tag"].InnerText);
                                                RegisterName("N_" + copy["id_tag"].InnerText, aux);
                                            }
                                            aux.Width = buttonWidth;//220; //4;//;
                                            aux.Height = buttonHeight;//40;//4;//;                                            
                                            aux.HorizontalContentAlignment = HorizontalAlignment.Left;
                                            aux.LayoutTransform = myTransformGroup;
                                            
                                            copiesViewer.Children.Add(aux);
                                        }
                                        tblTotalTags.Text = copies.Count.ToString();
                                        parentWindow.ModalDialog.HideHandlerDialog();
                                    }));

                                }
                                else
                                    startValid = "El servicio no encontró resultados";
                            }
                            else
                                startValid = "No es posible conectar con el servidor";

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    else
                        startValid = "El campo \"Ejemplar Inicio\" no puede estar vacío";
                };
                bw.RunWorkerCompleted += (s, args) =>
                {
                    if (startValid == "")
                    {
                        timer.Enabled = true;
                        timer.Start();
                    }
                    else
                        MessageBox.Show(startValid);
                };
                int cnt = 0;
                for (cnt = 0; cnt < 5; cnt++)
                {
                    if (!bw.IsBusy) //Esto hace que no se choquen los hilos
                    {
                        bw.RunWorkerAsync();
                        break;
                    }
                }
            }


        }
    }
}
