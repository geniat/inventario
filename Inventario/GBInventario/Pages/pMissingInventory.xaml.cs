﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GBInventario.Controls;
using GBInventario.Models;
using System.Xml;

namespace GBInventario.Pages
{
    /// <summary>
    /// Lógica de interacción para pMissingInventory.xaml
    /// </summary>
    public partial class pMissingInventory : Page
    {
        #region variables inventario
        List<string> tagsFile = new List<string>();
        string FileName;
        #endregion

        #region variables timer
        //TimerHelper _timerHelper;
        System.Timers.Timer timer;
        #endregion

        #region clases internas
        cTools tools = new cTools();

        Controls.cFiles archivos = new cFiles();
        //Controls.cDatabaseGenia dbGenia = new Controls.cDatabaseGenia();
        GeniaWebServices.Service1 geniaWebService = new GeniaWebServices.Service1();
        Controls.cFiles files = new Controls.cFiles();
        MainWindow parentWindow;
        #endregion

        string invSel = "";
        public List<string> missingList;
        public pMissingInventory()
        {
            InitializeComponent();
            #region inicializa idioma
            var culture = System.Globalization.CultureInfo.CurrentCulture;//.GetCultureInfo("en-EU");//.CurrentCulture;
            Dispatcher.Thread.CurrentCulture = culture;
            Dispatcher.Thread.CurrentUICulture = culture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = culture;
            #endregion

            #region inicializa timer
            //_timerHelper = new TimerHelper();
            //_timerHelper.TimerEvent += (timer, state) => Timer_Elapsed();
            //StartTimer();
            timer = new System.Timers.Timer();
            timer.Interval = 1000;
            timer.Elapsed += timer_Elapsed;

            #endregion
        }

        void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {

            #region inicializa idioma
            var culture = System.Globalization.CultureInfo.CurrentCulture;//.GetCultureInfo("en-EU");//.CurrentCulture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = culture;
            #endregion
            if (uc_sync.isSyncActive)
                return;

            timer.Enabled = false;
            timer.Stop();
            if (!uc_sync.existOpenFile)
            {
                string message = string.Format(Properties.Resources.main_message_create_file, Properties.Resources.menu_files_sync);
                tbMessage.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Send, new UpdateTextHandler(update_message), message);
            }
            loadInfoRfid();
            timer.Enabled = true;
            timer.Start();
        }
        public void startTimer()
        {
            timer.Enabled = true;
            timer.Start();
        }
        public void stopTimer()
        {
            timer.Enabled = false;
            timer.Stop();
        }
        #region métodos timer
        //void StartTimer()
        //{

        //    _timerHelper.Start(TimeSpan.FromSeconds(2));
        //    //System.Threading.Thread.Sleep(TimeSpan.FromSeconds(12));

        //}
        //void StopTimer()
        //{
        //    _timerHelper.Stop(new TimeSpan(0, 0, 1));
        //}
        //void Timer_Elapsed()
        //{
        //    loadInfoRfid();

        //}
        #endregion

        #region métodos inventario
        private void loadInfoRfid()
        {
            try
            {
                if (FileName != Properties.Settings.Default.last_file_missing)
                {
                    FileName = Properties.Settings.Default.last_file_missing;
                    loadFileName();
                    tagsFile = new List<string>();
                    //setFileInvGen(out message, invSel);
                    loadView();
                }
                List<string> tags = userControlRfid.reader.tags;
                if (tags.Count > 0)
                {
                    userControlRfid.reader.tags = new List<string>();
                    List<string> tagsInserFile = new List<string>();
                    foreach (string tag in tags)
                    {
                        if (tagsFile.Contains(tag))
                            continue;
                        if (missingList.Contains(tag))
                        {
                            tagsFile.Add(tag);
                            tagsInserFile.Add(tag + ";" + tools.FormatoFileFecha(DateTime.Now) + ";");
                            missingList.Remove(tag);
                        }
                    }
                    archivos.EscribirLineaGeneral(FileName, tagsInserFile);

                    loadView();
                }
                //Define main message 
                uc_sync.tagsActualFile = tagsFile.Count;
                string DeviceState = userControlRfid.state;
                if (string.IsNullOrEmpty(DeviceState))
                {
                    tbMessage.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Send, new UpdateTextHandler(update_message), Properties.Resources.main_message_attemp_conn);
                    return;
                }
                if (uc_sync.existOpenFile)
                {
                    string messageMain = "";
                    switch (DeviceState.ToString())
                    {//0:emparejar;1:buscar;2:conectar;3:inventariar
                        case "0":
                            messageMain = string.Format(Properties.Resources.main_message_pair_dev, Properties.Resources.ucrfid_tooltip_view_devices);
                            break;
                        case "1":
                            messageMain = string.Format(Properties.Resources.main_message_view_dev, Properties.Resources.ucrfid_tooltip_view_devices);
                            break;
                        case "2":
                            messageMain = string.Format(Properties.Resources.main_message_start_dev, Properties.Resources.ucrfid_tooltip_connect);
                            break;
                        case "3":
                            messageMain = Properties.Resources.main_message_trigg_dev;
                            break;
                    }

                    if (uc_sync.countFiles > 1)
                        messageMain += "\n" + string.Format(Properties.Resources.main_message_sync_pend_files, uc_sync.countFiles.ToString(), Properties.Resources.menu_files_sync);
                    else if (uc_sync.tagsActualFile > 150)
                        messageMain += "\n" + string.Format(Properties.Resources.main_message_sync_reg, Properties.Resources.menu_files_sync);
                    tbMessage.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Send, new UpdateTextHandler(update_message), messageMain);
                }
            }
            catch { }
        }
        delegate void UpdateTextHandler(string updatedText);
        private void loadView()
        {
            tblTagsDet.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Send, new UpdateTextHandler(update_textBox), tagsFile.Count.ToString());
        }
        void update_message(string updatedText)
        {
            tbMessage.Text = updatedText;
        }
        void update_textBox(string updatedText)
        {
            tblTagsDet.Text = updatedText;
        }

        delegate void UpdateTextFileNameHandler(string update_textBoxFileName);
        private void loadFileName()
        {
            tbFileName.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Send, new UpdateTextHandler(update_textBoxFileName), FileName);
        }
        void update_textBoxFileName(string update_textBoxFileName)
        {
            System.IO.FileInfo fi = new System.IO.FileInfo(update_textBoxFileName);
            string namef = fi.Name.Split('_')[0];
            tbFileName.Content = namef;
        }
        #endregion

        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {
            unload();
        }
        public void unload()
        {
            userControlRfid.unload();
            //StopTimer();
            timer.Enabled = false;
            timer.Stop();
            timer.Dispose();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {

                parentWindow = Window.GetWindow(this) as MainWindow;
                //System.ComponentModel.BackgroundWorker bw = new System.ComponentModel.BackgroundWorker();
                //parentWindow.ModalDialog.ShowHandlerDialog(Properties.Resources.invgral_start);
                //DoEvents();
                string mensajeIdInv = "";
                //string mensajeFile = "";

                string message = "";
                //bw.DoWork += (s, args) =>
                //{
                //borra = "findInventoryBdd(out mensajeIdInv);";
                invSel = findInventoryBdd(out mensajeIdInv);
                //FileName = setFileInvGen(out mensajeFile, invSel);
                FileName = Properties.Settings.Default.last_file_missing;
                tagsFile = files.loadFromFile(FileName, invSel, out message);
                //};
                //bw.RunWorkerCompleted += (s, args) =>
                //{
                if (string.IsNullOrEmpty(invSel))
                    tbInventoryName.Content = mensajeIdInv;
                else
                    tbInventoryName.Content = invSel;

                string filename = Properties.Settings.Default.last_file_missing;
                bool createFile = false;
                if (string.IsNullOrEmpty(filename))
                    createFile = true;
                else if (!System.IO.File.Exists(filename))
                    createFile = true;
                if (createFile)
                {
                    uc_sync.NewFileMissing();
                    filename = Properties.Settings.Default.last_file_missing;
                }
                System.IO.FileInfo fi = new System.IO.FileInfo(filename);
                string namef = fi.Name.Split('_')[0];
                tbFileName.Content = namef;
                loadView();

                //parentWindow.ModalDialog.HideHandlerDialog();
                loadMissing();
                tblTotalMissing.Text = "Total Faltantes Cargados: " + missingList.Count.ToString("N0");
                timer.Enabled = true;
                timer.Start();
                //};
                //int cnt = 0;
                //for (cnt = 0; cnt < 5; cnt++)
                //{
                //    if (!bw.IsBusy) //Esto hace que no se choquen los hilos
                //    {
                //        bw.RunWorkerAsync();
                //        break;
                //    }
                //}
            }
            catch (Exception ex)
            {
                WpfMessageBox.Show(ex.Message);
            }
        }
        public static void DoEvents()
        {
            System.Windows.Threading.DispatcherFrame frame = new System.Windows.Threading.DispatcherFrame();
            System.Windows.Threading.Dispatcher.CurrentDispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new System.Windows.Threading.DispatcherOperationCallback(ExitFrame), frame);
            System.Windows.Threading.Dispatcher.PushFrame(frame);
        }
        public static object ExitFrame(object f)
        {
            ((System.Windows.Threading.DispatcherFrame)f).Continue = false;

            return null;
        }
        private string findInventoryBdd(out string message)
        {
            message = "";
            string response = "";
            XmlDocument xm = new XmlDocument();
            XmlNode root;
            try
            {
                invSel = Properties.Settings.Default.last_id_inventory;
                if (invSel == "No es posible recuperar información de inventarios")
                    invSel = "";
                if (string.IsNullOrEmpty(invSel))
                {
                    response = geniaWebService.getInventories();

                    if (string.IsNullOrEmpty(response))
                        return null;
                    if (response == "<DocumentElement />")
                        return null;

                    xm = new XmlDocument();
                    xm.LoadXml(response);
                    root = xm.SelectNodes("DocumentElement").Item(0);

                    XmlNodeList inventories = root.SelectNodes("inventories");

                    if (inventories.Count <= 0)
                    {
                        WpfMessageBox.Show(Properties.Resources.database_no_inventory);
                        return null;
                    }
                    invSel = inventories[0]["id"].InnerText + " - " + inventories[0]["nombre"].InnerText;
                    Properties.Settings.Default.last_id_inventory = invSel;
                    Properties.Settings.Default.Save();
                    tools.setVariable("invSel", invSel);
                }
            }
            catch(Exception ex)
            {
                message = ex.Message + "\n\nStackTrace: " + ex.StackTrace;
            }
            return invSel;
        }
        /// <summary>
        /// obsoleto JUCO
        /// </summary>
        /// <param name="message"></param>
        /// <param name="idInventario"></param>
        /// <returns></returns>
        private string setFileInvGen(out string message, string idInventario)
        {
            string FileInvGen = Properties.Settings.Default.last_file_missing;
            //string FileInvGenSettings = Properties.Settings.Default.last_file_gen;
            //bool FileSettingsExists = false;
            message = "";

            try
            {
                List<string> Listfiles = files.getFiles(Properties.Settings.Default.path_genia, Properties.Settings.Default.file_type_general);

                if (Listfiles.Count > 0)
                {
                    //no estoy seguro que este sea el mejor mecanismo para escoger el 
                    var prim = Listfiles.First();
                    FileInvGen = prim.ToString();
                    //tools.setVariable("FileInvGen", FileInvGen);
                    Properties.Settings.Default.last_file_missing = FileInvGen;
                    Properties.Settings.Default.Save();
                    return FileInvGen;
                }
                //FileInvGen = files.crearArchivoInventario(Properties.Settings.Default.path_genia, Properties.Settings.Default.file_type_general, idInventario, "");
                //tools.setVariable("FileInvGen", FileInvGen);
                //Properties.Settings.Default.last_file_gen = FileInvGen;
                //Properties.Settings.Default.Save();

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return FileInvGen;
        }

        private void tbFileName_Click(object sender, RoutedEventArgs e)
        {
            //timer.Enabled = false;
            //timer.Stop();
        }

        private void btnLoadMissing_Click(object sender, RoutedEventArgs e)
        {
            Button loadbtn = (Button)sender;
            switch(loadbtn.Name)
            {
                case "btnLoadMissing":
                    loadMissing();
                    break;
                default:
                    LoadMissingFromFile();
                    break;
            }
            tblTotalMissing.Text = "Total Faltantes Cargados: " + missingList.Count.ToString("N0");
        }
        private void loadMissing()
        {

            string message = "";
            XmlDocument xm = new XmlDocument();
            XmlNode root;
            invSel = findInventoryBdd(out message);
            //System.Data.DataTable reporte = dbGenia.getMissingReport(invSel.Split('-')[0].Trim());
            string response = geniaWebService.GetMissingReport(invSel.Split('-')[0].Trim(), null);
            if (string.IsNullOrEmpty(response))
            {
                MessageBox.Show("No hay conexión con el sistema Genia Bibliotecas, intente cargar el inventario de faltantes desde un archivo");
                return;
            }
            if (response == "<DocumentElement />")
            {
                MessageBox.Show("No se encontraron registros en el inventario " + invSel);
                return;
            }

            xm = new XmlDocument();
            xm.LoadXml(response);
            root = xm.SelectNodes("DocumentElement").Item(0);

            XmlNodeList copies = root.SelectNodes("copies");

            if (copies == null)
            {
                MessageBox.Show("Error de consulta de inventario " + invSel);
                return;
            }
            if (copies.Count <= 0)
            {
                MessageBox.Show("No se encontraron registros en el inventario " + invSel);
                return;
            }
            missingList = new List<string>();
            foreach (XmlNode row in copies)
                missingList.Add(row["epc"].InnerText);
             
            MessageBox.Show(missingList.Count.ToString() + " registros cargados.");
            foreach (string tag in tagsFile)
                missingList.Remove(tag);
        }
        private void LoadMissingFromFile()
        {
            Microsoft.Win32.OpenFileDialog ofd = new Microsoft.Win32.OpenFileDialog();
            ofd.DefaultExt = ".csv"; // Default file extension
                                     //"JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|GIF Files (*.gif)|*.gif";
            ofd.Filter = "CSV files (*.csv)|*.csv"; // Filter files by extension
            ofd.Multiselect = false;
            // Show save file dialog box
            Nullable<bool> result = ofd.ShowDialog();
            if (string.IsNullOrEmpty(ofd.FileName))
                return;
            using (System.IO.StreamReader stream = new System.IO.StreamReader(ofd.FileName))
            {
                string line = "";
                line = stream.ReadLine();
                missingList = new List<string>();
                while ((line = stream.ReadLine()) != null)
                {
                    string[] array = line.Split(';');
                    if (array.Length < 2)
                        continue;
                    if (array[1].Length != 24)
                        continue;
                    missingList.Add(array[1]);
                }
                stream.Close();
            }
        }
    }
}
