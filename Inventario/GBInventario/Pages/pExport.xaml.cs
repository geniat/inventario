﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GBInventario.Models;
using System.Data;
using System.Xml;
using System.IO;

namespace GBInventario.Pages
{
    /// <summary>
    /// Lógica de interacción para pExport.xaml
    /// </summary>
    public partial class pExport : Page
    {
        string invSel = "";
        MainWindow parentWindow;
        Controls.cTools tools = new Controls.cTools();
        Controls.cExport export = new Controls.cExport();
        GeniaWebServices.Service1 geniaWebService = new GeniaWebServices.Service1();
        DataTable descrip = new DataTable();
        DataTable fullInventoy = new DataTable();
        string filename = "";
        DataTable reporte;

        public pExport()
        {
            InitializeComponent();
        }
        private void loadInventory()
        {
            parentWindow.ModalDialog.ShowHandlerDialog(Properties.Resources.export_title);
            System.ComponentModel.BackgroundWorker bw = new System.ComponentModel.BackgroundWorker();
            bw.DoWork += (s, args) =>
            {
                
                string response = "";
                XmlDocument xm = new XmlDocument();
                XmlNode root;
                invSel = Properties.Settings.Default.last_id_inventory;
                if (string.IsNullOrEmpty(invSel))
                    invSel = tools.getVariable("invSel");
                if (invSel == "No es posible recuperar información de inventarios")
                    invSel = "";
                if (string.IsNullOrEmpty(invSel))
                {
                    response = geniaWebService.getInventories();

                    if (string.IsNullOrEmpty(response))
                        return;

                    if (response == "<DocumentElement />")
                        return;

                    xm = new XmlDocument();
                    xm.LoadXml(response);
                    root = xm.SelectNodes("DocumentElement").Item(0);

                    XmlNodeList inventories = root.SelectNodes("inventories");

                    if (inventories.Count <= 0)
                    {
                        WpfMessageBox.Show(Properties.Resources.database_no_inventory);
                        return;
                    }
                    invSel = inventories[0]["id"].InnerText + " - " + inventories[0]["nombre"].InnerText;
                    Properties.Settings.Default.last_id_inventory = invSel;
                    Properties.Settings.Default.Save();
                    tools.setVariable("invSel", invSel);
                }
                //descrip = export.getDescInventory(invSel.Split('-')[0].Trim());
                //fullInventoy = export.getInventory(invSel.Split('-')[0].Trim());

            };
            bw.RunWorkerCompleted += (s, args) =>
            {
                //ojo Idioma
                //Dictionary<string, string> summaryCategories = export.getCategories(invSel.Split('-')[0].Trim());
                //TextBlock tbl = new TextBlock();
                //tbl.Text = "Resumen por Categorías";
                //tbl.Height = 30;
                Dictionary<string, int> summaryCategoriesFin = new Dictionary<string, int>();

                //spSummary.Children.Add(tbl);
                //foreach (KeyValuePair<string, string> cat in summaryCategories)
                //{
                //    string[] arrayCant = cat.Value.Split(',');
                //    string tot = "0";
                //    string inv = "0";
                //    int invInt = 0;

                //    if (arrayCant.Length == 1)
                //        tot = arrayCant[0];
                //    else if (arrayCant.Length == 2)
                //    {
                //        tot = arrayCant[0];
                //        inv = arrayCant[1];
                //    }
                //    int.TryParse(inv, out invInt);
                //    summaryCategoriesFin.Add(cat.Key, invInt);
                //    //tbl = new TextBlock();
                //    //tbl.Text = cat.Key + "      Total:" + tot + "       Inventariados:" + inv;
                //    //spSummary.Children.Add(tbl);
                //}
                if (summaryCategoriesFin.Count > 0)
                {
                    //spSummary.Visibility = System.Windows.Visibility.Visible;
                    //pieChart.DataContext = summaryCategoriesFin;
                }

                //
                parentWindow.ModalDialog.HideHandlerDialog();
            };
            int cnt = 0;
            for (cnt = 0; cnt < 5; cnt++)
            {
                if (!bw.IsBusy) //Esto hace que no se choquen los hilos
                {
                    bw.RunWorkerAsync();
                    break;
                }
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Uri uri = new Uri("Pages/pDashboard.xaml", UriKind.Relative);
            this.NavigationService.Navigate(uri);
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            parentWindow = Window.GetWindow(this) as MainWindow;
            LoadCheckboxSelect();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Button btnSender = (Button)sender;
            string response = "";
            XmlDocument xm = new XmlDocument();
            XmlNode root;
            invSel = Properties.Settings.Default.last_id_inventory;
            if (invSel == "No es posible recuperar información de inventarios")
                invSel = "";
            if (string.IsNullOrEmpty(invSel))
            {
                response = geniaWebService.getInventories();

                if (string.IsNullOrEmpty(response))
                    return;

                if (response == "<DocumentElement />")
                    return;

                xm = new XmlDocument();
                xm.LoadXml(response);
                root = xm.SelectNodes("DocumentElement").Item(0);

                XmlNodeList inventories = root.SelectNodes("inventories");

                if (inventories.Count <= 0)
                {
                    WpfMessageBox.Show(Properties.Resources.database_no_inventory);
                    return;
                }
                invSel = inventories[0]["id"].InnerText + " - " + inventories[0]["nombre"].InnerText;
                Properties.Settings.Default.last_id_inventory = invSel;
                Properties.Settings.Default.Save();
                tools.setVariable("invSel", invSel);
            }
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            
            switch (btnSender.Name)
            {
                case "btnMissingExport":
                    dlg.FileName = "GeniaFaltantes_" + invSel.Split('-')[0].Trim()+ "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss"); // Default file name
                    break;
                case "btnTotalExport":
                    dlg.FileName = "GeniaTotal_" + invSel.Split('-')[0].Trim() + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss"); // Default file name
                    break;
            }
            dlg.DefaultExt = ".csv"; // Default file extension
            dlg.Filter = "CSV files (*.csv)|*.csv"; // Filter files by extension

            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result != true)
                return;
            else
                filename = dlg.FileName;
            response = "";
            StringReader sReader;
            DataSet ds = new DataSet();
            switch (btnSender.Name)
            {
                case "btnMissingExport":
                    List<string> categories = new List<string>();
                    for(int i = 0; i < chbMissingSelect.Children.Count; i++)
                    {
                        if (chbMissingSelect.Children[i].GetType() != typeof(CheckBox))
                            continue;
                        CheckBox chb = (CheckBox)chbMissingSelect.Children[i];
                        if (chb.IsChecked.Value)
                            categories.Add(chb.Content.ToString());
                    }
                    
                    string[] ubications = categories.Count == chbMissingSelect.Children.Count ? null : categories.ToArray(); 
                    response = geniaWebService.GetMissingReport(invSel.Split('-')[0].Trim(), ubications);
                    sReader = new StringReader(response);
                    ds.ReadXml(sReader);
                    reporte = ds.Tables[0];
                    //reporte = geniaDataBase.getMissingReport(invSel.Split('-')[0].Trim());
                    break;
                case "btnTotalExport":
                    response = geniaWebService.GetInventoryReport(invSel.Split('-')[0].Trim());
                    sReader = new StringReader(response);
                    ds.ReadXml(sReader);
                    reporte = ds.Tables[0];
                    //reporte = geniaDataBase.getInventoryReport(invSel.Split('-')[0].Trim());
                    break;
            }
            //validar cuando es null
            if (reporte.Rows.Count <= 0)
                WpfMessageBox.Show("");
            if (!export.ExportInventoryCsv(filename, reporte))
                filename = "";
            if (!string.IsNullOrEmpty(filename))
                System.Diagnostics.Process.Start(filename);
        }
        private void LoadCheckboxSelect()
        {
            string response= "";
            XmlDocument xm;
            XmlNode root;
            invSel = Properties.Settings.Default.last_id_inventory;
            if (invSel == "No es posible recuperar información de inventarios")
                invSel = "";
            if (string.IsNullOrEmpty(invSel))
            {
                response = geniaWebService.getInventories();

                if (string.IsNullOrEmpty(response))
                    return;

                if (response == "<DocumentElement />")
                    return;

                xm = new XmlDocument();
                xm.LoadXml(response);
                root = xm.SelectNodes("DocumentElement").Item(0);

                XmlNodeList inventories = root.SelectNodes("inventories");

                if (inventories.Count <= 0)
                {
                    WpfMessageBox.Show(Properties.Resources.database_no_inventory);
                    return;
                }
                invSel = inventories[0]["id"].InnerText + " - " + inventories[0]["nombre"].InnerText;
                Properties.Settings.Default.last_id_inventory = invSel;
                Properties.Settings.Default.Save();
                tools.setVariable("invSel", invSel);
            }
            response = geniaWebService.GetInventoryUbic(invSel.Split('-')[0].Trim());
            
            xm = new XmlDocument();
            xm.LoadXml(response);
            root = xm.SelectNodes("DocumentElement").Item(0);

            XmlNodeList ubications = root.SelectNodes("ubications");
            foreach(XmlNode node in ubications)
            {
                string content = "";
                if (node["coleccion"] == null)
                    content = " - ";
                else
                    content = node["coleccion"].InnerText;
                CheckBox chbAux = new CheckBox();
                chbAux.IsChecked = true;
                chbAux.Tag = content;
                chbAux.Content = content;
                chbMissingSelect.Children.Add(chbAux);
            }
        }

        private void chbAll_Click(object sender, RoutedEventArgs e)
        {
            CheckBox chbChangeSel = (CheckBox)sender;
            switch(chbChangeSel.Name)
            {
                case "chbAll":
                    foreach(Control control in chbMissingSelect.Children)
                    {
                        if (control.GetType() != typeof(CheckBox))
                            continue;
                        CheckBox chbAux = (CheckBox)control;
                        chbAux.IsChecked = true;
                    }
                    chbAll.IsChecked = true;
                    chbNone.IsChecked = false;
                    break;
                default:
                    foreach (Control control in chbMissingSelect.Children)
                    {
                        if (control.GetType() != typeof(CheckBox))
                            continue;
                        CheckBox chbAux = (CheckBox)control;
                        chbAux.IsChecked = false;
                    }
                    chbAll.IsChecked = false;
                    chbNone.IsChecked = true;
                    break;
            }
        }
    }
}
