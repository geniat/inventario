﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using GBInventario.Controls;
using GBInventario.Models;
using System.Data;
using System.Xml;

namespace GBInventario
{
    /// <summary>
    /// Lógica de interacción para pDashboard.xaml
    /// </summary>
    public partial class pDashboard : Page
    {
        cTools tools = new cTools();
        //cDatabaseGenia geniaDataBase;
        cCompactDatabase localDataBase = new cCompactDatabase();
        string invSel;
        GeniaWebServices.Service1 geniaWebService = new GeniaWebServices.Service1();
        public pDashboard()
        {
            InitializeComponent();
        }
        private void showColumnChart()
        {
            try
            {
                string response = "";
                XmlDocument xm = new XmlDocument();
                XmlNode root;
                invSel = Properties.Settings.Default.last_id_inventory;
                if (invSel == "No es posible recuperar información de inventarios")
                    invSel = "";
                if (string.IsNullOrEmpty(invSel))
                {
                    response = geniaWebService.getInventories();

                    if (string.IsNullOrEmpty(response))
                    {
                        showDefaultImage();
                        return;
                    }
                    if (response == "<DocumentElement />")
                    {
                        showDefaultImage();
                        return;
                    }

                    xm = new XmlDocument();
                    xm.LoadXml(response);
                    root = xm.SelectNodes("DocumentElement").Item(0);

                    XmlNodeList inventories = root.SelectNodes("inventories");

                    if (inventories.Count <= 0)
                    {
                        showDefaultImage();
                        WpfMessageBox.Show(Properties.Resources.database_no_inventory);
                        return;
                    }
                    invSel = inventories[0]["id"].InnerText + " - " + inventories[0]["nombre"].InnerText;
                    Properties.Settings.Default.last_id_inventory = invSel;
                    Properties.Settings.Default.Save();
                    tools.setVariable("invSel", invSel);
                }

                response = geniaWebService.InventorySummary(invSel.Split('-')[0].Trim());
                xm = new XmlDocument();
                xm.LoadXml(response);
                root = xm.SelectNodes("root").Item(0);

                string missing = root.SelectSingleNode("missing-0").InnerText;
                int int_missing = 0;
                int.TryParse(missing, out int_missing);

                string inventoried = root.SelectSingleNode("inventoried-1").InnerText;
                int int_inventoried = 0;
                int.TryParse(inventoried, out int_inventoried);

                string loaned = root.SelectSingleNode("loaned-2").InnerText;
                int int_loaned = 0;
                int.TryParse(loaned, out int_loaned);

                string noAsociated = root.SelectSingleNode("noAsociated-3").InnerText;
                int int_noAsociated = 0;
                int.TryParse(noAsociated, out int_noAsociated);

                Pie pieInv = new Pie() { state = Properties.Resources.dhbrd_inventoried, amount = int_inventoried };
                Pie pieNoInv = new Pie() { state = Properties.Resources.dhbrd_non_inventoried, amount = int_missing };
                Pie pieLoan = new Pie() { state = Properties.Resources.dhbrd_non_loaned, amount = int_loaned };
                Pie pieNoAssoc = new Pie() { state = Properties.Resources.dhbrd_non_associated, amount = int_noAsociated };

                List<Pie> valueList = new List<Pie>();

                valueList.Add(pieInv);
                valueList.Add(pieNoInv);
                valueList.Add(pieLoan);
                valueList.Add(pieNoAssoc);
                piePlotter.Opacity = 1;
                piePlotter.IsEnabled = true;
                this.DataContext = valueList;
            }
            catch(Exception ex)
            {
                showDefaultImage();
                MessageBox.Show(ex.Message + "\n\nStackeTrace: " + ex.StackTrace);
            }
        }
        private void showDefaultImage()
        {
            List<Pie> valueList = new List<Pie>();
            Pie pieAux = new Pie() { state = Properties.Resources.dhbrd_inventoried, amount = 14785 };
            valueList.Add(pieAux);
            pieAux = new Pie() { state = Properties.Resources.dhbrd_non_inventoried, amount = 247 };
            valueList.Add(pieAux);
            pieAux = new Pie() { state = Properties.Resources.dhbrd_non_loaned, amount = 124 };
            valueList.Add(pieAux);
            pieAux = new Pie() { state = Properties.Resources.dhbrd_non_associated, amount = 1100 };
            valueList.Add(pieAux);
            this.DataContext = valueList;
            piePlotter.IsEnabled = false;
            piePlotter.Opacity = 0.3;
            invSel = "No es posible recuperar información de inventarios";
        }
        private void showCopiesColumnChart()
        {
            try
            {
                string response = "";
                XmlDocument xm = new XmlDocument();
                XmlNode root;
                invSel = Properties.Settings.Default.last_id_inventory;
                if (invSel == "No es posible recuperar información de inventarios")
                    invSel = "";
                if (string.IsNullOrEmpty(invSel))
                {
                    response = geniaWebService.getInventories();

                    if (string.IsNullOrEmpty(response))
                    {
                        showDefaultImage();
                        return;
                    }
                    if (response == "<DocumentElement />")
                    {
                        showDefaultImage();
                        return;
                    }

                    xm = new XmlDocument();
                    xm.LoadXml(response);
                    root = xm.SelectNodes("DocumentElement").Item(0);

                    XmlNodeList inventories = root.SelectNodes("inventories");

                    if (inventories.Count <= 0)
                    {
                        showDefaultImage();
                        WpfMessageBox.Show(Properties.Resources.database_no_inventory);
                        return;
                    }
                    invSel = inventories[0]["id"].InnerText + " - " + inventories[0]["nombre"].InnerText;
                    Properties.Settings.Default.last_id_inventory = invSel;
                    Properties.Settings.Default.Save();
                    tools.setVariable("invSel", invSel);
                }
                //DataTable datosInv = geniaDataBase.copiesSummary(invSel.Split('-')[0].Trim());
                response = geniaWebService.CopiesSummary(invSel.Split('-')[0].Trim());
                xm = new XmlDocument();
                xm.LoadXml(response);
                root = xm.SelectNodes("DocumentElement").Item(0);

                XmlNodeList copies = root.SelectNodes("copies");

                List<Pie> valueList = new List<Pie>();

                foreach (XmlNode row in copies)
                {
                    Pie pieInv = new Pie();
                    string[] colection_data = row.SelectSingleNode("ubicacion").InnerText.Split('-');
                    if (colection_data.Length < 2)
                        continue;
                    pieInv = new Pie() { state = String.Join("-", colection_data, 1, colection_data.Length - 1), amount = double.Parse(row.SelectSingleNode("count").InnerText) };
                    valueList.Add(pieInv);
                }
                piePlotter.Opacity = 1;
                piePlotter.IsEnabled = true;
                this.DataContext = valueList;
            }
            catch (Exception ex)
            {
                showDefaultImage();
                MessageBox.Show(ex.Message + "\n\nStackeTrace: " + ex.StackTrace);
            }
        }

        private void showMissingCopiesColumnChart()
        {
            try
            {
                string response = "";
                XmlDocument xm = new XmlDocument();
                XmlNode root;
                invSel = Properties.Settings.Default.last_id_inventory;
                if (string.IsNullOrEmpty(invSel))
                {
                    response = geniaWebService.getInventories();

                    if (string.IsNullOrEmpty(response))
                    {
                        showDefaultImage();
                        return;
                    }
                    if (response == "<DocumentElement />")
                    {
                        showDefaultImage();
                        return;
                    }

                    xm = new XmlDocument();
                    xm.LoadXml(response);
                    root = xm.SelectNodes("DocumentElement").Item(0);

                    XmlNodeList inventories = root.SelectNodes("inventories");

                    if (inventories.Count <= 0)
                    {
                        showDefaultImage();
                        WpfMessageBox.Show(Properties.Resources.database_no_inventory);
                        return;
                    }
                    invSel = inventories[0]["id"].InnerText + " - " + inventories[0]["nombre"].InnerText;
                    Properties.Settings.Default.last_id_inventory = invSel;
                    Properties.Settings.Default.Save();
                    tools.setVariable("invSel", invSel);
                }
                //DataTable datosInv = geniaDataBase.copiesSummary(invSel.Split('-')[0].Trim());
                response = geniaWebService.MissingcopiesSummary(invSel.Split('-')[0].Trim());
                xm = new XmlDocument();
                xm.LoadXml(response);
                root = xm.SelectNodes("DocumentElement").Item(0);

                XmlNodeList copies = root.SelectNodes("copies");

                List<Pie> valueList = new List<Pie>();

                foreach (XmlNode row in copies)
                {
                    Pie pieInv = new Pie();
                    string[] colection_data = row.SelectSingleNode("ubicacion").InnerText.Split('-');
                    if (colection_data.Length < 2)
                        continue;
                    pieInv = new Pie() { state = String.Join("-", colection_data, 1, colection_data.Length - 1), amount = double.Parse(row.SelectSingleNode("count").InnerText) };
                    valueList.Add(pieInv);
                }
                piePlotter.Opacity = 1;
                piePlotter.IsEnabled = true;
                this.DataContext = valueList;
            }
            catch (Exception ex)
            {
                showDefaultImage();
                MessageBox.Show(ex.Message + "\n\nStackeTrace: " + ex.StackTrace);
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            System.Threading.Thread.Sleep(1000);
            //geniaDataBase = new cDatabaseGenia();
            //showColumnChart();
            string[] Pielist = Properties.Resources.dhbrd_pie_list.Split('|');
            ObservableCollection<ComboBoxItem> cbPieList = new ObservableCollection<ComboBoxItem>();
            foreach (string pie in Pielist)
                //cbPieList.Add(new ComboBoxItem { Content = pie });
            lstPieList.Items.Add(pie);// = cbPieList;
            lstPieList.SelectedIndex = 0;
            tblInventory.Text = Properties.Resources.invgral_inv_tit_name + " " + invSel;
        }

        private void lstPieList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch(lstPieList.SelectedIndex)
            {
                case 0:
                    showColumnChart();
                    break;
                case 1:
                    showCopiesColumnChart();
                    break;
                case 2:
                    showMissingCopiesColumnChart();
                    break;
            }
        }
    }
}
