﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GBInventario.Controls;
using System.Data;
using System.IO;
using System.Xml;

namespace GBInventario.Pages
{
    /// <summary>
    /// Lógica de interacción para pFileManager.xaml
    /// </summary>
    public partial class pFileManager : Page
    {
        cFiles files = new cFiles();
        //cDatabaseGenia geniaDataBase = new cDatabaseGenia();
        GeniaWebServices.Service1 geniaWebService = new GeniaWebServices.Service1();
        cTools tools = new cTools();
        string invSel;
        MainWindow parentWindow;
        Controls.cFiles archivos;

        public pFileManager()
        {
            InitializeComponent();
        }
        private void load_files()
        {
            try
            {
                invSel = Properties.Settings.Default.last_id_inventory;
                //DataTable filesDb = null;
                List<string> geniaFiles = files.getAllFiles(Properties.Settings.Default.path_genia);
                string message = "";
                string response = geniaWebService.getSyncFile(invSel.Split('-')[0].Trim(), out message);
                XmlDocument xm = new XmlDocument();
                XmlNode root;
                xm = new XmlDocument();
                xm.LoadXml(response);
                root = xm.SelectNodes("DocumentElement").Item(0);

                XmlNodeList filesdb = root.SelectNodes("files");

                List<GeniaFiles> fileList = new List<GeniaFiles>();
                int countReg = 0;
                int regSync = 0;
                string dateSync = "";

                foreach (XmlNode row in filesdb)
                {
                    countReg = 0;
                    regSync = 0;
                    int.TryParse(row["num_reg"].InnerText, out countReg);
                    int.TryParse(row["efectives"].InnerText, out regSync);
                    dateSync = row["to_char"].InnerText;//.Split('.')[0];
                    fileList.Add(new GeniaFiles() { fileName = row["name"].InnerText, state = Properties.Resources.file_state_sync, count = countReg, countSync = regSync.ToString(), timeStamp = dateSync, location = "Base de datos" });

                    if (geniaFiles.Contains(row["name"].InnerText))
                    {
                        geniaFiles.Remove(row["name"].InnerText);
                    }
                }
                foreach (string file in geniaFiles)
                {
                    message = "";
                    countReg = files.countRegFiles(file, out message);
                    string state = "";
                    if (file.EndsWith("igs") || file.EndsWith("ifs"))
                        state = Properties.Resources.file_state_sync;
                    else
                        state = Properties.Resources.file_state_open;
                    fileList.Add(new GeniaFiles() { fileName = file, state = state, count = countReg, countSync = "", timeStamp = "", location = "C:/genia" });
                }
                dgFiles.ItemsSource = fileList;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\nStackTrace: " + ex.StackTrace);
            }
        }
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            archivos = new Controls.cFiles();
            parentWindow = Window.GetWindow(this) as MainWindow;
            load_files();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem item = (MenuItem)sender;
            
            List<string> fileNames = new List<string>();
            string messageDatabase = "";
            switch (item.Name)
            {
                case "SYNCSEL":
                    messageDatabase = "No se puede sincronizar registros de la base de datos seleccione archivos no sincronizados.\nDesea continuar ?";
                    fileNames = getFilesSelected(dgFiles.SelectedItems, messageDatabase);
                    SyncFiles(fileNames);
                    break;
                case "DELSEL":
                    messageDatabase = "Para borrar los registros de base de datos elimine el inventario con la opción \"" 
                        + Properties.Resources.menu_database_del + "\".\nDesea continuar revisando otros ítems ?";
                    fileNames = getFilesSelected(dgFiles.SelectedItems, messageDatabase);
                    DeleteFiles(fileNames);
                    break;
                case "SYNCALL":
                    fileNames = getFilesSelected(dgFiles.Items, "");
                    SyncFiles(fileNames);
                    break;
                case "DELALL":
                    messageDatabase = "Para borrar los registros de base de datos elimine el inventario con la opción \""
                        + Properties.Resources.menu_database_del + "\".\nDesea continuar revisando otros ítems ?";
                    fileNames = getFilesSelected(dgFiles.Items, messageDatabase);
                    DeleteFiles(fileNames);
                    break;
            }
            
            load_files();
        }
        private List<string> getFilesSelected(System.Collections.IList items, string messageDatabase)
        {
            bool first = true;
            List<string> resp = new List<string>();
            foreach(GeniaFiles file in items)
            {

                if (file.location == "Base de datos" && first)
                {
                    first = false;
                    if (string.IsNullOrEmpty(messageDatabase))
                        continue;
                    switch (MessageBox.Show(messageDatabase, "", MessageBoxButton.YesNoCancel))
                    {
                        case MessageBoxResult.Yes:
                            continue;
                        case MessageBoxResult.Cancel:
                            return null;
                        case MessageBoxResult.No:
                            break;
                    }
                }
                else if (file.location != "Base de datos")
                {
                    resp.Add(file.fileName);
                }
            }
            return resp;
        }
        private List<string> getFilesItems(ItemCollection items, string messageDatabase)
        {
            bool first = true;
            List<string> resp = new List<string>();
            for (int i = 0; i < items.Count; i++)
            {
                object itemGet = items.GetItemAt(i);
                if (itemGet.GetType() != typeof(GeniaFiles))
                    continue;
                GeniaFiles file = (GeniaFiles)itemGet;
                if (file.location == "Base de datos" && first)
                {
                    first = false;
                    if (string.IsNullOrEmpty(messageDatabase))
                        continue;
                    switch (MessageBox.Show(messageDatabase, "", MessageBoxButton.YesNoCancel))
                    {
                        case MessageBoxResult.Yes:
                            continue;
                        case MessageBoxResult.Cancel:
                            return null;
                        case MessageBoxResult.No:
                            break;
                    }
                }
                else if (file.location != "Base de datos")
                {
                    resp.Add(file.fileName);
                }
            }
            return resp;
        }
        private void DeleteFiles(List<string> fileNames)
        {
            if (fileNames == null)
                return;
            if (fileNames.Count <= 0)
            {
                MessageBox.Show("No existen archivos para eliminar.");
                return;
            }
            else if (fileNames.Count == 1)
            {
                if (MessageBox.Show("Desea eliminar el archivo " + fileNames.First(), "", MessageBoxButton.YesNo) != MessageBoxResult.Yes)
                    return;
            }
            else if (MessageBox.Show("Desea eliminar los siguientes archivos:\r\n" + string.Join("\r\n", fileNames.ToArray()), "", MessageBoxButton.YesNo) != MessageBoxResult.Yes)
                return;

            foreach (string fileName in fileNames)
            {
                if (!File.Exists(fileName))
                {
                    if (MessageBox.Show("El archivo \"" + fileName + "\" no existe, Desea continuar ? ", "", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                        continue;
                    else
                        break;
                }
                try
                {
                    FileInfo fi = new FileInfo(fileName);
                    fi.Delete();
                }
                catch(Exception ex)
                {
                    if (MessageBox.Show("No fue posible borrar el archivo " + fileName + "\nMensaje error: " + ex.Message + "\n\nDesea continuar ?", "Error borrando archivo", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                        continue;
                    else
                        return;
                }
            }

        }
        private void SyncFiles(List<string> files)
        {
            if (files.Count <= 0)
            {
                MessageBox.Show("No existen archivos para sincronizar.");
                return;
            }
            if (string.IsNullOrEmpty(invSel))
                invSel = Properties.Settings.Default.last_id_inventory;

            int regSync = 0;
            parentWindow.ModalDialog.ShowHandlerDialog(Properties.Resources.file_sync);
            System.ComponentModel.BackgroundWorker bw = new System.ComponentModel.BackgroundWorker();
            bw.DoWork += (s, args) =>
            {
                
                string message = "";
                bool closeFile = false;


                foreach (string file in files)
                {
                    if (file.EndsWith("igs") || file.EndsWith("ifs"))
                    {
                        if (MessageBox.Show("El archivo \"" + file + "\" fue sincronizado previamente, no es posible sincronizar este archivo, desea continuar ?.", "", MessageBoxButton.YesNo) == MessageBoxResult.No)
                            break;
                        else
                            continue;

                    }
                    if (!archivos.syncFile(file, invSel, closeFile, out message))
                        if (MessageBox.Show(file + " No sincronizado,\nDesea continuar ?\nclick en NO para finalizar, si hace click en SI continuará procesando los demás archivos.", "", MessageBoxButton.YesNo) == MessageBoxResult.No)
                        {
                            //System.IO.File.Delete(file);
                            break;
                        }
                    int regsAux = 0;
                    if (int.TryParse(message, out regsAux))
                        regSync += regsAux;
                }                
            };
            bw.RunWorkerCompleted += (s, args) =>
            {
                load_files();
                parentWindow.ModalDialog.HideHandlerDialog();
                MessageBox.Show("Sincronización finalizada.\n\n" + regSync + " registros.");
            };
            int cnt = 0;
            for (cnt = 0; cnt < 5; cnt++)
            {
                if (!bw.IsBusy) //Esto hace que no se choquen los hilos
                {
                    bw.RunWorkerAsync();
                    break;
                }
            }
        }
    }
    public class GeniaFiles
    {
        public string fileName { get; set; }
        public string state { get; set; }
        public int count { get; set; }
        public string timeStamp { get; set; }
        public string countSync { get; set; }
        public string location { get; set; }
    }
}
