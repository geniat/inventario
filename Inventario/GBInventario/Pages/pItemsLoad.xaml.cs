﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.IO;
using System.Data;
using System.Data.OleDb;
using GBInventario.Controls;

namespace GBInventario.Pages
{
    /// <summary>
    /// Lógica de interacción para pItemsLoad.xaml
    /// </summary>
    public partial class pItemsLoad : Page
    {
        //cDatabaseGenia database = new cDatabaseGenia();
        cTools tools = new cTools();
        MainWindow parentWindow;
        Controls.cExport export = new Controls.cExport();
        GeniaWebServices.Service1 geniaWebService = new GeniaWebServices.Service1();
        string invSel = "";

        public pItemsLoad()
        {
            InitializeComponent();
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            System.ComponentModel.BackgroundWorker bw = new System.ComponentModel.BackgroundWorker();
            int? numExistencias = null;
            int? numInvCurso = null;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Excel Files (.xlsx)|*.xlsx|CSV files (*.csv)|*.csv";
            bool? resp = openFileDialog.ShowDialog();
            if (!resp.HasValue)
                return;
            if (!resp.Value)
                return;
            parentWindow.ModalDialog.ShowHandlerDialog(Properties.Resources.file_items_loading);
            string nombreInventario ="";
            string fecha = "";
            string idInventario = "";
            string message = "";
            string SheetFile = GetSheetFile(openFileDialog.FileName);

            bw.DoWork += (s, args) =>
            {


                DataTable items = ReadFile(openFileDialog.FileName, SheetFile);
                //buscar repetidos
                List<string> barcodeduplicates = items.AsEnumerable()
                    .Select(dr => dr.Field<string>(1))
                    .GroupBy(x => x)
                    .Where(g => g.Count() > 1)
                    .Select(g => g.Key)
                    .ToList();
                if(barcodeduplicates.Count>0)
                {
                    message = string.Format(Properties.Resources.file_duplicate_value, ", Barcode = " + barcodeduplicates[0]);
                    return;
                }
                List<string> idTagduplicates = items.AsEnumerable()
                    .Select(dr => dr.Field<string>(5))
                    .GroupBy(x => x)
                    .Where(g => g.Count() > 1)
                    .Select(g => g.Key)
                    .ToList();
                if (idTagduplicates.Count > 0 && !string.IsNullOrEmpty(idTagduplicates[0]))
                {
                    message = string.Format(Properties.Resources.file_duplicate_value, ", Tag = " + idTagduplicates[0]);
                    return;
                }
                //
                

                Dictionary<string, int> columns = GetColumns(items);
                //if (items.Rows.Count > 1)
                //    numExistencias = database.insertExistencias(items, columns, out fecha, out message);
                if (numExistencias.HasValue)
                {
                    nombreInventario = DateTime.Now.ToString("dd/MMM/yyyy_HHmmss");
                    idInventario = geniaWebService.CreateInventory(nombreInventario, "", 0, true, out message);
                    //idInventario = database.CreateInventory(nombreInventario,"",0, 2, out message);
                    int idInvInt = 0;
                    if (!int.TryParse(idInventario, out idInvInt))
                        return;
                    //if (idInvInt >= 0)
                    //    numInvCurso = database.InsertInventarioCurso_existencias(idInventario, out message);
                }

            };
            bw.RunWorkerCompleted += (s, args) =>
            {
                parentWindow.ModalDialog.HideHandlerDialog();
                if (!numExistencias.HasValue)
                {
                    WpfMessageBox.Show(Properties.Resources.file_items_error_loading + ". " + message);
                    return;
                }
                if (!numInvCurso.HasValue)
                {
                    WpfMessageBox.Show(Properties.Resources.inv_error_create + ". " + message);
                    return;
                }
                //if (numExistencias.Value == numInvCurso.Value)
                //{
                    Properties.Settings.Default.last_id_inventory = idInventario + " - " + nombreInventario;
                    tblInv.Text = Properties.Resources.invgral_inv_tit_name + " " + nombreInventario;
                    tblItems.Text = Properties.Resources.menu_files_items + ": " + fecha;
                    tools.setVariable("invSel", Properties.Settings.Default.last_id_inventory);
                    
                    Properties.Settings.Default.Save();
                    if (WpfMessageBox.Show("", string.Format(Properties.Resources.file_items_loaded + ", " + Properties.Resources.file_items_q_go_inv_gral, numExistencias.Value.ToString(), nombreInventario), MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                    {
                        Uri uri = new Uri("Pages/pGeneralInventory.xaml", UriKind.Relative);
                        this.NavigationService.Navigate(uri);
                    }
                //}
                //else
                //    WpfMessageBox.Show(Properties.Resources.inv_num_incomplete);
            };
            int cnt = 0;
            for (cnt = 0; cnt < 5; cnt++)
            {
                if (!bw.IsBusy) //Esto hace que no se choquen los hilos
                {
                    bw.RunWorkerAsync();
                    break;
                }
            }
            
        }
        private Dictionary<string, int> GetColumns(DataTable items)
        {
            Dictionary<string, int> resp = new Dictionary<string, int>();
            if (Properties.Settings.Default.db_colexs_id > 0)
                resp.Add("id", Properties.Settings.Default.db_colexs_id);
            if (Properties.Settings.Default.db_colexs_barcode > 0)
                resp.Add("barcode", Properties.Settings.Default.db_colexs_barcode);
            if (Properties.Settings.Default.db_colexs_epc > 0)
                resp.Add("epc", Properties.Settings.Default.db_colexs_epc);
            if (Properties.Settings.Default.db_colexs_tag_id > 0)
                resp.Add("tag_id", Properties.Settings.Default.db_colexs_tag_id);
            if (Properties.Settings.Default.db_colexs_nombre > 0)
                resp.Add("nombre", Properties.Settings.Default.db_colexs_nombre);
            if (Properties.Settings.Default.db_colexs_descripcion > 0)
                resp.Add("descripcion", Properties.Settings.Default.db_colexs_descripcion);
            if (Properties.Settings.Default.db_colexs_ubicacion > 0)
                resp.Add("ubicacion", Properties.Settings.Default.db_colexs_ubicacion);
            if (Properties.Settings.Default.db_colexs_usuario > 0)
                resp.Add("usuario", Properties.Settings.Default.db_colexs_usuario);
            if (Properties.Settings.Default.db_colexs_observacion > 0)
                resp.Add("observacion", Properties.Settings.Default.db_colexs_observacion);
            if (Properties.Settings.Default.db_colexs_fecha > 0)
                resp.Add("fecha", Properties.Settings.Default.db_colexs_fecha);
            if (Properties.Settings.Default.db_colexs_clasificacion > 0)
                resp.Add("clasificacion", Properties.Settings.Default.db_colexs_clasificacion);
            if (Properties.Settings.Default.db_colexs_categoria > 0)
                resp.Add("categoria", Properties.Settings.Default.db_colexs_categoria);
            if (Properties.Settings.Default.db_colexs_volumen > 0)
                resp.Add("volumen", Properties.Settings.Default.db_colexs_volumen);
            if (Properties.Settings.Default.db_colexs_ejemplar > 0)
                resp.Add("ejemplar", Properties.Settings.Default.db_colexs_ejemplar);
            if (Properties.Settings.Default.db_colexs_tomo > 0)
                resp.Add("tomo", Properties.Settings.Default.db_colexs_tomo);
            if (Properties.Settings.Default.db_colexs_prestado > 0)
                resp.Add("prestado", Properties.Settings.Default.db_colexs_prestado);


            return resp;
        }
        private bool isItemsValid(DataTable items, out string message)
        {
            bool resp = false;
            message = "";
            if (items.Columns.Count < 2)
                return resp;
            int colsOblig = 0;
            int colsOtro = 0;
            for (int i = 0; i < items.Columns.Count; i++)
            {
                switch (items.Rows[0][i].ToString())
                {
                    case "barcode":
                        colsOblig++;
                        break;
                    case "epc":
                        colsOblig++;
                        break;
                    case "tag_id":
                        colsOblig++;
                        break;
                    case "nombre":
                        colsOtro++;
                        break;
                    case "descripcion":
                        colsOtro++;
                        break;
                    case "ubicacion":
                        colsOtro++;
                        break;
                    case "usuario":
                        break;
                    case "observacion":
                        break;
                }
            }
            if (colsOblig > 0 && colsOtro > 0)
                resp = true;
            return resp;
        }
        private string GetSheetFile(string path)
        {
            string resp = null;

            if (!File.Exists(path))
                return null;

            string extension = System.IO.Path.GetExtension(path);

            switch (extension)
            {
                case ".xlsx":
                    resp = ReadSheetFile(path);
                    break;
                case ".csv":
                    break;
            }
            return resp;
            
        }
        private string ReadSheetFile(string path)
        {
            string firstSheetName = null;
            string connstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties='Excel 8.0;HDR=NO;IMEX=1';";   // Extra blank space cannot appear in Office 2007 and the last version. And we need to pay attention on semicolon.

            //string connstring = "Provider=Microsoft.JET.OLEDB.4.0;Data Source=" + path + ";Extended Properties='Excel 8.0;HDR=NO;IMEX=1';";  //This connection string is appropriate for Office 2007 and the older version. We can select the most suitable connection string according to Office version or our program.
            try
            {
                using (OleDbConnection conn = new OleDbConnection(connstring))
                {
                    conn.Open();
                    DataTable sheetsName = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "Table" });  //Get All Sheets Name
                    List<string> stringNames = new List<string>();
                    foreach (DataRow row in sheetsName.Rows)
                        stringNames.Add(row[2].ToString());
                    firstSheetName = WpfMessageBox.ShowListString(Properties.Resources.file_items_select_file_sheet, Properties.Resources.file_items_select_file_sheet_q, MessageBoxButton.YesNo, stringNames);
                }
            }
            catch {}
            return firstSheetName;
        }
        private DataTable ReadFile(string path, string SheetFile)
        {
            string message = "";
            DataTable tableDatos = new DataTable();

            if (!File.Exists(path))
                return tableDatos;
            
            string extension = System.IO.Path.GetExtension(path);
            switch(extension)
            {
                case ".xlsx":
                    tableDatos = ReadExcelToTable(path, out message, SheetFile);
                    break;
                case ".csv":
                    break;
            }
            return tableDatos;
            
        }
        ///<summary>
        ///Method to Read XLS/XLSX File
        ///</summary>
        ///<param name="path">Excel File Full Path</param>
        ///<returns></returns>
        private DataTable ReadExcelToTable(string path, out string message, string SheetName)
        {
            //string firstSheetName = WpfMessageBox.ShowListString(Properties.Resources.file_items_select_file_sheet, Properties.Resources.file_items_select_file_sheet_q, MessageBoxButton.YesNo, new List<string>(){"primero"});

            message = "";
            DataSet set = new DataSet();
            DataTable tableResp = new DataTable();
            //Connection String

            string connstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties='Excel 8.0;HDR=NO;IMEX=1';";   // Extra blank space cannot appear in Office 2007 and the last version. And we need to pay attention on semicolon.

            //string connstring = "Provider=Microsoft.JET.OLEDB.4.0;Data Source=" + path + ";Extended Properties='Excel 8.0;HDR=NO;IMEX=1';";  //This connection string is appropriate for Office 2007 and the older version. We can select the most suitable connection string according to Office version or our program.
            try
            {
                using (OleDbConnection conn = new OleDbConnection(connstring))
                {
                    conn.Open();
                    //DataTable sheetsName = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "Table" });  //Get All Sheets Name
                    ////string firstSheetName = sheetsName.Rows[2][2].ToString();   //Get the First Sheet Name
                    //List<string> stringNames = new List<string>();
                    //foreach(DataRow row in sheetsName.Rows)
                    //{
                    //    stringNames.Add(row[2].ToString());
                    //}
                    //string firstSheetName = WpfMessageBox.ShowListString(Properties.Resources.file_items_select_file_sheet, Properties.Resources.file_items_select_file_sheet_q, MessageBoxButton.YesNo, stringNames);
                    string sql = string.Format("SELECT * FROM [{0}]", SheetName);  //Query String
                    OleDbDataAdapter ada = new OleDbDataAdapter(sql, connstring);
                
                    ada.Fill(set);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            if (set.Tables.Count > 0)
                tableResp = set.Tables[0];
            return tableResp;
        }
        ///<summary>
        ///Method to Read CSV Format
        ///</summary>
        ///<param name="path">Read File Full Path</param>
        ///<returns></returns>
        private DataTable ReadExcelWithStream(string path)
        {
           DataTable dt = new DataTable();
           bool isDtHasColumn = false;   //Mark if DataTable Generates Column
           StreamReader reader = new StreamReader(path,System.Text.Encoding.Default);  //Data Stream
           while(!reader.EndOfStream)
            {
               string message = reader.ReadLine();
               string[] splitResult = message.Split(new char[]{','}, StringSplitOptions.None);  //Read One Row and Separate by Comma, Save to Array
               DataRow row = dt.NewRow();
               for(int i = 0;i<splitResult.Length;i++)
                  {
                      if(!isDtHasColumn) //If not Generate Column
                          {
                                dt.Columns.Add("column" + i,typeof(string));
                            }
                            row[i] = splitResult[i];
                     }
                    dt.Rows.Add(row);  //Add Row
                    isDtHasColumn = true;  //Mark the Existed Column after Read the First Row, Not Generate Column after Reading Later Rows
             }
            return dt;
        }
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            parentWindow = Window.GetWindow(this) as MainWindow;
            #region inicializa idioma
            var culture = System.Globalization.CultureInfo.CurrentCulture;//.GetCultureInfo("en-EU");//.CurrentCulture;
            Dispatcher.Thread.CurrentCulture = culture;
            Dispatcher.Thread.CurrentUICulture = culture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = culture;
            #endregion
        }

    }
}
