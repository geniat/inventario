﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Data;
using System.Windows.Shapes;
using GBInventario.Controls;
using System.Xml;

namespace GBInventario.Pages
{
    /// <summary>
    /// Lógica de interacción para pDeleteInventory.xaml
    /// </summary>
    public partial class pDeleteInventory : Page
    {
        //cDatabaseGenia geniaDataBase = new cDatabaseGenia();
        cTools tools = new cTools();
        Controls.cFiles archivos = new cFiles();
        GeniaWebServices.Service1 geniaWebService = new GeniaWebServices.Service1();

        public pDeleteInventory()
        {
            InitializeComponent();
        }
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            InventoriesLoad();
        }
        private void InventoriesLoad()
        {

            try
            {
                //DataTable inventories = geniaDataBase.getInventoryList();
                string response = geniaWebService.getInventories();
                if (string.IsNullOrEmpty(response))
                {
                    return;
                }
                if (response == "<DocumentElement />")
                {
                    return;
                }
                XmlDocument xm = new XmlDocument();
                xm.LoadXml(response);
                XmlNode root = xm.SelectNodes("DocumentElement").Item(0);
                XmlNodeList inventories = root.SelectNodes("inventories");
                spInventoryList.Children.Clear();
                foreach (XmlNode row in inventories)
                {
                    Button btnAux = new Button();
                    btnAux.Click += btnAux_Click;
                    btnAux.Tag = row["id"].InnerText + " - " + row["nombre"].InnerText;
                    btnAux.ToolTip = Properties.Resources.inv_select;
                    string state = "";
                    if (string.IsNullOrEmpty(row["tipo"].InnerText))
                        state = Properties.Resources.inv_state_open + ", " + row["estado"].InnerText;
                    else
                        state = Properties.Resources.inv_state_close;

                    btnAux.Content = row["nombre"].InnerText + " " + "(" + state + ")";
                    btnAux.Style = (Style)Application.Current.Resources["LinkButton"];
                    
                    spInventoryList.Children.Add(btnAux);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n\n" + ex.StackTrace);
            }
        }

        void btnAux_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btnAux = (Button)sender;
                string invSel = btnAux.Tag.ToString();
                string[] arrayIdInv = invSel.Split('-');
                MessageBoxResult buttonResp = WpfMessageBox.Show("", string.Format(Properties.Resources.inv_delete_message, invSel, Properties.Resources.menu_database_exp), MessageBoxButton.OKCancel);
                string message = "";

                
                switch (buttonResp)
                {
                    case MessageBoxResult.OK:
                        string response = geniaWebService.DeleteInventory(arrayIdInv[0], out message);
                        //if (geniaDataBase.DeleteInventory(arrayIdInv[0], out message))
                        if(response == "OK")
                            WpfMessageBox.Show("Eliminado correctamente.");
                        InventoriesLoad();
                        break;
                    case MessageBoxResult.Cancel:
                        
                        Properties.Settings.Default.last_id_inventory = invSel;
                        Properties.Settings.Default.Save();
                        tools.setVariable("invSel", invSel);
                        this.NavigationService.Navigate(new Uri("/Pages/pDashboard.xaml", UriKind.Relative));
                        break;
                    //default:
                        
                    //    break;
                }


                
            }
            catch(Exception ex)
            {
                WpfMessageBox.Show("Error: " + ex.Message);
            }
        }

        //private void btnDeleteFiles_Click(object sender, RoutedEventArgs e)
        //{
        //    if (MessageBox.Show("Se eliminarán todos los archivos físicos y los registros de los archivos usados en inventarios anteriores, antes de continuar asegurese de tener un respaldo, Desea Continuar ?", "", MessageBoxButton.YesNo) == MessageBoxResult.No)
        //        return;
        //    //eliminar registros
        //    string message = "";
        //    string response = geniaWebService.DeleteFileRegisters(out message);
        //    //if (!geniaDataBase.DeleteFileRegisters(out message))
        //    //{
        //    if(response != "OK")
        //    { 
        //        MessageBox.Show("Error eliminando archivos: " + message);
        //        return;
        //    }
        //    List<string> generalFiles = archivos.getFiles(Properties.Settings.Default.path_genia, Properties.Settings.Default.file_type_general);
        //    List<string> generalSyncFiles = archivos.getFiles(Properties.Settings.Default.path_genia, Properties.Settings.Default.file_type_general + "s");
        //    generalFiles.AddRange(generalSyncFiles);
        //    foreach (string file in generalFiles)
        //    {
        //        System.IO.File.Delete(file);
        //    }
        //    MessageBox.Show("Se borrarron correctamente los archivos de inventario.");
        //}
    }
}
