﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GBInventario.Controls;

namespace GBInventario.Pages
{
    /// <summary>
    /// Lógica de interacción para pInventoryManagement.xaml
    /// </summary>
    public partial class pInventoryManagement : Page
    {

        //cDatabaseGenia geniaDataBase = new cDatabaseGenia();
        GeniaWebServices.Service1 geniaWebService = new GeniaWebServices.Service1();
        cTools tools = new cTools();
        MainWindow parentWindow;

        public pInventoryManagement()
        {
            InitializeComponent();
        }

        private void btnQuickStart_Click(object sender, RoutedEventArgs e)
        {
            ModalDialog.ShowHandlerDialog("Creando archivo ...");
            System.ComponentModel.BackgroundWorker bw = new System.ComponentModel.BackgroundWorker();
            bool created = true;
            string message = "";
            string nameInventory = tbInventoryName.Text + "_" + DateTime.Now.ToString("dd-MM-yyyy_hhmm");
            string idInventario = "";
            bw.DoWork += (s, args) =>
            {
                idInventario = geniaWebService.CreateInventory(nameInventory, null, 0, true, out message);
                //idInventario = geniaDataBase.CreateInventory(nameInventory, null, 0, 0, out message);
                if (string.IsNullOrEmpty(idInventario))
                {
                    created = false;
                    MessageBox.Show(Properties.Resources.inv_error_create + ", " + message);
                    //parentWindow.TimedDialog.ShowHandlerDialog(Properties.Resources.inv_error_create + ", " + message,3000);
                    return;
                }
                //created = geniaDataBase.InsertInventarioCurso_existencias(idInventario, out message).HasValue;
            };
            bw.RunWorkerCompleted += (s, args) =>
            {
                ModalDialog.HideHandlerDialog();
                if (!created)
                {
                    MessageBox.Show(Properties.Resources.inv_error_create + ", Error: " + message);
                    //parentWindow.ModalDialog.ShowHandlerDialog(Properties.Resources.inv_error_create + ", Error: " + message);
                    return;
                }
                Properties.Settings.Default.last_id_inventory = idInventario + " - " + nameInventory;
                Properties.Settings.Default.Save();
                tools.setVariable("invSel", Properties.Settings.Default.last_id_inventory);

                this.NavigationService.Navigate(new Uri("/Pages/pDashboard.xaml", UriKind.Relative));
                WpfMessageBox.Show(Properties.Resources.inv_create);
                //parentWindow.ModalDialog.ShowHandlerDialog(Properties.Resources.inv_create);
                
            };
            int cnt = 0;
            for (cnt = 0; cnt < 5; cnt++)
            {
                if (!bw.IsBusy) //Esto hace que no se choquen los hilos
                {
                    bw.RunWorkerAsync();
                    break;
                }
                System.Threading.Thread.Sleep(5000);
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            parentWindow = Window.GetWindow(this) as MainWindow;
            ModalDialog.SetParent(parentWindow);
            try
            {
                version.Content = "GeniaInventario Version: " + System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
            }
            catch { }
        }
    }
}
