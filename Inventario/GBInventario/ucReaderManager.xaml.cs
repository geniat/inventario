﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using InTheHand.Net;
using InTheHand.Net.Bluetooth;
using InTheHand.Net.Bluetooth.Widcomm;
using InTheHand.Net.Sockets;
using System.Text.RegularExpressions;
using System.Management;
using GBInventario.Models;
using GBInventario.Controls;

namespace GBInventario
{
    /// <summary>
    /// Lógica de interacción para ReaderManager.xaml
    /// </summary>
    public partial class ucReaderManager : UserControl
    {
        public cReader reader = new cReader();
        MainWindow parentWindow;
        bool isReaderConnect = false;
        public string state;//0:emparejar;1:buscar;2:conectar;3:inventariar
        //System.Timers.Timer timer = new System.Timers.Timer();

        public ucReaderManager()
        {
            InitializeComponent();
            
            spMain.Background = Brushes.LightGray;
            if (string.IsNullOrEmpty(Properties.Settings.Default.last_device))
            {
                state = "1";
                btnState.IsEnabled = false;
                btnState.Opacity = 0.3;
                return;
            }
            if (string.IsNullOrEmpty(Properties.Settings.Default.last_com_device))
            {
                state = "0";
                return;
            }
            //timer.Interval = 10000;
            //timer.Elapsed += timer_Elapsed;
            //timer.Start();
        }

        //void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        //{
        //    timer.Stop();
        //    if (!reader.isConnect)
        //        _disconect();
        //    timer.Start();
        //}
        
        private void btnList_Click(object sender, RoutedEventArgs e)
        {
            List<Device> dispositivos = new List<Device>();
            System.ComponentModel.BackgroundWorker bw = new System.ComponentModel.BackgroundWorker();
            parentWindow.ModalDialog.ShowHandlerDialog(Properties.Resources.ucrfid_find_devices);
            string mensaje = "";
            bw.DoWork += (s, args) =>
            {
                dispositivos = GetDevices(out mensaje);
                if (dispositivos.Count > 0)
                {
                    //ojo solo desconectar si está conectado
                    if (isReaderConnect)
                        disconnect();
                }
            };
            bw.RunWorkerCompleted += (s, args) =>
            {
                if (dispositivos == null)
                    return;
                if(dispositivos.Count==0)
                {
                    parentWindow.ModalDialog.HideHandlerDialog();
                    parentWindow.TimedDialog.ShowHandlerDialog(Properties.Resources.ucrfid_no_devices, 2000);
                    return;
                }
                Device dispSel = WpfMessageBox.ShowDevices(Properties.Resources.ucrfid_sel_device_title, Properties.Resources.ucrfid_sel_device_text, MessageBoxButton.YesNo, dispositivos);
                if (string.IsNullOrEmpty(dispSel.name))
                {
                    parentWindow.ModalDialog.HideHandlerDialog();
                    return;
                }
                    
                if (string.IsNullOrEmpty(dispSel.serialPort) && dispSel.address != null)
                {
                    //emparejar
                    // replace DEVICE_PIN here, synchronous method, but fast
                    bool isPaired = BluetoothSecurity.PairRequest(new BluetoothAddress(dispSel.address), "0000");
                    if (isPaired)
                    {
                        // now it is paired
                        string serialPort = GetBluetoothPort(dispSel.address.ToString());
                        BluetoothSecurity.RemoveDevice(new BluetoothAddress(dispSel.address));
                    }
                    else
                    {
                        // pairing failed
                    }
                }
                if (!string.IsNullOrEmpty(dispSel.name))
                {
                    btnState.IsEnabled = true;
                    Properties.Settings.Default.last_device = dispSel.name;
                    Properties.Settings.Default.last_com_device = dispSel.serialPort;
                    Properties.Settings.Default.Save();
                    connect();
                }
                parentWindow.ModalDialog.HideHandlerDialog();
            };
            int cnt=0;
            for (cnt = 0; cnt < 5; cnt++)
            {
                if (!bw.IsBusy) //Esto hace que no se choquen los hilos
                {
                    bw.RunWorkerAsync();
                    break;
                }
            }
        }
        public List<Device> GetDevices(out string mensaje)
        {
            List<Device> resp = new List<Device>();
            mensaje = "";
            if (BluetoothRadio.IsSupported)
            {
                BluetoothClient client = new BluetoothClient();
                BluetoothDeviceInfo[] devices;
                devices = client.DiscoverDevicesInRange();
                foreach (BluetoothDeviceInfo d in devices)
                {
                    string port = GetBluetoothPort(d.DeviceAddress.ToString());
                    byte[] addressLong = d.DeviceAddress.ToByteArray();
                    Device disp = new Device(d.DeviceName, port, d.Connected, addressLong);
                    resp.Add(disp);
                }
            }
            else
            {
                
                mensaje = Properties.Resources.ucrfid_not_supported;
            }
            return resp;
        }
        private string GetBluetoothPort(string deviceAddress)
        {
            const string Win32_SerialPort = "Win32_SerialPort";
            SelectQuery q = new SelectQuery(Win32_SerialPort);
            ManagementObjectSearcher s = new ManagementObjectSearcher(q);
            foreach (object cur in s.Get())
            {
                ManagementObject mo = (ManagementObject)cur;
                string pnpId = mo.GetPropertyValue("PNPDeviceID").ToString();

                if (pnpId.Contains(deviceAddress))
                {
                    object captionObject = mo.GetPropertyValue("Caption");
                    string caption = captionObject.ToString();
                    int index = caption.LastIndexOf("(COM");
                    if (index > 0)
                    {
                        string portString = caption.Substring(index);
                        string comPort = portString.
                                      Replace("(", string.Empty).Replace(")", string.Empty);
                        return comPort;
                    }
                }
            }
            return null;
        }
        private void btnState_Click(object sender, RoutedEventArgs e)
        {

            if (isReaderConnect)
                disconnect();
            else

                connect();
        }
        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            unload();
        }
        public void unload()
        {
            reader.disconnect(Properties.Settings.Default.last_device);
        }
        private bool connect()
        {
            //bool resp = false;
            string message = "";
            if (string.IsNullOrEmpty(Properties.Settings.Default.last_com_device))
            {
                parentWindow.TimedDialog.ShowHandlerDialog(Properties.Resources.ucrfid_unpair, 2000);
                state = "0";
                return false;
            }
            parentWindow.ModalDialog.ShowHandlerDialog(Properties.Resources.ucrfid_connecting);
            System.ComponentModel.BackgroundWorker bw = new System.ComponentModel.BackgroundWorker();
            bw.DoWork += (s, args) =>
            {
                if (reader.connect(Properties.Settings.Default.last_device, Properties.Settings.Default.last_com_device, out message))
                    isReaderConnect = true;
                else
                    isReaderConnect = false;
            };
            bw.RunWorkerCompleted += (s, args) =>
            {
                if (isReaderConnect)
                {
                    spMain.Background = Brushes.LightGreen;
                    btnState.ToolTip = Properties.Resources.ucrfid_tooltip_disconnect;
                    imState.Source = new BitmapImage(new Uri("/Images/disconnect_bltt.ico", UriKind.Relative));
                    btnState.ToolTip = Properties.Resources.ucrfid_tooltip_disconnect;
                    btnState.Opacity = 1;
                    state = "3";
                }
                parentWindow.ModalDialog.HideHandlerDialog();
                if (!isReaderConnect)
                {
                    parentWindow.TimedDialog.ShowHandlerDialog(string.Format(Properties.Resources.ucrfid_device_no_find, Properties.Settings.Default.last_device), 2000);
                    tblState.ToolTip = string.Format(Properties.Resources.ucrfid_device_no_find, "");
                    state = "2";
                }
                else
                {
                    parentWindow.TimedDialog.ShowHandlerDialog(string.Format(Properties.Resources.ucrfid_device_done, Properties.Settings.Default.last_device), 2000);
                    tblState.ToolTip = reader.readerFirmware;//string.Format(Properties.Resources.ucrfid_device_done, "");
                }
            };
            int cnt = 0;
            for (cnt = 0; cnt < 5; cnt++)
            {
                if (!bw.IsBusy) //Esto hace que no se choquen los hilos
                {
                    bw.RunWorkerAsync();
                    break;
                }
            }
            
            return isReaderConnect;
        }
        private bool disconnect()
        {
            bool resp = false;
            if (WpfMessageBox.Show(Properties.Resources.ucrfid_q_disconnect_title, Properties.Resources.ucrfid_q_disconnect_message, MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                _disconect();
                state = "2";
                resp = true;
            }
            return resp;
        }
        private void _disconect()
        {
            spMain.Background = Brushes.LightGray;
            imState.Source = new BitmapImage(new Uri("/Images/conec_tbltt.ico", UriKind.Relative));
            btnState.IsEnabled = false;
            reader.disconnect(Properties.Settings.Default.last_device);
            isReaderConnect = false;
            btnState.ToolTip = Properties.Resources.ucrfid_tooltip_connect;
            //Properties.Settings.Default.last_device = "";
            //Properties.Settings.Default.last_com_device = "";
            tblState.ToolTip = string.Format(Properties.Resources.ucrfid_device_no_find, "");
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            parentWindow = Window.GetWindow(this) as MainWindow;
            if (isReaderConnect == false)
                connect();
        }
    }
}
