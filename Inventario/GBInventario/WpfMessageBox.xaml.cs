﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GBInventario.Models;
namespace GBInventario
{
    /// <summary>
    /// Lógica de interacción para WpfMessageBox.xaml
    /// </summary>
    public partial class WpfMessageBox : Window
    {
        public enum MessageBoxType
        {
            ConfirmationWithYesNo = 0,
            ConfirmationWithYesNoCancel,
            Information,
            Error,
            Warning
        }

        public enum MessageBoxImage
        {
            Warning = 0,
            Question,
            Information,
            Error,
            None
        }
        public WpfMessageBox()
        {
            InitializeComponent();
        }
        static WpfMessageBox _messageBox;
        static MessageBoxResult _result = MessageBoxResult.No;
        static string _stringResult;
        static Device _deviceResult = new Device();
        static string _ListStringResult;

        public static MessageBoxResult Show(string caption, string msg, MessageBoxType type)
        {
            switch (type)
            {
                case MessageBoxType.ConfirmationWithYesNo:
                    return Show(caption, msg, MessageBoxButton.YesNo,
                    MessageBoxImage.Question);
                case MessageBoxType.ConfirmationWithYesNoCancel:
                    return Show(caption, msg, MessageBoxButton.YesNoCancel,
                    MessageBoxImage.Question);
                case MessageBoxType.Information:
                    return Show(caption, msg, MessageBoxButton.OK,
                    MessageBoxImage.Information);
                case MessageBoxType.Error:
                    return Show(caption, msg, MessageBoxButton.OK,
                    MessageBoxImage.Error);
                case MessageBoxType.Warning:
                    return Show(caption, msg, MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                default:
                    return MessageBoxResult.No;
            }
        }
        public static MessageBoxResult Show(string msg, MessageBoxType type)
        {
            return Show(string.Empty, msg, type);
        }
        public static MessageBoxResult Show(string msg)
        {
            return Show(string.Empty, msg, MessageBoxButton.OK, MessageBoxImage.None);
        }
        public static MessageBoxResult Show(string caption, string text)
        {
            return Show(caption, text,
            MessageBoxButton.OK, MessageBoxImage.None);
        }
        public static string ShowItems(string caption, string text, MessageBoxButton button, List<string> items)
        {
            _messageBox = new WpfMessageBox { txtMsg = { Text = text }, MessageTitle = { Text = caption } };
            addList(items);
            SetVisibilityOfButtons(null);
            _messageBox.ShowDialog();
            return _stringResult;
        }
        public static Device ShowDevices(string caption, string text, MessageBoxButton button, List<Device> items)
        {
            _messageBox = new WpfMessageBox { txtMsg = { Text = text }, MessageTitle = { Text = caption } };
            addDevices(items);
            SetVisibilityOfButtons(null);
            _messageBox.ShowDialog();
            return _deviceResult;
        }
        public static string ShowListString(string caption, string text, MessageBoxButton button, List<string> items)
        {
            _messageBox = new WpfMessageBox { txtMsg = { Text = text }, MessageTitle = { Text = caption } };
            addTableItems(items);
            SetVisibilityOfButtons(null);
            _messageBox.ShowDialog();
            return _ListStringResult;
        }
        public static MessageBoxResult Show(string caption, string text, MessageBoxButton button)
        {
            return Show(caption, text, button,MessageBoxImage.None);
        }
        public static MessageBoxResult Show(string caption, string text, MessageBoxButton button, MessageBoxImage image)
        {
            _messageBox = new WpfMessageBox { txtMsg = { Text = text }, MessageTitle = { Text = caption } };
            
            SetVisibilityOfButtons(button);
            SetImageOfMessageBox(image);
            _messageBox.ShowDialog();
            return _result;
        }
        
        private static void SetVisibilityOfButtons(MessageBoxButton? button)
        {
            switch (button)
            {
                case MessageBoxButton.OK:
                    _messageBox.btnCancel.Visibility = Visibility.Collapsed;
                    _messageBox.btnNo.Visibility = Visibility.Collapsed;
                    _messageBox.btnYes.Visibility = Visibility.Collapsed;
                    _messageBox.btnOk.Focus();
                    break;
                case MessageBoxButton.OKCancel:
                    _messageBox.btnNo.Visibility = Visibility.Collapsed;
                    _messageBox.btnYes.Visibility = Visibility.Collapsed;
                    _messageBox.btnCancel.Focus();
                    break;
                case MessageBoxButton.YesNo:
                    _messageBox.btnOk.Visibility = Visibility.Collapsed;
                    _messageBox.btnCancel.Visibility = Visibility.Collapsed;
                    _messageBox.btnNo.Focus();
                    break;
                case MessageBoxButton.YesNoCancel:
                    _messageBox.btnOk.Visibility = Visibility.Collapsed;
                    _messageBox.btnCancel.Focus();
                    break;
                default:
                    _messageBox.btnNo.Visibility = Visibility.Collapsed;
                    _messageBox.btnYes.Visibility = Visibility.Collapsed;
                    _messageBox.btnOk.Visibility = Visibility.Collapsed;
                    _messageBox.btnCancel.Focus();
                    break;
            }
        }
        private static void SetImageOfMessageBox(MessageBoxImage image)
        {
            switch (image)
            {
                case MessageBoxImage.Warning:
                    _messageBox.SetImage("Warning.png");
                    break;
                case MessageBoxImage.Question:
                    _messageBox.SetImage("Question.png");
                    break;
                case MessageBoxImage.Information:
                    _messageBox.SetImage("Information.png");
                    break;
                case MessageBoxImage.Error:
                    _messageBox.SetImage("Error.png");
                    break;
                default:
                    _messageBox.img.Visibility = Visibility.Collapsed;
                    break;
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (sender == btnOk)
                _result = MessageBoxResult.OK;
            else if (sender == btnYes)
                _result = MessageBoxResult.Yes;
            else if (sender == btnNo)
                _result = MessageBoxResult.No;
            else if (sender == btnCancel)
                _result = MessageBoxResult.Cancel;
            else
                _result = MessageBoxResult.None;
            _messageBox.Close();
            _messageBox = null;
        }
        private void SetImage(string imageName)
        {
            string uri = string.Format("/Resources/images/{0}", imageName);
            var uriSource = new Uri(uri, UriKind.RelativeOrAbsolute);
            img.Source = new BitmapImage(uriSource);
        }
        private static void addList(List<string> items)
        {
            Button btn;
            Style style = Application.Current.FindResource("FileButton") as Style;

            foreach (string item in items)
            {
                btn = new Button();
                btn.Content = item;
                btn.Click += btn_Click;
                btn.Style = style;
                _messageBox.spListItems.Children.Add(btn);
            }
        }
        private static void addDevices(List<Device> items)
        {
            Button btn;
            Style styleUnpair = Application.Current.FindResource("FileButton") as Style;
            Style stylePair = Application.Current.FindResource("pairDeviceBtn") as Style;
            
            foreach (Device item in items)
            {
                btn = new Button();
                btn.Content = item.name;
                btn.Click += btnDevice_Click;
                btn.Tag = item;//.serialPort + "|" + item.connect.ToString();

                if (!string.IsNullOrEmpty(item.serialPort))
                {
                    btn.Style = stylePair;
                    btn.Background = Brushes.LightGreen;
                    btn.ToolTip = Properties.Resources.ucrfid_pair;
                }
                else
                {
                    btn.Style = styleUnpair;
                    btn.ToolTip = Properties.Resources.ucrfid_unpair;
                }
                _messageBox.spListItems.Children.Add(btn);
            }
        }
        private static void addTableItems(List<string> items)
        {
            Button btn;
            Style styleUnpair = Application.Current.FindResource("FileButton") as Style;
            Style stylePair = Application.Current.FindResource("pairDeviceBtn") as Style;

            foreach (string item in items)
            {
                btn = new Button();
                btn.Content = item;
                btn.Click += btnListString_Click;
                btn.Tag = item;//.serialPort + "|" + item.connect.ToString();

                //if (!string.IsNullOrEmpty(item.serialPort))
                //{
                btn.Style = stylePair;
                btn.Background = Brushes.LightGreen;
                //    btn.ToolTip = Properties.Resources.ucrfid_pair;
                //}
                //else
                //{
                //    btn.Style = styleUnpair;
                //    btn.ToolTip = Properties.Resources.ucrfid_unpair;
                //}
                _messageBox.spListItems.Children.Add(btn);
            }
        }
        static void btn_Click(object sender, RoutedEventArgs e)
        {
            Button btnAux = (Button)sender;
            _stringResult = btnAux.Content.ToString();
            _messageBox.Close();
            _messageBox = null;
        }
        static void btnDevice_Click(object sender, RoutedEventArgs e)
        {
            Button btnAux = (Button)sender;
            _deviceResult.name = btnAux.Content.ToString();
            //string tag = (string)btnAux.Tag;

            if (btnAux.Tag!=null)
            {
                _deviceResult = (Device)btnAux.Tag;
                //string[] arrTag = tag.Split('|');
                //string serialPort = arrTag[0];
                //bool connect = false;
                //bool.TryParse(arrTag[1], out connect);
                //_deviceResult.serialPort = serialPort;
                //_deviceResult.connect = connect;
                
            }
            _messageBox.Close();
            _messageBox = null;
        }
        static void btnListString_Click(object sender, RoutedEventArgs e)
        {
            Button btnAux = (Button)sender;
            _ListStringResult = btnAux.Content.ToString();
            _messageBox.Close();
            _messageBox = null;
        }
        
        private void Image_MouseUp(object sender, MouseButtonEventArgs e)
        {
            _result = MessageBoxResult.None;
            _messageBox.Close();
            _messageBox = null;
        }
    }
}
