﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Globalization;
using GBInventario.Models;
using System.Xml;

namespace GBInventario
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region variables menú
        int menu_max_width = Properties.Settings.Default.menu_max_width;
        int menu_min_width = Properties.Settings.Default.menu_min_width;
        bool isblockMax = false;
        #endregion
        #region variables lenguage
        int _languageChanges;
        #endregion
        #region clases internas
        Controls.cTools tools = new Controls.cTools();
        //Controls.cExport export = new Controls.cExport();
        Controls.cFiles archivos;
        //Controls.cDatabaseGenia geniaDataBase;
        mConfiguration conf;
        #endregion
        string invSel = "";
        GeniaWebServices.Service1 geniaWebService;

        public MainWindow()
        {
            #region inicializa idioma
            var culture = System.Globalization.CultureInfo.CurrentCulture;//.GetCultureInfo("en-EU");//.CurrentCulture;
            Dispatcher.Thread.CurrentCulture = culture;
            Dispatcher.Thread.CurrentUICulture = culture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = culture;
            #endregion
            InitializeComponent();
        }

        #region métodos menú
        private void miStart_Click(object sender, RoutedEventArgs e)
        {
            if (frmPages.CurrentSource.ToString() == "Pages/pDashboard.xaml")
                frmPages.Refresh();
            else
                frmPages.Navigate(new Uri("/Pages/pDashboard.xaml", UriKind.Relative));
        }
        /// <summary>
        /// Si el campo width del menú es máximo lo minimiza y viceversa.
        /// </summary>
        /// <returns>Se realizó el cambio correctamente o no.</returns>
        public bool change_menu_visibility()
        {
            bool resp = false;
            try
            {
                if (svMenu.Width > menu_min_width)
                    svMenu.Width = menu_min_width;
                else
                    svMenu.Width = menu_max_width;
                resp = true;
            }
            catch {}
            return resp;
        }
        /// <summary>
        /// Configura el campo width del menú en el máximo
        /// </summary>
        /// <returns>Se realizó el cambio correctamente o no.</returns>
        public bool set_max_width_menu()
        {
            bool resp = false;
            try
            {
                svMenu.Width = menu_max_width;
                resp = true;
            }
            catch { }
            return resp;
        }
        /// <summary>
        /// Configura el campo width del menú en el mínimo
        /// </summary>
        /// <returns>Se realizó el cambio correctamente o no.</returns>
        public bool set_min_width_menu()
        {
            bool resp = false;
            try
            {
                svMenu.Width = menu_min_width;
                resp = true;
            }
            catch { }
            return resp;
        }
        public void menuCollapsedAll()
        {
            System.Windows.Visibility vis = new System.Windows.Visibility();
            vis = System.Windows.Visibility.Collapsed;

            miDatabaseCreate.Visibility = vis;
            miDatabaseDel.Visibility = vis;
            miDatabaseExp.Visibility = vis;
            miDatabaseSel.Visibility = vis;

            miFilesAdmin.Visibility = vis;
            miFilesExpInventoryPlane.Visibility = vis;
            miFilesLoadMissing.Visibility = vis;
            miFilesSync.Visibility = vis;

            miInventoryGen.Visibility = vis;
            miInventoryMissing.Visibility = vis;
            miInventorySync.Visibility = vis;
            miFilesItems.Visibility = vis;
        }
        //categorías
        public void menuViewInventory()
        {
            System.Windows.Visibility vis = new System.Windows.Visibility();
            if (menuIsVisibleInventory())
                vis = System.Windows.Visibility.Collapsed;
            else
                vis = System.Windows.Visibility.Visible;

            miInventoryGen.Visibility = vis;
            miInventoryMissing.Visibility = vis;
            miInventorySync.Visibility = vis;
        }
        public void menuViewDatabase()
        {
            System.Windows.Visibility vis = new System.Windows.Visibility();
            if (menuIsVisibleDatabase())
                vis = System.Windows.Visibility.Collapsed;
            else
                vis = System.Windows.Visibility.Visible;

            miDatabaseCreate.Visibility = vis;
            miDatabaseDel.Visibility = vis;
            miDatabaseExp.Visibility = vis;
            miDatabaseSel.Visibility = vis;
            miFilesExpInventoryPlane.Visibility = vis;
        }
        public void menuViewFiles()
        {
            System.Windows.Visibility vis = new System.Windows.Visibility();
            if (menuIsVisibleFiles())
                vis = System.Windows.Visibility.Collapsed;
            else
                vis = System.Windows.Visibility.Visible;

            miFilesAdmin.Visibility = vis;
            
            miFilesLoadMissing.Visibility = vis;
            miFilesSync.Visibility = vis;
            miFilesItems.Visibility = vis;
        }

        public bool menuIsVisibleInventory()
        {
            bool resp = false;
            if (miInventoryGen.Visibility == System.Windows.Visibility.Visible)
                resp = true;
            return resp;
        }
        public bool menuIsVisibleFiles()
        {
            bool resp = false;
            if (miFilesAdmin.Visibility == System.Windows.Visibility.Visible)
                resp = true;
            return resp;
        }
        public bool menuIsVisibleDatabase()
        {
            bool resp = false;
            if (miDatabaseCreate.Visibility == System.Windows.Visibility.Visible)
                resp = true;
            return resp;
        }
        //mouse events
        private void svMenu_MouseEnter(object sender, MouseEventArgs e)
        {
            set_max_width_menu();
        }
        private void svMenu_MouseLeave(object sender, MouseEventArgs e)
        {
            if (!isblockMax)
                set_min_width_menu();
            menuCollapsedAll();
        }
        
        
        //System.ComponentModel.BackgroundWorker bw = new System.ComponentModel.BackgroundWorker();
        //bw.DoWork += (s, args) =>
        //{
        //};
        //bw.RunWorkerCompleted += (s, args) =>
        //{
        //    ModalDialog.HideHandlerDialog();
        //};
        //int cnt = 0;
        //for (cnt = 0; cnt < 5; cnt++)
        //{
        //    if (!bw.IsBusy) //Esto hace que no se choquen los hilos
        //    {
        //        bw.RunWorkerAsync();
        //        break;
        //    }
        //}
        public static void DoEvents()
        {
            System.Windows.Threading.DispatcherFrame frame = new System.Windows.Threading.DispatcherFrame();
            System.Windows.Threading.Dispatcher.CurrentDispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new System.Windows.Threading.DispatcherOperationCallback(ExitFrame), frame);
            System.Windows.Threading.Dispatcher.PushFrame(frame);
        }
        public static object ExitFrame(object f)
        {
            ((System.Windows.Threading.DispatcherFrame)f).Continue = false;

            return null;
        }
        //click menu items
        private void miInventory_Click(object sender, RoutedEventArgs e)
        {
            menuViewInventory();
            //menuCollapsedAll();
        }
        private void miInventoryGen_Click(object sender, RoutedEventArgs e)
        {
            frmPages.Navigate(new Uri("/Pages/pGeneralInventory.xaml", UriKind.Relative));
        }
        private void miInventoryMissing_Click(object sender, RoutedEventArgs e)
        {
            frmPages.Navigate(new Uri("/Pages/pMissingInventory.xaml", UriKind.Relative));
        }
        private void miInventorySync_Click(object sender, RoutedEventArgs e)
        {
            frmPages.Navigate(new Uri("/Pages/pSyncInventory.xaml", UriKind.Relative));
        }
        private void miDataBase_Click(object sender, RoutedEventArgs e)
        {
            //menuCollapsedAll();
            menuViewDatabase();
        }
        private void miDatabaseSel_Click(object sender, RoutedEventArgs e)
        {
            frmPages.Navigate(new Uri("/Pages/pInventoryList.xaml", UriKind.Relative));
        }
        private void miDatabaseCreate_Click(object sender, RoutedEventArgs e)
        {
            frmPages.Navigate(new Uri("/Pages/pInventoryManagement.xaml", UriKind.Relative));
        }
        private void miDatabaseDel_Click(object sender, RoutedEventArgs e)
        {
            frmPages.Navigate(new Uri("/Pages/pDeleteInventory.xaml", UriKind.Relative));
        }
        private void miDatabaseExp_Click(object sender, RoutedEventArgs e)
        {
            frmPages.Navigate(new Uri("/Pages/pExport.xaml", UriKind.Relative));
        }
        private void miFiles_Click(object sender, RoutedEventArgs e)
        {
            //menuCollapsedAll();
            menuViewFiles();
        }
        private void miFilesSync_Click(object sender, RoutedEventArgs e)
        {
            invSel = Properties.Settings.Default.last_id_inventory;
            if (string.IsNullOrEmpty(invSel))
                invSel = tools.getVariable("invSel");
            if (MessageBox.Show(invSel, "Inventario Seleccionado", MessageBoxButton.YesNo) == MessageBoxResult.No)
                return;
            int regSync = 0;
            ModalDialog.ShowHandlerDialog(Properties.Resources.file_sync);
            System.ComponentModel.BackgroundWorker bw = new System.ComponentModel.BackgroundWorker();
            bw.DoWork += (s, args) =>
            {
                List<string> generalFiles = archivos.getFiles(Properties.Settings.Default.path_genia, Properties.Settings.Default.file_type_general);
                List<string> missingFiles = archivos.getFiles(Properties.Settings.Default.path_genia, Properties.Settings.Default.file_type_missing);
                generalFiles.AddRange(missingFiles);
                string message = "";
                bool closeFile = false;
                

                foreach (string file in generalFiles)
                {
                    if (!archivos.syncFile(file, invSel, closeFile, out message))
                        if (MessageBox.Show(file + " No sincronizado,\nDesea continuar ?\nclick en NO para finalizar, si hace click en SI continuará procesando los demás archivos.", "", MessageBoxButton.YesNo) == MessageBoxResult.No)
                        {
                            //System.IO.File.Delete(file);
                            break;
                        }
                    int regsAux = 0;
                    if (int.TryParse(message, out regsAux))
                        regSync += regsAux;
                }
                generalFiles = archivos.getFiles(Properties.Settings.Default.path_genia, Properties.Settings.Default.file_type_general);
            };
            bw.RunWorkerCompleted += (s, args) =>
            {
                ModalDialog.HideHandlerDialog();
                MessageBox.Show("Sincronización finalizada.\n\n" + regSync +" registros.");
                if (frmPages.CurrentSource.ToString() == "Pages/pFileManager.xaml" || frmPages.CurrentSource.ToString() == "Pages/pDashboard.xaml")
                    frmPages.Refresh();
            };
            int cnt = 0;
            for (cnt = 0; cnt < 5; cnt++)
            {
                if (!bw.IsBusy) //Esto hace que no se choquen los hilos
                {
                    bw.RunWorkerAsync();
                    break;
                }
            }
        }
        private void miFilesAdmin_Click(object sender, RoutedEventArgs e)
        {
            if (frmPages.CurrentSource.ToString() == "Pages/pFileManager.xaml")
                frmPages.Refresh();
            else
                frmPages.Navigate(new Uri("/Pages/pFileManager.xaml", UriKind.Relative));
        }
        private void miFilesLoadMissing_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog ofd = new Microsoft.Win32.OpenFileDialog();
            ofd.DefaultExt = ".ig"; // Default file extension
            //"JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|GIF Files (*.gif)|*.gif";
            ofd.Filter = "Inventory Genia files (*.ig)|*.ig"; // Filter files by extension
            ofd.Multiselect = true;
            // Show save file dialog box
            Nullable<bool> result = ofd.ShowDialog();
            int countFileCopy = 0;
            for (int i =0;i< ofd.SafeFileNames.Length;i++)
            {
                string pathEnd = System.IO.Path.Combine(Properties.Settings.Default.path_genia, ofd.SafeFileNames[i]);
                while (System.IO.File.Exists(pathEnd))
                {
                    pathEnd = pathEnd.Substring(0, pathEnd.Length - 3) + "(" + countFileCopy + ")." + ofd.DefaultExt;
                    countFileCopy++;
                }
                System.IO.File.Copy(ofd.FileNames[i], pathEnd);
                countFileCopy++;
            }
            MessageBox.Show(string.Format(Properties.Resources.file_copied_files, countFileCopy.ToString()));
            if (frmPages.CurrentSource.ToString() == "Pages/pFileManager.xaml")
                frmPages.Refresh();
        }
        private void miFilesExpInventoryPlane_Click(object sender, RoutedEventArgs e)
        {
            //invSel = Properties.Settings.Default.last_id_inventory;
            if (string.IsNullOrEmpty(invSel))
                invSel = tools.getVariable("invSel");
               
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "InvFile_"+ invSel.Split('-')[0] + DateTime.Now.ToString("ddMMyy_hhmmss"); // Default file name
            dlg.DefaultExt = ".txt"; // Default file extension
            dlg.Filter = "Text documents (.txt)|*.txt"; // Filter files by extension

            string filename = "";
            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();
            // Process save file dialog box results
            if (result != true)
                return;
            else
                filename = dlg.FileName;


            //StringBuilder reporte = geniaDataBase.getInventoryStringBuilder(invSel.Split('-')[0].Trim());
            string reporte = geniaWebService.GetInventoryBarcodes(invSel.Split('-')[0].Trim());
            //validar cuando es null
            if (string.IsNullOrEmpty(reporte))
            {
                WpfMessageBox.Show("Error");
                return;
            }
            Controls.cExport export = new Controls.cExport();
            if (!export.ExportInventoryTxt(filename, reporte))
            {
                WpfMessageBox.Show("Error");
                filename = "";
            }
            if (!string.IsNullOrEmpty(filename))
                System.Diagnostics.Process.Start(filename);
        }
        private void miFilesItems_Click(object sender, RoutedEventArgs e)
        {
            frmPages.Navigate(new Uri("/Pages/pItemsLoad.xaml", UriKind.Relative));
        }
        #endregion

        #region métodos idiomas
        private object Multilingual_Callback(CultureInfo culture, CultureInfo uiCulture, object parameter)
        {
            // The "culture" and "uiCulture" parameters must be always respected if:
            // - The application calls the "LocalizationManager.UpdateValues()" method from threads
            //   other than the UI thread. In this case this callback will be executed in the context
            //   of the thread calling the "UpdateValues()" method.
            // - The application uses multiple languages at the same time and this callback is used to 
            //   localize a control that uses a language different than the default language of the
            //   application

            return string.Format(
                culture,
                Properties.Resources.ResourceManager.GetString("Text_CurrentCulture", uiCulture),
                uiCulture.Name
                );
        }
        private object Callback(CultureInfo culture, CultureInfo uiCulture, object parameter)
        {
            // The "culture" and "uiCulture" parameters must be always respected if:
            // - The application calls the "LocalizationManager.UpdateValues()" method from threads
            //   other than the UI thread. In this case this callback will be executed in the context
            //   of the thread calling the "UpdateValues()" method.
            // - The application uses multiple languages at the same time and this callback is used to 
            //   localize a control that uses a language different than the default language of the
            //   application

            return string.Format(
                culture,
                Properties.Resources.ResourceManager.GetString("Text_LanguageChanges", uiCulture),
                ++_languageChanges
                );
        }
        #endregion 

        private void frmPages_MouseDown(object sender, MouseButtonEventArgs e)
        {
            set_min_width_menu();
        }

        private void miBlock_Click(object sender, RoutedEventArgs e)
        {
            isblockMax = !isblockMax;
            if (!isblockMax)
            {
                miBlock.Icon = new Image
                {
                    Source = new BitmapImage(new Uri("/Images/pin_blue.ico", UriKind.Relative))//"pack://application:,,,/your_assembly;component/yourpath/Image.png"))
                };
                set_min_width_menu();
                frmPages.Margin = new Thickness(43, 113, 12, 12);
            }
            else
            {
                miBlock.Icon = new Image
                {
                    Source = new BitmapImage(new Uri("/Images/pin_red.ico", UriKind.Relative))//"pack://application:,,,/your_assembly;component/yourpath/Image.png"))
                };
                set_max_width_menu();
                frmPages.Margin = new Thickness(246, 113, 12, 12);
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (this.frmPages.Content is GBInventario.Pages.pGeneralInventory)
            {
                ((GBInventario.Pages.pGeneralInventory)this.frmPages.Content).unload();
            }
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            
            string message = "";
            conf = new mConfiguration(out message);
            geniaWebService = new GeniaWebServices.Service1();

            archivos = new Controls.cFiles();
            invSel = Properties.Settings.Default.last_id_inventory;
            if (string.IsNullOrEmpty(invSel))
            {
                string response = geniaWebService.getInventories();

                if (string.IsNullOrEmpty(response))
                {
                    return;
                }
                if (response == "<DocumentElement />")
                {
                    return;
                }
                XmlDocument xm = new XmlDocument();
                XmlNode root;
                xm = new XmlDocument();
                xm.LoadXml(response);
                root = xm.SelectNodes("DocumentElement").Item(0);

                XmlNodeList inventories = root.SelectNodes("inventories");

                if (inventories.Count <= 0)
                {
                    WpfMessageBox.Show(Properties.Resources.database_no_inventory);
                    return;
                }
                invSel = inventories[0]["id"].InnerText + " - " + inventories[0]["nombre"].InnerText;
                Properties.Settings.Default.last_id_inventory = invSel;
                Properties.Settings.Default.Save();
                tools.setVariable("invSel", invSel);
            }
            imLogoGenia.Tag = Properties.Settings.Default.inventory_type;
            #region inicializa menú
            set_min_width_menu();
            menuCollapsedAll();
            #endregion
            frmPages.Navigate(new Uri("/Pages/pDashboard.xaml", UriKind.Relative));
            ModalDialog.SetParent(this);
            TimedDialog.SetParent(this);
            try
            {
                miBlock.ToolTip = "GeniaInventario Version: " + System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
            }
            catch { }
        }

        
    }
}
