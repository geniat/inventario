﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;

namespace GBInventario
{
    /// <summary>
    /// Lógica de interacción para ucTimedMessage.xaml
    /// </summary>
    public partial class ucTimedMessage : UserControl
    {
        public ucTimedMessage()
        {
            InitializeComponent();
            Visibility = Visibility.Hidden;
            //timer.Interval = 500;
            //timer.Elapsed += timer_Elapsed;
        }
        void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            // HACK: Simulate "DoEvents"
            this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background, new ThreadStart(delegate { }));
        }
        private bool _hideRequest = false;
        private bool _result = false;
        private UIElement _parent;

        public void SetParent(UIElement parent)
        {
            _parent = parent;
        }

        #region Message

        public string TimedMessage
        {
            get { return (string)GetValue(MessageProperty); }
            set { SetValue(MessageProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Message.
        // This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MessageProperty = DependencyProperty.Register("TimedMessage", typeof(string), typeof(ucTimedMessage), new UIPropertyMetadata(string.Empty));

        #endregion
        /// <summary>
        /// Muestra un mensaje bloqueando la pantalla durante un lapso de tiempo
        /// </summary>
        /// <param name="message">Mensaje que se muestra</param>
        /// <param name="interval">Lapso de tiempo que dura el mensaje en pantalla</param>
        /// <returns></returns>
        public bool ShowHandlerDialog(string message, int interval)
        {
            TimedMessage = message;
            MessageTextBlock.Text = TimedMessage;
            Visibility = Visibility.Visible;

            //_parent.IsEnabled = false;

            _hideRequest = false;
            DateTime ini = DateTime.Now;
            while (!_hideRequest)
            {
                TimeSpan timeDur = DateTime.Now-ini;
                if (timeDur.TotalMilliseconds > interval)
                    break;
                // HACK: Stop the thread if the application is about to close
                if (this.Dispatcher.HasShutdownStarted || this.Dispatcher.HasShutdownFinished)
                {
                    break;
                }
                
                // HACK: Simulate "DoEvents"
                //this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background, new ThreadStart(delegate { }));
                Thread.Sleep(20);
            }
            //timer.Enabled = true;
            //timer.Start();
            //this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background, new ThreadStart(delegate { }));
            //Thread.Sleep(interval);
            HideHandlerDialog();
            return _result;
        }

        public void HideHandlerDialog()
        {
            //timer.Enabled = false;
            //timer.Stop();
            _hideRequest = true;
            Visibility = Visibility.Hidden;
            _parent.IsEnabled = true;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            //timer.Enabled = false;
            //timer.Stop();
        }
    }
}
