﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using GBInventario.Controls;

namespace GBInventario
{
    /// <summary>
    /// Lógica de interacción para ucSync.xaml
    /// </summary>
    public partial class ucSync : UserControl
    {
        public int sumFilesClosed = 0;
        public int tagsActualFile = 0;
        public bool isSyncActive = false;
        public bool existOpenFile = false;
        string actualFileSystem;

        Controls.cFiles archivos = new Controls.cFiles();
        cTools tools = new cTools();

        System.Timers.Timer timer = new System.Timers.Timer();
        MainWindow parentWindow;
        public int countFiles;
        int? countSynchronized;
        string InvType;
        public static readonly DependencyProperty InventoryTypeProperty = DependencyProperty.Register("InventoryType", typeof(string), typeof(ucSync));
        public string InventoryType
        {
            get { return (string)GetValue(InventoryTypeProperty); }
            set { SetValue(InventoryTypeProperty, value); }
        }

        public ucSync()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            InvType = InventoryType;
            switch (InvType)
            {
                case "ig":
                    actualFileSystem = Properties.Settings.Default.last_file_gen;
                    break;
                case "if":
                    actualFileSystem = Properties.Settings.Default.last_file_missing;
                    break;
            }
            
            parentWindow = Window.GetWindow(this) as MainWindow;
            timer.Elapsed += timer_Elapsed;
            timer.Interval = 1500;
            timer.Enabled = true;
            timer.Start();
        }
        private void FileReview()
        {
            try
            {
                //este dato no se está actualizando
                if (string.IsNullOrEmpty(actualFileSystem))
                {
                    switch (InvType)
                    {
                        case "ig":
                            actualFileSystem = Properties.Settings.Default.last_file_gen;
                            break;
                        case "if":
                            actualFileSystem = Properties.Settings.Default.last_file_missing;
                            break;
                    }
                }
                string invSel = Properties.Settings.Default.last_id_inventory;
                countSynchronized = archivos.getSynchronized(invSel);
                string infoFileName = "";
                bool createFile = false;
                List<string> generalFiles = archivos.getFiles(Properties.Settings.Default.path_genia, InvType);
                countFiles = generalFiles.Count;
                if (generalFiles.Count == 0)
                    createFile = true;
                if (!createFile)
                {
                    foreach (string file in generalFiles)
                    {

                        if (actualFileSystem == file)
                        {
                            existOpenFile = true;
                            continue;
                        }
                    }
                    if (!existOpenFile)
                        createFile = true;
                }
                if(createFile)
                {
                    if (wNewFile.Show("", "nombre", System.Windows.MessageBoxButton.OKCancel) == System.Windows.MessageBoxResult.OK)
                    {
                        string result = wNewFile._texboxResult;
                        if (string.IsNullOrEmpty(result))
                            infoFileName = "No_Info";
                        else
                            infoFileName = result;
                    }
                    actualFileSystem = archivos.crearArchivoInventario(Properties.Settings.Default.path_genia, InvType, invSel, "", infoFileName);
                    if (string.IsNullOrEmpty(actualFileSystem))
                    {
                        switch (InvType)
                        {
                            case "ig":
                                Properties.Settings.Default.last_file_gen = actualFileSystem;
                                break;
                            case "if":
                                Properties.Settings.Default.last_file_missing = actualFileSystem;
                                break;
                        }
                    }
                    
                }
            }
            catch
            {
            }
        }
        private void loadUserControl()
        {
            //fileType + ";;" + inventarioSel + ";" + idCliente + ";" + usuario
            string line = "";
            int numReg = 0;
            //string actualFile = "";
            //icFiles.Items.Clear();
            string actualFileSystem = Properties.Settings.Default.last_file_gen;
            string invSel = Properties.Settings.Default.last_id_inventory;
            List<string> items = new List<string>();
            string message = "";
            string infoFileName = "";
            try
            {
                
                countSynchronized = archivos.getSynchronized(invSel);
                List<string> generalFiles = archivos.getFiles(Properties.Settings.Default.path_genia, Properties.Settings.Default.file_type_general);
                countFiles = generalFiles.Count;

                if (generalFiles.Count == 0)
                {
                    
                    if (wNewFile.Show("", "nombre", System.Windows.MessageBoxButton.OKCancel) == System.Windows.MessageBoxResult.OK)
                    {
                        string result = wNewFile._texboxResult;
                        if (string.IsNullOrEmpty(result))
                            infoFileName = "No_Info";
                        else
                            infoFileName = result;
                    }
                    actualFileSystem = archivos.crearArchivoInventario(Properties.Settings.Default.path_genia, Properties.Settings.Default.file_type_general, invSel, "", infoFileName);
                    Properties.Settings.Default.last_file_gen = actualFileSystem;
                    generalFiles = archivos.getFiles(Properties.Settings.Default.path_genia, Properties.Settings.Default.file_type_general);
                    return;
                }
                List<string> erasingFiles = new List<string>();
                List<string> openFiles = new List<string>();
                sumFilesClosed = 0;
                bool existOpenFile = false;

                foreach (string file in generalFiles)
                {
                    bool fileClosed = false;//archivos.fileIsClose(actualFileSystem, out message);
                    using (StreamReader stream = new StreamReader(file))
                    {
                        if ((line = stream.ReadLine()) != null)
                        {
                            string[] array = line.Split(';');
                            if (array.Length >= 6)//tamaño para archivo cerrado
                            {
                                fileClosed = true;
                                if (int.TryParse(array[5], out numReg)) 
                                {
                                    items.Add(numReg.ToString() + ";" + fileClosed.ToString() + ";" + file);
                                    sumFilesClosed += numReg;
                                }
                            }
                            else if (actualFileSystem != file && actualFileSystem != null && fileClosed)
                            {
                                erasingFiles.Add(file);
                            }
                            else if (array.Length >= 5)
                            {
                                existOpenFile = true;
                                //se usa 0 porque no se muestra, este valor se muestra en proceso
                                items.Add("0;" + fileClosed.ToString() + ";" + file);
                                openFiles.Add(file);
                            }
                            
                        }
                    }
                }
                int fileOpenCount = openFiles.Count;
                foreach (string openFile in openFiles)
                {
                    //verificar si no deja archivos abiertos debe crear
                    if (openFile != actualFileSystem && actualFileSystem != null)
                    {
                        //decremento el número de archivos abiertos para verificar si se cierran todos
                        fileOpenCount--;
                        //archivos.closeFile(openFile, out message);
                    }
                }
                //para que se cree el archivo en caso de que se hayan cerrado todos los archivos abiertos
                if (fileOpenCount <= 0)
                    existOpenFile = false;
                foreach (string fileErase in erasingFiles)
                {
                    File.Delete(fileErase);
                }
            }
            catch { }
        }
        delegate void UpdateItemCollectionHandler(List<int> update_icFiles);
        
        void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            timer.Enabled = false;
            timer.Stop();
            FileReview();
            //loadUserControl();
            //if (countSynchronized.HasValue)
            //    tblNumTags.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Send, new UpdateTextHandler(updateSynchronized_textBox), countSynchronized.ToString());
            tblNumFiles.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Send, new UpdateTextHandler(updateNumFiles_textBox), countFiles.ToString());
            timer.Enabled = true;
            timer.Start();
        }
        delegate void UpdateTextHandler(string updatedText);
        void updateNumFiles_textBox(string updatedText)
        {
            tblNumFiles.Text = updatedText;
        }
        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            timer.Enabled = false;
            timer.Stop();
        }
        private void btnSync_Click(object sender, RoutedEventArgs e)
        {
            bool closeFile=true;
            //si no desea sincronizar crear nuevo archivo
            if (WpfMessageBox.Show("", Properties.Resources.file_q_sync, MessageBoxButton.YesNo) != MessageBoxResult.Yes)
            {
                if (WpfMessageBox.Show("", Properties.Resources.file_q_sync_no_db, MessageBoxButton.YesNo) != MessageBoxResult.Yes)
                    return;
                else
                    NewFile();
                return;
            }
            string message = "";
            
            
            timer.Enabled = false;
            timer.Stop();

            System.Threading.Thread.Sleep(700);//espera para que actualice el número de tags
            string invSel = Properties.Settings.Default.last_id_inventory;

            isSyncActive = true;

            //if (WpfMessageBox.Show("", Properties.Resources.file_q_sync_no_db, MessageBoxButton.YesNo) != MessageBoxResult.Yes)
            //    return;
            //else
            //    NewFile();

            parentWindow.ModalDialog.ShowHandlerDialog(Properties.Resources.file_sync);
            System.ComponentModel.BackgroundWorker bw = new System.ComponentModel.BackgroundWorker();

            bw.DoWork += (s, args) =>
            {
                archivos.syncFile(actualFileSystem, invSel, closeFile, out message);
                
            };
            bw.RunWorkerCompleted += (s, args) =>
            {
                parentWindow.ModalDialog.HideHandlerDialog();
                if (!string.IsNullOrEmpty(message))
                    WpfMessageBox.Show("", message);
                NewFile();
                timer.Enabled = true;
                timer.Start();
                isSyncActive = false;
            };
            int cnt = 0;
            for (cnt = 0; cnt < 5; cnt++)
            {
                if (!bw.IsBusy) //Esto hace que no se crucen los hilos
                {
                    bw.RunWorkerAsync();
                    break;
                }
            }
        }
        public void NewFile()
        {
            if (string.IsNullOrEmpty(InvType))
                InvType = InventoryType;
            string invSel = Properties.Settings.Default.last_id_inventory;
            string infoFileName = "NoInfo";
            if (wNewFile.Show("", Properties.Resources.file_create_message, System.Windows.MessageBoxButton.OKCancel) == System.Windows.MessageBoxResult.OK)
            {
                string result = wNewFile._texboxResult;
                if (!string.IsNullOrEmpty(result))
                    infoFileName = result;
            }
            actualFileSystem = archivos.crearArchivoInventario(Properties.Settings.Default.path_genia, InvType, invSel, "", infoFileName);
            switch (InvType)
            {
                case "ig":
                    Properties.Settings.Default.last_file_gen = actualFileSystem;
                    break;
                case "if":
                    Properties.Settings.Default.last_file_missing = actualFileSystem;
                    break;
            }
            

            Properties.Settings.Default.Save();
            existOpenFile = true;
        }
        public void NewFileMissing()
        {
            string invSel = Properties.Settings.Default.last_id_inventory;
            string infoFileName = "NoInfo";
            if (wNewFile.Show("", Properties.Resources.file_create_message, System.Windows.MessageBoxButton.OKCancel) == System.Windows.MessageBoxResult.OK)
            {
                string result = wNewFile._texboxResult;
                if (!string.IsNullOrEmpty(result))
                    infoFileName = result;
            }
            actualFileSystem = archivos.crearArchivoInventario(Properties.Settings.Default.path_genia, Properties.Settings.Default.file_type_missing, invSel, "", infoFileName);
            Properties.Settings.Default.last_file_missing = actualFileSystem;
            Properties.Settings.Default.Save();
            existOpenFile = true;
        }
    }
}
