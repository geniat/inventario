﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GBInventario.Models
{
    class mInventory
    {
    }
    public class Inventario
    {
        public string id { get; set; }
        public string nombre { get; set; }
        public DateTime fecha_ini { get; set; }
        public int estado { get; set; }
        public int biblioteca { get; set; }
        public int tipo { get; set; }
    }
}
