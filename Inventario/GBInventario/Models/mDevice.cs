﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GBInventario.Models
{
    class mDevice
    {
    }
    public class Device
    {
        public string name;
        public string serialPort;
        public bool connect;
        public byte[] address;

        public Device()
        { 
        }

        public Device(string name, string serialPort, bool connect, byte[] address)
        {
            this.name = name;
            this.serialPort = serialPort;
            this.connect = connect;
            this.address = address;
        }
    }
}
