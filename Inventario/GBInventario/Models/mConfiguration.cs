﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;

namespace GBInventario.Models
{
    public class mConfiguration
    {
        private string directoryGenia = @"c:\Genia\";
        private string fileConfig = @"geninventory.gtconfig";

        private string _ConnStringPostgresGenia;
        private int _menu_max_width;
        private int _menu_min_width;
        private string _path_genia;
        private string _id_proceso_rfid;
        private string _file_encabezado_general;
        private string _file_encabezado_faltantes;
        private int _size_list_tags_update;
        private string _inventory_type;
        private int _db_colexs_id;
        private int _db_colexs_barcode;
        private int _db_colexs_epc;
        private int _db_colexs_tag_id;
        private int _db_colexs_nombre;
        private int _db_colexs_descripcion;
        private int _db_colexs_ubicacion;
        private int _db_colexs_usuario;
        private int _db_colexs_observacion;
        private int _db_colexs_fecha;
        private int _db_colexs_clasificacion;
        private int _db_colexs_categoria;
        private int _db_colexs_volumen;
        private int _db_colexs_ejemplar;
        private int _db_colexs_tomo;
        private int _db_colexs_prestado;
        private bool _db_colexs_head;
        private string _db_colexs_prestado_true;
        private string _com_ip30_BaudRate;
        private string _com_ip30_Firmware;
        private string _ConfigFechaHora;
        private string _ConfigFechaHoraFiles;
        private string _postgresConnSiabuc;
        private string _linkWebService;
        private string _tipoIntegracion;
        private string _AttribIp30;
        private int _frecuenciaLectura;
        private string _idCliente;
        private int _numreginsert;
        private string _providerName;
        private string _GeniaWebServiceUri;

        public mConfiguration(out string message)
        {
            message = "";
            bool doneConfig = false;

            string fileName = Path.Combine(directoryGenia, fileConfig);
            if (!File.Exists(fileName))
                message = "Local configuration file not exists";
            using (StreamReader sr = new StreamReader(fileName))
            {
                String line;

                while ((line = sr.ReadLine()) != null)
                {
                    if (line == "//" && doneConfig)
                        break;
                    if (line == "//GeniaInventory" && !doneConfig)
                    {
                        doneConfig = true;
                        continue;
                    }

                    if (!doneConfig)
                        continue;
                    string[] arrLine = line.Split('|');

                    if (arrLine[0] == "userSettings")
                    {
                        switch (arrLine[1])
                        {
                            case "ConnStringPostgresGenia":
                                _ConnStringPostgresGenia = arrLine[2];
                                break;
                            case "menu_max_width":
                                _menu_max_width = int.Parse(arrLine[2]);
                                break;
                            case "menu_min_width":
                                _menu_min_width = int.Parse(arrLine[2]);
                                break;
                            case "path_genia":
                                _path_genia = arrLine[2];
                                break;
                            case "id_proceso_rfid":
                                _id_proceso_rfid = arrLine[2];
                                break;
                            case "file_encabezado_general":
                                _file_encabezado_general = arrLine[2];
                                break;
                            case "file_encabezado_faltantes":
                                _file_encabezado_faltantes = arrLine[2];
                                break;
                            case "size_list_tags_update":
                                _size_list_tags_update = int.Parse(arrLine[2]);
                                break;
                            case "inventory_type":
                                _inventory_type = arrLine[2];
                                break;
                            case "db_colexs_id":
                                _db_colexs_id = int.Parse(arrLine[2]);
                                break;
                            case "db_colexs_barcode":
                                _db_colexs_barcode = int.Parse(arrLine[2]);
                                break;
                            case "db_colexs_epc":
                                _db_colexs_epc = int.Parse(arrLine[2]);
                                break;
                            case "db_colexs_tag_id":
                                _db_colexs_tag_id = int.Parse(arrLine[2]);
                                break;
                            case "db_colexs_nombre":
                                _db_colexs_nombre = int.Parse(arrLine[2]);
                                break;
                            case "db_colexs_descripcion":
                                _db_colexs_descripcion = int.Parse(arrLine[2]);
                                break;
                            case "db_colexs_ubicacion":
                                _db_colexs_ubicacion = int.Parse(arrLine[2]);
                                break;
                            case "db_colexs_usuario":
                                _db_colexs_usuario = int.Parse(arrLine[2]);
                                break;
                            case "db_colexs_observacion":
                                _db_colexs_observacion = int.Parse(arrLine[2]);
                                break;
                            case "db_colexs_fecha":
                                _db_colexs_fecha = int.Parse(arrLine[2]);
                                break;
                            case "db_colexs_clasificacion":
                                _db_colexs_clasificacion = int.Parse(arrLine[2]);
                                break;
                            case "db_colexs_categoria":
                                _db_colexs_categoria = int.Parse(arrLine[2]);
                                break;
                            case "db_colexs_volumen":
                                _db_colexs_volumen = int.Parse(arrLine[2]);
                                break;
                            case "db_colexs_ejemplar":
                                _db_colexs_ejemplar = int.Parse(arrLine[2]);
                                break;
                            case "db_colexs_tomo":
                                _db_colexs_tomo = int.Parse(arrLine[2]);
                                break;
                            case "db_colexs_prestado":
                                _db_colexs_prestado = int.Parse(arrLine[2]);
                                break;
                            case "db_colexs_head":
                                _db_colexs_head = bool.Parse(arrLine[2]);
                                break;
                            case "db_colexs_prestado_true":
                                _db_colexs_prestado_true = arrLine[2];
                                break;
                            case "com_ip30_BaudRate":
                                _com_ip30_BaudRate = arrLine[2];
                                break;
                            case "com_ip30_Firmware":
                                _com_ip30_Firmware = arrLine[2];
                                break;
                            case "ConfigFechaHora":
                                _ConfigFechaHora = arrLine[2];
                                break;
                            case "ConfigFechaHoraFiles":
                                _ConfigFechaHoraFiles = arrLine[2];
                                break;
                            case "postgresConnSiabuc":
                                _postgresConnSiabuc = arrLine[2];
                                break;
                            case "linkWebService":
                                _linkWebService = arrLine[2];
                                break;
                            case "tipoIntegracion":
                                _tipoIntegracion = arrLine[2];
                                break;
                            case "AttribIp30":
                                _AttribIp30 = arrLine[2];
                                break;
                            case "frecuenciaLectura":
                                _frecuenciaLectura = int.Parse(arrLine[2]);
                                break;
                            case "idCliente":
                                _idCliente = arrLine[2];
                                break;
                            case "numreginsert":
                                _numreginsert = int.Parse(arrLine[2]);
                                break;
                            case "GeniaWebServiceUri":
                                _GeniaWebServiceUri = arrLine[2];
                                break;
                        }
                    }
                    else if (arrLine[0] == "connectionStrings")
                    {
                        _providerName = arrLine[2];
                        _ConnStringPostgresGenia = arrLine[3];
                    }
                }
            }
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var connectionStringsSection = (ConnectionStringsSection)config.GetSection("connectionStrings");
            connectionStringsSection.ConnectionStrings["ConnStringPostgresGenia"].ProviderName = _providerName;
            connectionStringsSection.ConnectionStrings["ConnStringPostgresGenia"].ConnectionString = _ConnStringPostgresGenia;
            config.Save();
            setProperties();
        }
        private void setProperties()
        {
            Properties.Settings.Default.com_ip30_BaudRate = com_ip30_BaudRate;
            Properties.Settings.Default.com_ip30_Firmware = com_ip30_Firmware;
            Properties.Settings.Default.db_colexs_barcode = db_colexs_barcode;
            Properties.Settings.Default.db_colexs_categoria = db_colexs_categoria;
            Properties.Settings.Default.db_colexs_clasificacion = db_colexs_clasificacion;
            Properties.Settings.Default.db_colexs_descripcion = db_colexs_descripcion;
            Properties.Settings.Default.db_colexs_ejemplar = db_colexs_ejemplar;
            Properties.Settings.Default.db_colexs_epc = db_colexs_epc;
            Properties.Settings.Default.db_colexs_fecha = db_colexs_fecha;
            Properties.Settings.Default.db_colexs_head = db_colexs_head;
            Properties.Settings.Default.db_colexs_id = db_colexs_id;
            Properties.Settings.Default.db_colexs_nombre = db_colexs_nombre;
            Properties.Settings.Default.db_colexs_observacion = db_colexs_observacion;
            Properties.Settings.Default.db_colexs_prestado = db_colexs_prestado;
            Properties.Settings.Default.db_colexs_prestado_true = db_colexs_prestado_true;
            Properties.Settings.Default.db_colexs_tag_id = db_colexs_tag_id;
            Properties.Settings.Default.db_colexs_tomo = db_colexs_tomo;
            Properties.Settings.Default.db_colexs_ubicacion = db_colexs_ubicacion;
            Properties.Settings.Default.db_colexs_usuario = db_colexs_usuario;
            Properties.Settings.Default.db_colexs_volumen = db_colexs_volumen;
            Properties.Settings.Default.file_encabezado_faltantes = file_encabezado_faltantes;
            Properties.Settings.Default.file_encabezado_general = file_encabezado_general;
            Properties.Settings.Default.id_proceso_rfid = id_proceso_rfid;
            Properties.Settings.Default.inventory_type = inventory_type;
            Properties.Settings.Default.menu_max_width = menu_max_width;
            Properties.Settings.Default.menu_min_width = menu_min_width;
            Properties.Settings.Default.path_genia = path_genia;
            Properties.Settings.Default.size_list_tags_update = size_list_tags_update;

            Properties.Settings.Default.AttribIp30 = AttribIp30;
            Properties.Settings.Default.ConfigFechaHora = ConfigFechaHora;
            Properties.Settings.Default.ConfigFechaHoraFiles = ConfigFechaHoraFiles;
            Properties.Settings.Default.frecuenciaLectura = frecuenciaLectura;
            Properties.Settings.Default.idCliente = idCliente;
            Properties.Settings.Default.linkWebService = linkWebService;
            Properties.Settings.Default.numreginsert = numreginsert;
            Properties.Settings.Default.tipoIntegracion = tipoIntegracion;

            Properties.Settings.Default.providerName = providerName;
            Properties.Settings.Default.ConnStringPostgresGenia = ConnStringPostgresGenia;
        }
        public string ConnStringPostgresGenia
        {
            get { return _ConnStringPostgresGenia; }
        }
        public int menu_max_width
        {
            get { return _menu_max_width; }
        }
        public int menu_min_width
        {
            get { return _menu_min_width; }
        }
        public string path_genia
        {
            get { return _path_genia; }
        }
        public string id_proceso_rfid
        {
            get { return _id_proceso_rfid; }
        }
        public string file_encabezado_general
        {
            get { return _file_encabezado_general; }
        }
        public string file_encabezado_faltantes
        {
            get { return _file_encabezado_faltantes; }
        }
        public int size_list_tags_update
        {
            get { return _size_list_tags_update; }
        }
        public string inventory_type
        {
            get { return _inventory_type; }
        }
        public int db_colexs_id
        {
            get { return _db_colexs_id; }
        }
        public int db_colexs_barcode
        {
            get { return _db_colexs_barcode; }
        }
        public int db_colexs_epc
        {
            get { return _db_colexs_epc; }
        }
        public int db_colexs_tag_id
        {
            get { return _db_colexs_tag_id; }
        }
        public int db_colexs_nombre
        {
            get { return _db_colexs_nombre; }
        }
        public int db_colexs_descripcion
        {
            get { return _db_colexs_descripcion; }
        }
        public int db_colexs_ubicacion
        {
            get { return _db_colexs_ubicacion; }
        }
        public int db_colexs_usuario
        {
            get { return _db_colexs_usuario; }
        }
        public int db_colexs_observacion
        {
            get { return _db_colexs_observacion; }
        }
        public int db_colexs_fecha
        {
            get { return _db_colexs_fecha; }
        }
        public int db_colexs_clasificacion
        {
            get { return _db_colexs_clasificacion; }
        }
        public int db_colexs_categoria
        {
            get { return _db_colexs_categoria; }
        }
        public int db_colexs_volumen
        {
            get { return _db_colexs_volumen; }
        }
        public int db_colexs_ejemplar
        {
            get { return _db_colexs_ejemplar; }
        }
        public int db_colexs_tomo
        {
            get { return _db_colexs_tomo; }
        }
        public int db_colexs_prestado
        {
            get { return _db_colexs_prestado; }
        }
        public bool db_colexs_head
        {
            get { return _db_colexs_head; }
        }
        public string db_colexs_prestado_true
        {
            get { return _db_colexs_prestado_true; }
        }
        public string com_ip30_BaudRate
        {
            get { return _com_ip30_BaudRate; }
        }
        public string com_ip30_Firmware
        {
            get { return _com_ip30_Firmware; }
        }
        public string ConfigFechaHora
        {
            get { return _ConfigFechaHora; }
        }
        public string ConfigFechaHoraFiles
        {
            get { return _ConfigFechaHoraFiles; }
        }
        public string postgresConnSiabuc
        {
            get { return _postgresConnSiabuc; }
        }
        public string linkWebService
        {
            get { return _linkWebService; }
        }
        public string tipoIntegracion
        {
            get { return _tipoIntegracion; }
        }
        public string AttribIp30
        {
            get { return _AttribIp30; }
        }
        public int frecuenciaLectura
        {
            get { return _frecuenciaLectura; }
        }
        public string idCliente
        {
            get { return _idCliente; }
        }
        public int numreginsert
        {
            get { return _numreginsert; }
        }
        public string providerName
        {
            get { return _providerName; }
        }
        public string GeniaWebServiceUri
        {
            get { return _GeniaWebServiceUri; }
        }
    }
}
