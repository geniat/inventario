﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GBInventario.Models
{
    public class mTag
    {
        public string epc;
        public string fecha;

        public mTag()
        {
        }
        public mTag(string epc, string fecha)
        {
            this.epc = epc;
            this.fecha = fecha;
        }
    }
}
