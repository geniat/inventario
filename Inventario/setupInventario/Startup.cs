﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(setupInventario.Startup))]
namespace setupInventario
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
