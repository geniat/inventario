﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using TechnologySolutions.AsciiProtocolSample;

namespace ControlTSL
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Decoder dec = new Decoder("034082");
            string respuesta =  dec.HexToString(dec.LimpiarEpc(dec.StringToHex()));
        }
    }
}
