﻿
namespace TechnologySolutions.AsciiProtocolSample.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public interface IResponseSettings
    {
        bool IsProtocolResponsePanelVisible { get; set; }
    }
}
