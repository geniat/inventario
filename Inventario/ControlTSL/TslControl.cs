﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace ControlTSL
{

    using TechnologySolutions.AsciiProtocolSample;
    using TechnologySolutions.AsciiProtocolSample.ViewModels;
    //using ViewModels;
    using Views;
    using TechnologySolutions.Rfid.AsciiProtocol;
    using TechnologySolutions.AsciiProtocolSample.Services;
    using TechnologySolutions.Rfid.AsciiProtocol.Commands;
    using System.IO;

    public class TslControl
    {
        private ConnectViewModel connectViewModel;

        private ResponsesViewModel responseViewModel;

        private InventoryViewModel inventory;

        private ReadWriteViewModel viewModel;

        private BarcodeViewModel barcodeViewModel;


        //Encargado de manejar los eventos de lectura de Tags y Barcode

        CommandService service;

        //CommandService service;


        //Establace si el dispositivo está habilitado para lectura                 
        public bool habilitado;

        //Establece si se ha realizado la conexión
        public bool conexionEstablecida;

        //Tipo de proceso que va a realizarse con el equipo - Inventario, Asociación.
        int proceso;
        string bufferEpc;
        string bufferData;
        public string bufferAcq { get; set; }


        //PARÁMETROS ASOCIACIÓN
        Decoder decoder;
        public string tagAsociacion { get; set; }
        public string barcodeAsociacion { get; set; }
        Service ser;

        //PARÁMETROS INVENTARIO
        public string idInventario { get; set; }


        //PARÁMETROS ESTACIONES
        private string port;
        public int potencia;
        public List<string[]> ejemplaresDevuelto;
        public List<string> mensajesDevolucion;

        public List<string> tagsLeidos { get; set; }
        public string tagActual;
        public string fechaActual;
        public string path;
        // A delegate type for hooking up change notifications.
        public delegate void ChangedEventHandler(object sender, EventArgs e);


        public event ChangedEventHandler Changed;


        public TslControl(int proceso)
        {
            this.proceso = proceso;
            ser = new Service();
            System.ComponentModel.Design.ServiceContainer container = new System.ComponentModel.Design.ServiceContainer();
            ser.RegisterServices(container);
            service = ser.commands;
            tagsLeidos = new List<string>();
            //ser.RegisterMainView(this);

            this.responseViewModel = ViewModelLocator.ViewModel<ResponsesViewModel>();
            this.connectViewModel = ViewModelLocator.ViewModel<ConnectViewModel>();
            this.inventory = ViewModelLocator.ViewModel<InventoryViewModel>();
            this.viewModel = ViewModelLocator.ViewModel<ReadWriteViewModel>();
            this.barcodeViewModel = ViewModelLocator.ViewModel<BarcodeViewModel>();

            if (proceso == 4)
            {
                port = ConfigurationManager.AppSettings["PuertoTSL"];
                potencia = Convert.ToInt32(ConfigurationManager.AppSettings["PotenciaTSL"]);
            }


        }

        protected virtual void OnChanged(EventArgs e)
        {
            if (Changed != null)
                Changed(this, e);
        }


        public bool Connect(string comPort, out string message)
        {
            message = "";
            bool resp = false;
            //timerLectura.Tick += new EventHandler(timerLectura_Tick);
            //this.connectViewModel = new ConnectViewModel();
            this.connectViewModel.PortName = comPort;
            try
            {
                if (!conexionEstablecida)
                {
                    connectViewModel.Connect();
                    if (this.connectViewModel.IsConnected)
                    {
                        resp = true;
                        conexionEstablecida = true;
                        this.service.inventoryCommand.TransponderReceived += InventoryCommand_TransponderReceived;
                        this.service.barcodeCommand.BarcodeReceived += BarcodeCommand_BarcodeReceived;
                    }
                    else
                    {
                        resp = false;
                        conexionEstablecida = false;
                    }
                }
                else
                {
                    resp = true;
                    conexionEstablecida = true;
                }

            }
            catch (Exception e)
            {
                System.Diagnostics.StackFrame callStack = new System.Diagnostics.StackFrame(1, true);
                message = e.Message + "|" + callStack.GetFileName() + "|" + callStack.GetFileLineNumber();
                resp = false;
                conexionEstablecida = false;
            }

            return resp;


        }

        public bool ConexionAutomatica()
        {
            bool conectado = false;

            this.connectViewModel.RefreshPorts();
            //this.mainViewModel.TransponderMessage += MainViewModel_TransponderMessage;
            IEnumerable<string> puertos = this.connectViewModel.PortNames;       //agrega los puertos
            
            foreach (string puerto in puertos)
            {
                if (connectViewModel.IsConnected)
                {
                    Disconnect();
                }
                //this.connectViewModel.PortName = puerto.Substring(0, 4);
                this.connectViewModel.PortName = puerto;
                //this.connectViewModel.PortName = "COM4";
                this.connectViewModel.Connect();
                //this.connectViewModel.
                if (this.connectViewModel.IsConnected)
                {
                    if (this.inventory.ExecuteInventoryCommandSynchronouslyPrueba())
                    {
                        conectado = true;
                        conexionEstablecida = true;
                        break;
                    }
                }

            }

            if (conectado)
            {
                this.service.inventoryCommand.TransponderReceived += InventoryCommand_TransponderReceived;
                this.service.barcodeCommand.BarcodeReceived += BarcodeCommand_BarcodeReceived;
            }
            return conectado;
        }

        private void BarcodeCommand_BarcodeReceived(object sender, BarcodeEventArgs e)
        {
            //System.Windows.Forms.MessageBox.Show(e.Barcode);
            if (habilitado && proceso == 0)
            {
                // procesarLectura(tag);
            }
            //else if (habilitado && proceso == 3)
            else if (habilitado && proceso == 3)
            {

                barcodeAsociacion = e.Barcode;
                OnChanged(EventArgs.Empty);
            }
            //

        }

        private void InventoryCommand_TransponderReceived(object sender, TransponderDataEventArgs e)
        {
            string tag = "";
            string acq = "";
            try
            {
                switch (proceso)
                {
                    case 0:
                        if (!habilitado)
                            break;
                        if (tagsLeidos.Contains(e.Transponder.Epc))
                            break;
                        tagsLeidos.Add(e.Transponder.Epc);
                        break;
                    case 1:
                        if (!habilitado)
                            break;
                        if (acq.Equals("Error"))
                            break;
                        tag = e.Transponder.Epc;
                        decoder = new Decoder();
                        acq = decoder.LimpiarEpc(e.Transponder.Epc);
                        acq = decoder.HexToString(acq);
                        try
                        {
                            acq = acq.Substring(0, acq.IndexOf('\0'));
                        }
                        catch { }
                        OnChanged(EventArgs.Empty);
                        break;
                    case 3:
                        if (!habilitado)
                            break;
                        tagAsociacion = e.Transponder.Epc;
                        OnChanged(EventArgs.Empty);
                        break;
                }
            }
            catch { }
        }

        public void Disconnect()
        {
            connectViewModel.Disconnect();
            this.service.Dispose();
            ser.Dispose();
            
        }

        public void Write(string mask, string acq, out string message)
        {
            message = "";
            string respuesta = "";
            decoder = new Decoder(acq);
            acq = decoder.StringToHex();

            try
            {
                viewModel.ExecuteWriteSingleUser(mask, acq);
                respuesta = viewModel.mensajeData.ToString();
                if (respuesta.Contains("Words Written:"))
                {
                    int words = Int32.Parse(respuesta.Replace("Words Written:", "#").Split('#')[1]);
                    if (words > 0)
                    {
                        System.Windows.Forms.MessageBox.Show("Escritura realizada con éxito");
                    }
                    else
                    {
                        System.Windows.Forms.MessageBox.Show(respuesta);
                    }
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show(respuesta);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.StackFrame callStack = new System.Diagnostics.StackFrame(1, true);
                message = e.Message + "|" + callStack.GetFileName() + "|" + callStack.GetFileLineNumber();
                throw e;
            }

        }

        public void Activar(string mask, int accion, out string message)
        {
            message = "";
            string respuesta = "";
            try
            {
                
                viewModel.ExecuteWriteActivator(mask, accion, out message);
                respuesta = viewModel.mensajeData.ToString();
                if (respuesta.Contains("Words Written:"))
                {
                    int words = Int32.Parse(respuesta.Replace("Words Written:", "#").Split('#')[1]);
                    if (words > 0)
                    {
                        System.Windows.Forms.MessageBox.Show("Proceso realizado con éxito");
                    }
                    else
                    {
                        System.Windows.Forms.MessageBox.Show(respuesta);
                    }
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show(respuesta);
                }

            }
            catch (Exception e)
            {
                System.Diagnostics.StackFrame callStack = new System.Diagnostics.StackFrame(1, true);
                message = e.Message + "|" + callStack.GetFileName() + "|" + callStack.GetFileLineNumber();
                throw e;
            }

        }

        public void ForzarLectura()
        {
            //ControlEstacionExterna controlExterna = new Control.ControlEstacionExterna();
            //ControlEjemplares controlEjemplares = new ControlEjemplares();


            //Sensor = valorSensor;
            //System.Windows.Forms.Button test = new System.Windows.Forms.Button();
            //inventory.conexion = false;
            //CommandBinder command = new ButtonBinder(test, inventory.InventoryAsynchronous);
            //CommandBinder command = new ButtonBinder(test, inventory.InventorySynchronous);
            //test.PerformClick();
            //command.Command.Execute();


            using (var commander = new AsciiCommander())
            {
                commander.AddResponder(new LoggerResponder());
                commander.AddSynchronousResponder();


                //for (int i = 0; i < 1; i++)
                //{
                commander.Connect(new SerialPortWrapper(port));
                var Inventory = new InventoryCommand();
                Inventory.OutputPower = potencia;

                commander.ExecuteCommand(Inventory, Inventory.Responder);

                ejemplaresDevuelto = new List<string[]>();
                mensajesDevolucion = new List<string>();
                foreach (var tag in Inventory.Transponders)
                {
                    //tagsLeidos

                    //string mensaje = "";
                    //string correo = "";
                    //INSERTAR TAGS DETECTADOS
                    //controlExterna.generar_evento_estacion_externa(tag.Epc);
                    // string barcode = controlExterna.getBarcode(tag.ToString());
                    //string[] ejemplar = controlEjemplares.getEjemplarPorTag(tag.Epc);
                    //string[] ejemplarDevuelto = new string[6];

                    //if (!String.IsNullOrEmpty(ejemplar[0]))
                    //{
                    //    controlExterna.devolverEjemplar(ejemplar[0], "", out mensaje, out correo);
                    //    ejemplarDevuelto = ejemplar;
                    //    string mensajeDevolucion = mensaje;
                    //    //mensajeDevolucion = "ha sido devuelto con éxito";
                    //    ejemplaresDevuelto.Add(ejemplarDevuelto);
                    //    mensajesDevolucion.Add(mensajeDevolucion);
                    //}

                    OnChanged(EventArgs.Empty);


                }

                commander.Disconnect();

            }
        }

        public bool isConnect()
        {
            return connectViewModel.IsConnected;
        }



    }
}
