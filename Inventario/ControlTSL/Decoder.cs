﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechnologySolutions.Rfid.AsciiProtocol;

namespace TechnologySolutions.AsciiProtocolSample
{
    class Decoder
    {
        string input { get; set; }
        string data { get; set;  }

        Databank bank { get; set; }
        public  Decoder(string input){
           // this.data = data;
            this.input = input;
        }

        public Decoder() { }

        public string StringToHex()
        {
            // int len = data.Length;
            string output = "";
            bank = Databank.ElectronicProductCode;
            switch (bank)
            {
                case Databank.User:

                    break;
                case Databank.ElectronicProductCode:
                    string salida = "F";

                    //AÑADIR CÓDIGO DE BIBLIOTECA 0-99
                    salida += "99";
                    
                    if(input.Length <= 10)
                    {
                       
                        foreach(char letter in input.ToCharArray())
                        {
                            // Get the integral value of the character.
                            int value = Convert.ToInt32(letter);
                            // Convert the decimal value to a hexadecimal value in string form.
                            string hexOutput = String.Format("{0:X}", value);
                            salida += hexOutput;
                        }
                    }
                    else
                    {
                        ;
                    }
                    if(salida.Length < 24)
                    {
                        //int diferencia = 24 - salida.Length;
                        while(salida.Length < 23)
                        {
                            salida += "0";
                        }
                        salida += "F";
                    }
                    output = salida;
                    break;
                case Databank.Reserved:
                    break;
                case Databank.TransponderIdentifier:
                    break;
            }

            return output;


        }

        public string HexToString(string str)
        {
            var sb = new StringBuilder();
            for (var i = 0; i < str.Length; i += 2)
            {
                var hexChar = str.Substring(i, 2);
                sb.Append((char)Convert.ToByte(hexChar, 16));
            }
            return sb.ToString();
        }

        public string LimpiarEpc(string epc)
        {
            string respuesta = "";
            //epc.Substring(3,4)
            if (epc.Substring(0, 1).Equals("F") && epc.Substring(epc.Length - 1, 1).Equals("F"))
                respuesta = epc.Substring(3, (epc.Length - 1) - 3);
            else
                respuesta = "Error";
            return respuesta;
        }

        /*

        char[] values = input.ToCharArray();
           // string salida = "";
            foreach (char letter in values)
            {
                // Get the integral value of the character.
                int value = Convert.ToInt32(letter);
        // Convert the decimal value to a hexadecimal value in string form.
        string hexOutput = String.Format("{0:X}", value);
        salida += hexOutput;
                //Console.WriteLine("Hexadecimal value of {0} is {1}", letter, hexOutput);
            }
            if(salida.Length > 64)
            {
                for (int i = 0; i< 64; i++)
                {
                    salida += "0";
                }

                
            }
            else
            {
                int diferencia = 64 - salida.Length;
                for (int i = 0; i<diferencia; i++)
                {
                    salida += "0";
                }
            }
            return salida;
            */
    }
}
