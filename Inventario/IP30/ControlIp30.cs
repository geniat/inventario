﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;

namespace IP30
{
    public class ControlIp30
    {
        public bool isConnect;
        SerialPort ComPort = new SerialPort();
        string InputData = String.Empty;
        internal delegate void SerialDataReceivedEventHandlerDelegate(object sender, SerialDataReceivedEventArgs e);
        //string potenciaIp30 = ConfigurationManager.AppSettings["potenciaIp30"];
        public string comBaudRate;
        CapaBDD.ClsDatos datos = new CapaBDD.ClsDatos();
        int proceso;
        List<string> tags = new List<string>();
        public string queryRango;
        public string idInventario;
        public List<string> invPend = new List<string>();
        bool gatillo = false;

        public string tagLeido { get; set; }
        public List<string> tagsLeidos { get; set; }
        public string tagActual;
        public string fechaActual;
        //System.Windows.Forms.Timer timerLectura;
        System.Timers.Timer timerLectura;
        public string firmwareIP30;
        public string attribIp30;// = ConfigurationManager.AppSettings["AttribIp30"];

        public bool Connect(string comPort, int procesoSol)
        {
            tagsLeidos = new List<string>();
            
            bool resp = false;
            proceso = procesoSol;
            ComPort.PortName = Convert.ToString(comPort);
            if (!string.IsNullOrEmpty(comBaudRate))
                ComPort.BaudRate = Convert.ToInt32(comBaudRate);
            ComPort.DataBits = Convert.ToInt16("8");
            ComPort.StopBits = (StopBits)Enum.Parse(typeof(StopBits), "One");
            ComPort.Handshake = (Handshake)Enum.Parse(typeof(Handshake), "None");
            ComPort.Parity = (Parity)Enum.Parse(typeof(Parity), "None");
            ComPort.ReadTimeout = 500;
            ComPort.WriteTimeout = 500;
            //timerLectura = new System.Windows.Forms.Timer();
            //timerLectura.Tick += timerLectura_Tick;
            timerLectura = new System.Timers.Timer();
            timerLectura.Elapsed += timerLectura_Tick;
            int frecLectura = 2000;
            int.TryParse(ConfigurationManager.AppSettings["frecuenciaLectura"], out frecLectura);
            timerLectura.Interval = 2000;//frecLectura;//se cambia para probar en pereira, default 2000
            timerLectura.Enabled = true;

            try
            {
                if (ComPort.IsOpen)
                {
                    ComPort.Close();
                }
                ComPort.Open();
                ComPort.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(port_DataReceived);
                System.Threading.Thread.Sleep(2000);
                if (ComPort.IsOpen)
                {
                    configuracion();

                    //ComPort.WriteLine("SET MyMacRFON=\"R REPORT=EVENT\"");
                    //ComPort.WriteLine("SET MyMacRFOFF=\"R STOP\"");
                    //ComPort.WriteLine("TRIGGER \"TRIGPULL\" GPIOEDGE 1 0 FILTER 0 ACTION \"MyMacRFON\"");
                    //ComPort.WriteLine("TRIGGER \"TRIGRELEASE\" GPIOEDGE 1 1 FILTER 0 ACTION \"MyMacRFOFF\"");
                    //ComPort.WriteLine("ATTRIB IDTRIES=1");
                    //ComPort.WriteLine("ATTRIB ANTTRIES=1");
                    //ComPort.WriteLine("ATTRIB WRTRIES=3");
                    //ComPort.WriteLine("ATTRIB TAGTYPE=EPCC1G2");
                    //ComPort.WriteLine("READ EPCID REPORT=EVENTALL");
                }
                resp = true;
                isConnect = true;
            }
            catch (Exception e)
            {
                ComPort.Close();
                ComPort.Dispose();
                resp = false;
            }
            return resp;
        }

        private void TimerLectura_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            throw new NotImplementedException();
        }
        int contadorPing=0;
        void timerLectura_Tick(object sender, EventArgs e)
        {
            timerLectura.Stop();
            timerLectura.Enabled = false;
            contadorPing++;
            //bool error = false;
            try
            {
                if(!ComPort.IsOpen)
                {
                    gatillo = false;
                    isConnect = false;
                    return;
                }
                if (!gatillo && contadorPing >= 10)
                {
                    contadorPing = 0;
                    ComPort.WriteLine("PING");
                }
                if (gatillo)
                {
                    ComPort.WriteLine("READ");
                    
                }
            }
            catch //(Exception ex)
            {
                gatillo = false;
                isConnect = false;
            }
            timerLectura.Enabled = true;
            timerLectura.Start();
        }

        public bool ConexionAutomatica()
        {
            bool con = false;

            try
            {
                if (ComPort.IsOpen)
                    ComPort.Close();

                List<string> puertosCom = obtenePuertosCom();

                foreach (string puerto in puertosCom)
                {
                    con = Connect(puerto, 0);//conecta con ip30

                    if (con)
                    {

                        con = true;
                        //System.Windows.Forms.MessageBox.Show("IP30 CONECTADA");
                        break;
                    }
                }
                if (!con)
                {
                    //System.Windows.Forms.MessageBox.Show("Imposilbe realizar la conexión");
                }
            }
            catch (Exception ex)
            {
            }

            return con;
        }

        private List<string> obtenePuertosCom()
        {
            List<string> tList = new List<string>();
            foreach (string s in System.IO.Ports.SerialPort.GetPortNames())
            {
                tList.Add(s);
            }
            return tList;
        }

        public void configuracion()
        {
            
            string[] arrAtrib = attribIp30.Split(';');
            foreach (string attr in arrAtrib)
            {
                ComPort.WriteLine(attr);
                System.Threading.Thread.Sleep(50);
            }
            ///FUNCIONA EN TODOS LOS CASOS Y en Pereira es suficiente con estos atributos
            //ComPort.WriteLine("ATTRIB FIELDSTRENGTH=25DB");
            //ComPort.WriteLine("ATTRIB TAGTYPE=EPCC1G2");
            //ComPort.WriteLine("ATTRIB BAUD=" + comBaudRate);
            //ComPort.WriteLine("SWVER");
            

            //ComPort.WriteLine("ATTRIB CHKSUM=OFF");
            //ComPort.WriteLine("ATTRIB IDREPORT=ON");
            //ComPort.WriteLine("ATTRIB NOTAGRPT=OFF");
            //ComPort.WriteLine("ATTRIB TIMEOUTMODE=ON");
            //ComPort.WriteLine("ATTRIB TTY=ON");
            //ComPort.WriteLine("ATTRIB DENSEREADERMODE=ON");
            //ComPort.WriteLine("ATTRIB ECHO=ON");
            //ComPort.WriteLine("ATTRIB XONXOFF=OFF");
            //ComPort.WriteLine("ATTRIB RDTRIES=3");
            //ComPort.WriteLine("ATTRIB IDTRIES=1");
            //ComPort.WriteLine("ATTRIB ANTTRIES=1");
            //ComPort.WriteLine("ATTRIB WRTRIES=3");
            //ComPort.WriteLine("ATTRIB LOCKTRIES=3");
            //ComPort.WriteLine("ATTRIB SELTRIES=1");
            //ComPort.WriteLine("ATTRIB UNSELTRIES=1");
            //ComPort.WriteLine("ATTRIB INITTRIES=1");
            //ComPort.WriteLine("ATTRIB IDTIMEOUT=100");
            //ComPort.WriteLine("ATTRIB RPTTIMEOUT=1000");
            //ComPort.WriteLine("ATTRIB ANTTIMEOUT=0");
            ////ComPort.WriteLine("ATTRIB INITIALQ=4");
            //ComPort.WriteLine("ATTRIB SESSION=0");
            //ComPort.WriteLine("ATTRIB EPCC1G2PARMS=2");
            //ComPort.WriteLine("ATTRIB SCHEDULEOPT=1");
            //ComPort.WriteLine("ATTRIB BTPWROFF=300");
            //System.Threading.Thread.Sleep(1000);
            ////ComPort.WriteLine("ATTRIBUTE");
            //ComPort.WriteLine("READ ANT TIME REPORT=NO");
            //ComPort.WriteLine("READ STOP");

            
            //con esta configuración espera el evento read para hacer una lectura de hasta 10 tags 
            //(esto está por defecto en pereira y si se envía se bloquea la ip30)
            //ComPort.WriteLine("ATTRIB SCHEDOPT=0");
            //ComPort.WriteLine("ATTRIB SESSION=1");
            //ComPort.WriteLine("ATTRIB IDTRIES=1");
            //ComPort.WriteLine("ATTRIB ANTTRIES=1");
            //ComPort.WriteLine("ATTRIB INITIALQ=1");
            //ComPort.WriteLine("READ REPORT = EVENT");
            //ComPort.WriteLine("READ STOP");

            //esta configuración inicia lectura al apretar el gatillo y 
            //continua automáticamente hasta que deja de tener tags al alcance
            //ComPort.WriteLine("ATTRIB SCHEDOPT=1");
            //ComPort.WriteLine("ATTRIB SESSION=1");
            //ComPort.WriteLine("ATTRIB IDTIMEOUT=5000");
            //ComPort.WriteLine("ATTRIB ANTTIMEOUT=5000");
            //ComPort.WriteLine("ATTRIB INITIALQ=4");
            //ComPort.WriteLine("READ REPORT = NO");
            //no estoy seguro con esté atributo no aparece en el manual
            //ComPort.WriteLine("READ STOP");
        }
        string TextAux;
        public DateTime? conectado;
        /// <summary>
        /// Cuando se presiona el gatillo de la pistola, se recibe un mensaje
        /// donde contiene el EPC
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                //System.Threading.Thread.Sleep(10);
                InputData = ComPort.ReadExisting();
                int data = InputData.Count();
                if (data == 0)
                    return;
                if (InputData.Length <= 10 && !InputData.EndsWith(">"))
                {
                    TextAux += InputData;
                    return;
                }
                if (!string.IsNullOrEmpty(TextAux))
                {
                    InputData = TextAux + InputData;
                    TextAux = "";
                }
                if (InputData.EndsWith(">"))
                    TextAux = "";
                //System.Threading.Thread.Sleep(500);
                if (InputData.Contains("EVT:TRI"))
                {
                    conectado = DateTime.Now;
                    if (InputData.Contains("GPIO 0"))
                    {
                        //ComPort.WriteLine("READ ANT TIME REPORT=NO");
                        //ComPort.WriteLine("READ");
                        timerLectura.Start();
                        timerLectura.Enabled = true;
                        gatillo = true;
                    }
                    if (InputData.Contains("GPIO 1"))
                    {
                        ////ComPort.WriteLine("READ ANT TIME REPORT=NO");
                        //ComPort.WriteLine("READ POLL");
                        ComPort.WriteLine("READ STOP");
                        //timerLectura.Stop();
                        //timerLectura.Enabled = false;
                        gatillo = false;
                    }
                }
                else if (InputData.Contains("H") && data > 25 && !InputData.Contains("ATTRIB"))
                {
                    conectado = DateTime.Now;
                    string[] arrTags = InputData.Split("\r\n".ToCharArray());
                    foreach (string tag in arrTags)
                    {
                        if (tag.Trim() == "READ" || tag.Trim() == "OK>" || string.IsNullOrEmpty(tag.Trim()))
                            continue;
                        int inicio = tag.IndexOf("H");
                        if (inicio == -1)
                            continue;
                        tagActual = tag.Substring(inicio + 1);
                        try
                        {
                            string tagepc = tagActual.Substring(0, 24);

                            if (!tagsLeidos.Contains(tagepc))
                            {
                                tagsLeidos.Add(tagepc);
                                tagActual = tagepc;
                            }
                        }
                        catch { }
                    }
                    //if (gatillo)
                    //ComPort.WriteLine("READGPI");
                }
                else if (InputData.Contains("SWVER"))
                {
                    int inicio = InputData.IndexOf("SWVER");
                    if (inicio == -1)
                        return;
                    firmwareIP30 = InputData.Substring(inicio);
                    int fin = firmwareIP30.IndexOf("OK");
                    firmwareIP30 = firmwareIP30.Substring(0, fin);
                    
                }

            }
                            //if (InputData.Contains("0\r\nOK"))
            //{
            //    ComPort.WriteLine("READ ANT TIME REPORT=EVENT");

                //}
            //else if (InputData.Contains("1\r\nOK"))
            //{
            //    ComPort.WriteLine("READ ANT TIME REPORT=NO");
            //    ComPort.WriteLine("READ STOP");

                //}



            catch { }
        }

        public void pingIp30()
        {
            ComPort.WriteLine("PING");
        }

        public void Trigger()
        {
            try
            {
                if (gatillo)
                {
                    for (int i = 0; i <= 3; i++)
                    {
                        ComPort.WriteLine("READGPI");
                    }
                    gatillo = false;

                }
            }
            catch (Exception ex)
            {

            }
        }

        public void Disconnect()
        {
            try
            {
                if (ComPort.IsOpen)
                {
                    ComPort.WriteLine("READ ANT TIME REPORT=NO");
                    ComPort.WriteLine("READ STOP");
                    System.Threading.Thread.Sleep(100);//Espera que llegue el OK
                    ComPort.Close();
                    ComPort.Dispose();
                }
                ComPort.Close();

            }
            catch (Exception e) { }

        }

        public bool ping()
        {
            bool Rping = false;
            try
            {
                if (ComPort.IsOpen)
                {
                    ComPort.WriteLine("PING");
                    System.Threading.Thread.Sleep(1);
                    String lectura = ComPort.ReadLine();


                    if (lectura.Contains("OK") || lectura.Contains("PING") || lectura.Contains("EVT") || lectura != "")
                    {
                        Rping = true;
                    }
                    else { Rping = false; }

                }
                else
                { //Rping = false; 
                }
            }
            catch (TimeoutException e)
            { //Rping = false;
                string error = e.Message;
                ComPort.Close();
            }

            return Rping;
        }

        public void SingleShotRead(string tag, int procesoSol)
        {
            string t = tag;

            switch (procesoSol)
            {
                case 0://inventario
                    insertInventario(t, queryRango, idInventario);
                    break;
                case 1:
                    //dist = t.TagFields.FieldArray[0].ToString();
                    //dist = t.TagFields.FieldArray[0].DataLong;//probar también con el filtro de .Read("9849284A");
                    //datos.exeQuery("INSERT INTO ejemplares.eventos_inventario(id_tag, fecha, estado) VALUES ('" + t + "', NOW(), 0);", false);
                    break;
                case 2://prestamos internos
                    datos.exeQuery("INSERT INTO ejemplares.eventos_prestamo(id_tag, fecha, estado) VALUES ('" + t + "', NOW(), 0);", false);
                    break;
                case 3://programación
                    datos.exeQuery("INSERT INTO genia.eventos_portal(id_tag, hora, id_antena, id_reader)VALUES ('" + t + "', NOW(), 1, 2);", false);
                    break;

            }

        }

        private bool insertInventario(String id_tag, string idSegmento, string idInventario)
        {
            bool retResp = false;
            ///////////////
            //obtenemos el id del inventario en curso con los datos de clasificación
            StringBuilder strb = new StringBuilder();
            strb.Append("SELECT ic.acq ");
            strb.Append("FROM ejemplares.ejemplares e ");
            strb.Append("LEFT JOIN (SELECT id, acq, estado, id_inventario FROM ejemplares.inventario_en_curso WHERE id_inventario = " + idInventario + " AND estado <> 1) ic ON e.acq = ic.acq ");//quemado el estado 1 es inventariado
            strb.Append("WHERE e.id_tag = '" + id_tag + "' ");
            strb.Append("AND ic.id BETWEEN " + idSegmento);
            strb.Append(";");

            string resp = datos.exeQuery(strb.ToString(), true);

            string acq = resp;
            int idInv = 0;
            if (!string.IsNullOrEmpty(acq))
            {
                strb = new StringBuilder();
                strb.Append("UPDATE ejemplares.inventario_en_curso SET estado = 1 WHERE acq='" + resp + "' AND id_inventario = " + idInventario + ";");
                resp = datos.exeQuery(strb.ToString(), false);

            }
            if (int.TryParse(resp, out idInv))
                if (idInv > 0)
                {
                    invPend.Add(acq);
                    retResp = true;
                }
            return retResp;
        }




    }
}
