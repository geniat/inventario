﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Tools
{

    public class Variables
    {
        private static Dictionary<string, object> _values = new Dictionary<string, object>();
        public void SetValue(string key, object value)
        {
            if (_values.ContainsKey(key))
            {
                _values.Remove(key);
            }
            _values.Add(key, value);
        }
        public T GetValue<T>(string key)
        {
            if (_values.ContainsKey(key))
            {
                return (T)_values[key];
            }
            else
            {
                return default(T);
            }
        }
    }
    public class FormatDate
    {
        public string configFechaHora;// = ConfigurationManager.AppSettings["ConfigFechaHora"];
        public string ConfigFechaHoraFiles;//= ConfigurationManager.AppSettings["ConfigFechaHoraFiles"];

        public FormatDate(string cFechaHora, string cFechaHoraFiles)
        {
            configFechaHora = cFechaHora;
            ConfigFechaHoraFiles = cFechaHoraFiles;
        }
        public string FormatoInsertFecha(DateTime fecha)
        {
            string resp = "";
            try
            {
                return fecha.ToString(configFechaHora);
            }
            catch
            {
                //string configFechaHora = obtenerValorParametro("ConfigFechaHora", " ").ToString();
                string[] arrConfigFechaHora = configFechaHora.Split(' ');
                string sepFecha = "";
                string sepHora = "";

                
                string dia = fecha.Day.ToString();
                string mes = fecha.Month.ToString();
                string year = fecha.Year.ToString();
                string hora = fecha.Hour.ToString();
                string min = fecha.Minute.ToString();
                string seg = fecha.Second.ToString();

                for (int i = 0; i < arrConfigFechaHora.Length; i++)
                {
                    //;:;dd;MM;yyyy;hh;mm;ss
                    if (i > 1)
                    {
                        switch (arrConfigFechaHora[i])
                        {
                            case "dd":
                                resp += dia;
                                break;
                            case "MM":
                                resp += mes;
                                break;
                            case "yyyy":
                                resp += year;
                                break;
                            case "hh":
                                resp += hora;
                                break;
                            case "mm":
                                resp += min;
                                break;
                            case "ss":
                                resp += seg;
                                break;
                        }
                        if (i < 4)
                            resp += sepFecha;
                        else if (i == 4)
                            resp += " ";
                        else if (i < 7)
                            resp += sepHora;
                    }
                    else if (i == 0)
                        sepFecha = arrConfigFechaHora[i];
                    else if (i == 1)
                        sepHora = arrConfigFechaHora[i];
                }
            }
            //resp = dia + "/" + mes + "/" +  year + " " + hora + ":" + min + ":" + seg;
            return resp;
        }
        public string FormatoInsertFecha(string fecha, string formato, out string message)
        {
            message = "";
            string resp = "";
            try
            {
                if (string.IsNullOrEmpty(fecha))
                    return null;
                DateTime fechaAux = Convert.ToDateTime(fecha);
                resp = FormatoInsertFecha(fechaAux);
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return resp;
        }
        public string FormatoFileFecha(DateTime fecha)
        {
            string resp = "";
            //ddMMyyyy_HHmmss
            resp = fecha.ToString(ConfigFechaHoraFiles);
            return resp;
        }
        public string FormatoFileFecha(string fecha, string formato, out string message)
        {
            message = "";
            string resp = "";
            try
            {
                DateTime fechaAux = Convert.ToDateTime(fecha);
                resp = fechaAux.ToString(formato);
            }
            catch(Exception ex)
            {
                message = ex.Message;
            }
            return resp;
        }
        public DateTime getFechaFileName(string file)
        {
            string[] fileNameArray = file.Split('_');
            string date = fileNameArray[1].Substring(0, 2) + "-" + fileNameArray[1].Substring(2, 2) + "-" + fileNameArray[1].Substring(4, 4);
            string[] timeArray = fileNameArray[2].Split('.');
            string time = timeArray[0].Substring(0, 2) + ":" + timeArray[0].Substring(2, 2) + ":" + timeArray[0].Substring(4, 2);

            return Convert.ToDateTime(date + " " + time);
        }
    }
}
