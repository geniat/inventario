﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Npgsql;


namespace BaseDatos
{
    public class clsPostgreSql
    {
        string ConnStringPostgresql;// = System.Configuration.ConfigurationManager.AppSettings["postgresSqlConectionString"];
        public string indicaError = "error: ";

        public clsPostgreSql(string connectionString)
        {
            ConnStringPostgresql = connectionString;// System.Configuration.ConfigurationManager.AppSettings["ConnStringPostgresGenia"];
        }
        public bool OpenConnection(out string mensaje)
        {
            bool resp = false;
            mensaje = "";
            NpgsqlConnection conn = new NpgsqlConnection(ConnStringPostgresql);
            try
            {
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                    resp = true;
                }
            }
            catch (Exception ex)
            {
                conn.Close();
                mensaje = ex.Message;
            }
            conn.Close();
            return resp;
        }
        public string exeQuery(string query)
        {
            //ConnStringPostgresql = ConfigurationSettings.AppSettings["postgresSqlConectionString"];
            string resp = "";
            
            NpgsqlConnection conn = new NpgsqlConnection(ConnStringPostgresql);
            try
            {
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                NpgsqlCommand commPostgresql = new NpgsqlCommand(query, conn);
                string select = query.Trim().Substring(0,6);
                switch (select)
                {
                    case "SELECT":
                        resp = commPostgresql.ExecuteScalar().ToString();
                        break;
                    case "UPDATE":
                        resp = commPostgresql.ExecuteNonQuery().ToString();
                        break;
                    case "DELETE":
                        resp = commPostgresql.ExecuteNonQuery().ToString();
                        break;
                    case "INSERT":
                        resp = commPostgresql.ExecuteNonQuery().ToString();
                        break;
                    case "TRUNCATE":
                        resp = commPostgresql.ExecuteNonQuery().ToString();
                        break;
                }
                conn.Close();
            }
            catch (Exception sqle)
            {
                conn.Close();
                //MessageBox.Show(sqle.Message);
                resp = indicaError + sqle.Message;
            }
            conn.Close();

            return resp;
        }
        public object exeQuery(string query, string tipoDato)
        {
            object resp = new object();
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            NpgsqlConnection conn = new NpgsqlConnection(ConnStringPostgresql);
            NpgsqlDataAdapter da = new NpgsqlDataAdapter(query, conn);
            try
            {
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                switch (tipoDato)
                {
                    case "DataTable":
                        da.Fill(dt);
                        resp = new DataTable();
                        resp = dt;
                        break;
                    case "DataSet":
                        da.Fill(ds);
                        resp = new DataSet();
                        resp = ds;
                        break;
                    case "string":
                        da.Fill(dt);
                        if (dt.Rows.Count > 0)
                            resp = dt.Rows[0][0].ToString();
                        break;

                }
                conn.Close();
            }
            catch (Exception sqle)
            {
                conn.Close();
                resp = sqle.Message;
            }
            conn.Close();
            return resp;
        }
        /// <summary>
        /// Transactions
        /// </summary>
        /// <param name="queries">Queries string array </param>
        /// <param name="return_count">Maximum number of data returned, if complete, added to the following query</param>
        /// <returns></returns>
        public string[] transactionExeQuery(string[] queries, int return_count)
        {
            string resp = "";
            NpgsqlConnection conn = new NpgsqlConnection(ConnStringPostgresql);
            // Start a local transaction
            conn.Open();
            NpgsqlCommand pgCommand = conn.CreateCommand();
            NpgsqlTransaction myTrans = conn.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);
            string[] returns = new string[return_count];
            try
            {

                int return_state = 0;
                foreach (string query in queries)
                {

                    string select = query.Trim().Substring(0, 6);
                    if (query.Contains("RETURNING id"))
                        select = "SELECT";

                    switch (select)
                    {
                        case "SELECT":
                            pgCommand.CommandText = query;
                            resp = pgCommand.ExecuteScalar().ToString();
                            if (return_state < return_count)
                            {
                                returns[return_state] = resp;
                                return_state++;
                            }
                            else
                                return_state = 0;
                            break;
                        case "UPDATE":
                        case "DELETE":
                        case "INSERT":
                        case "TRUNCATE":
                            string new_query = query;
                            if (return_state == return_count)
                            {
                                new_query = string.Format(new_query, returns);
                            }
                            pgCommand.CommandText = new_query;
                            resp = pgCommand.ExecuteNonQuery().ToString();
                            break;
                    }
                    //pgCommand.ExecuteNonQuery();
                }
                myTrans.Commit();
                
            }
            catch (Exception e)
            {
                myTrans.Rollback();
                return new string[] { e.Message + "\nStackTrace: " + e.StackTrace };
            }
            finally
            {
                pgCommand.Dispose();
                myTrans.Dispose();
            }
            return returns;

        }
        public bool validaUsuario(string usuario, string contrasenia)
        {
            bool resp = false;
            string[] arrStringCon = ConnStringPostgresql.Split(';');
            string newStringCon = "";
            foreach (string dato in arrStringCon)
            {
                if (dato.Contains("User Id"))
                    newStringCon += "User Id=" + usuario + ";";
                else if (dato.Contains("Password"))
                    newStringCon += "Password=" + contrasenia + ";";
                else
                    newStringCon += dato + ";";
            }
            try
            {
                NpgsqlConnection conn = new NpgsqlConnection(newStringCon);
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                    resp = true;
                }
                conn.Close();
            }
            catch
            {
                resp = false;
            }
            return resp;
        }
    }
}
