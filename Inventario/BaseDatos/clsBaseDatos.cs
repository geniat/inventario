﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using System.Data;

namespace BaseDatos
{
    public class clsBaseDatos
    {
        clsPostgreSql postgres;
        string tipoBaseDatos = "POSTGRESQL";
        public string indicaError = "error: ";
        public clsBaseDatos(string connectionString)
        {
            postgres = new clsPostgreSql(connectionString);
        }
        public bool isConnect()
        {
            bool resp = false;
            switch (tipoBaseDatos)
            {
                case "POSTGRESQL":
                    string mensaje = "";
                    resp = postgres.OpenConnection(out mensaje);
                    break;
            }
            return resp;
        }
        /// <summary>
        /// Ejecuta una consulta en base de datos
        /// </summary>
        /// <param name="query">texto de la consulta</param>
        /// <returns>resultado de la consulta puede incluir el error</returns>
        public string exeQuery(string query)
        {
            string resp = "";
            switch (tipoBaseDatos)
            {
                case "POSTGRESQL":
                    resp = postgres.exeQuery(query);
                    break;
            }

            return resp;
        }
        /// <summary>
        /// Ejecuta una consulta en base de datos
        /// </summary>
        /// <param name="query">texto de la consulta</param>
        /// <returns>resultado de la consulta puede incluir el error</returns>
        public object exeQuery(string query, string tipoDato)
        {
            object resp = "";
            switch (tipoBaseDatos)
            {
                case "POSTGRESQL":
                    resp = postgres.exeQuery(query,tipoDato);
                    break;
            }

            return resp;
        }

        /// <summary>
        /// Ejecuta una consulta en base de datos
        /// </summary>
        /// <param name="query">texto de la consulta</param>
        /// <returns>resultado de la consulta puede incluir el error</returns>
        public string[] transactionExeQuery(string[] queries, int return_count)
        {
            string[] resp = new string[] { "Error SIN TRANSACCIÓN JC" };
            switch (tipoBaseDatos)
            {
                case "POSTGRESQL":
                    resp = postgres.transactionExeQuery(queries, return_count);
                    break;
            }

            return resp;
        }

        /// <summary>
        /// valida si el usuario y contraseña de postgres se digitó correctamente
        /// </summary>
        /// <param name="usuario">usuario</param>
        /// <param name="contrasenia">contraseña</param>
        /// <returns></returns>
        public bool validaUsuario(string usuario, string contrasenia)
        {
            bool resp = false;
            switch (tipoBaseDatos)
            {
                case "POSTGRESQL":
                    resp = postgres.validaUsuario(usuario, contrasenia);
                    break;
            }
            return resp;
        }

    }
}
